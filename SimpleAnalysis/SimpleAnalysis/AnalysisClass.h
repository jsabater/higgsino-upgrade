#ifndef ANALYSISCLASS_H
#define ANALYSISCLASS_H

#include "SimpleAnalysis/OutputHandler.h"
#include "TLorentzVector.h"
#include <vector>
#include <algorithm>
#include <functional>
#include <initializer_list>

#include <RootCore/Packages.h>

#include "SimpleAnalysis/BDT.h"
#include "SUSYTools/SUSYCrossSection.h"

#ifdef ROOTCORE_PACKAGE_Ext_RestFrames
#include "SimpleAnalysis/RestFramesHelper.h"
#endif

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#include "xAODBTaggingEfficiency/BTaggingTruthTaggingTool.h"
#include "xAODBTaggingEfficiency/TruthTagResults.h"
#endif

enum AnalysisObjectType {
  ELECTRON, MUON, TAU, PHOTON, JET, FATJET, MET, COMBINED, NONE, TRUTH
};

enum AnalysisCommonID {
    NotBit = 1 << 31
};
#define NOT(x) (NotBit|x)

enum AnalysisElectronID {
  EVeryLooseLH = 1 << 0, 
  ELooseLH = 1 << 1, 
  EMediumLH = 1 << 2, 
  ETightLH = 1 << 3, 
  ELooseBLLH = 1 << 4, 
  EIsoGradientLoose = 1 << 8, 
  EIsoBoosted = 1 << 9, // from 3-bjet paper
  EIsoFixedCutTight = 1 << 10, 
  EIsoLooseTrack = 1 << 11, 
  EIsoLoose = 1 << 12, 
  EIsoGradient = 1 << 13, 
  EIsoFixedCutLoose = 1 << 14, 
  EIsoFixedCutTightTrackOnly = 1 << 15, 
  ED0Sigma5 = 1 << 16, 
  EZ05mm = 1 << 17, 
  EIsoFCTight = 1<< 18,
  EIsoFCTightTrackOnly = 1<< 19,

  EGood = EVeryLooseLH | ELooseLH | EMediumLH | ETightLH | ELooseBLLH | ED0Sigma5 | EZ05mm, 
  EIsoGood = EGood | EIsoGradientLoose | EIsoBoosted | EIsoFixedCutTight | EIsoFCTight | EIsoLooseTrack | EIsoLoose | EIsoGradient | EIsoFixedCutLoose | EIsoFixedCutTightTrackOnly | EIsoFCTightTrackOnly
};

enum AnalysisMuonID {
    MuLoose = 1 << 0, 
    MuMedium = 1 << 1, 
    MuTight = 1 << 2, 
    MuVeryLoose = 1 << 3, 
    MuHighPt = 1 << 4, 
    MuIsoGradientLoose = 1 << 8, 
    MuIsoBoosted = 1 << 9, // from 3-bjet paper
    MuIsoFixedCutTightTrackOnly = 1 << 10, 
    MuIsoLooseTrack = 1 << 11, 
    MuIsoLoose = 1 << 12, 
    MuIsoGradient = 1 << 13, 
    MuIsoFixedCutLoose = 1 << 14, 
    MuD0Sigma3 = 1 << 16, 
    MuZ05mm = 1 << 17, 
    MuNotCosmic = 1 << 18, 
    MuQoPSignificance = 1 << 19, 
    MuCaloTaggedOnly = 1 << 20, 
    MuIsoFCTightTrackOnly = 1 << 21,
    MuIsoFCTightFR = 1 << 22,

    MuGood = MuLoose | MuMedium | MuTight | MuHighPt | MuVeryLoose | MuD0Sigma3 | MuZ05mm | MuNotCosmic | MuQoPSignificance, 
    MuIsoGood = MuGood | MuIsoGradientLoose | MuIsoBoosted | MuIsoFixedCutTightTrackOnly | MuIsoFCTightTrackOnly | MuIsoLooseTrack | MuIsoLoose | MuIsoGradient | MuIsoFixedCutLoose | MuIsoFCTightFR
};

enum AnalysisTauID {
    TauLoose = 1 << 0, 
    TauMedium = 1 << 1, 
    TauTight = 1 << 2, 
    TauOneProng = 1 << 10, 
    TauThreeProng = 1 << 11, 
    TauGood = TauLoose | TauMedium | TauTight, 
    TauIsoGood = TauGood
};

enum AnalysisPhotonID {
    PhotonLoose = 1 << 0, 
    PhotonTight = 1 << 1, 
    PhotonIsoFixedCutLoose = 1 << 8, 
    PhotonIsoFixedCutTight = 1 << 9, 
    PhotonGood = PhotonLoose | PhotonTight, 
    PhotonIsoGood = PhotonGood | PhotonIsoFixedCutLoose | PhotonIsoFixedCutTight
};

enum AnalysisJetID {
    LooseBadJet = 1 << 8, 
    TightBadJet = 1 << 9, 
    JVT50Jet = 1 << 10, 
    JVT120Jet = 1 << 11,
    LessThan3Tracks = 1 << 12, // most signal jets should fail this
    GoodJet = LooseBadJet | TightBadJet | JVT50Jet | JVT120Jet,
    BTag85MV2c20 = 1 << 0, 
    BTag80MV2c20 = 1 << 1, 
    BTag77MV2c20 = 1 << 2, 
    BTag70MV2c20 = 1 << 3, 
    BTag85MV2c10 = 1 << 4, 
    BTag77MV2c10 = 1 << 5, 
    BTag70MV2c10 = 1 << 6, 
    BTag60MV2c10 = 1 << 7, 
    GoodBJet = BTag85MV2c20 | BTag80MV2c20 | BTag77MV2c20 | BTag70MV2c20 | BTag85MV2c10 | BTag77MV2c10 | BTag70MV2c10 | BTag60MV2c10 | GoodJet, 
    TrueLightJet = 1 << 27, //These should not be used for actual analysis selection, only for understanding
    TrueCJet = 1 << 28, 
    TrueBJet = 1 << 29, 
    TrueTau = 1 << 30,

};

enum AnalysisFatJetID {
    LooseFatJet = 1 << 8, 
    GoodFatJet = LooseFatJet
};

enum ParticleIDs {
    //PDGIDs
    Neutralino1 = 1000022,
    Neutralino2 = 1000023,
    Neutralino3 = 1000025,
    Neutralino4 = 1000035,
    Chargino1 = 1000024,
    Chargino2 = 1000037,
    //Status codes
    StablePart = 1<<24,
    DecayedPart = 2<<24
};

class AnalysisObject: public TLorentzVector {
    public:
 AnalysisObject(double Px, double Py, double Pz, double E, int charge, int id, AnalysisObjectType type, int motherID, int orgIndex, float ptcone, float etcone) :
                    TLorentzVector(Px, Py, Pz, E),
		      _charge(charge),
		      _id(id),
		      _type(type),
		      _motherID(motherID),
		      _orgIndex(orgIndex), 
		      _ptcone(ptcone),
		      _etcone(etcone) {
        }
        ;
 AnalysisObject(TLorentzVector tlv, int charge, int id, AnalysisObjectType type, int motherID, int orgIndex, float ptcone, float etcone) :
                    TLorentzVector(tlv),
                    _charge(charge),
                    _id(id),
                    _type(type),
		    _motherID(motherID),
		    _orgIndex(orgIndex),
		    _ptcone(ptcone),
		    _etcone(etcone) {
        }
        ;
        virtual bool pass(int id) const {
	    if (_type==TRUTH) {
	      int mask = (id>>24)? 0x7FFFFFFF : 0xFFFFFF;
	      return ((_id & mask) == id) || id==0;
	    }
            if (id & NotBit) return (id & _id) == 0;
            else return (id & _id) == id;
        }
        ;
        virtual int charge() const {
            return _charge;
        }
        ;
        virtual AnalysisObjectType type() const {
            return _type;
        }
        ;
	virtual bool valid() const { 
	    return _type!=NONE; 
	}
	;
        virtual int id() const {
            return _id;
        };
	virtual int pdgId() const {
	    if (_type!=TRUTH) throw std::runtime_error("Can only call pdgId() for truth objects");
	    int pdg = _id&0xFFFFFF;
	    if (_id&(1<<31)) pdg=-1*pdg;
            return pdg;
        };
        virtual int status() const {
	    if (_type!=TRUTH) throw std::runtime_error("Can only call status() for truth objects");
	    return (_id>>24)&0x7F;
	};
        virtual int motherID() const {
            return _motherID;
        }
        ; // not supposed to be used directly except to store in ntuples
	virtual int index() const { 
	    return _orgIndex; 
        }; //for internal use                                                                                                                                                                                   
        virtual float ptcone() const {
	  return _ptcone;
        }; //for internal use                                                                                                                                                                                   
        virtual float etcone() const {
	  return _etcone;
	}; //for internal use
        virtual AnalysisObject transFourVect() const {
            TLorentzVector tlv;
            tlv.SetPtEtaPhiM(Pt(), 0.0, Phi(), M());
            return AnalysisObject(tlv, charge(), id(), type(), motherID(), _orgIndex, ptcone(), etcone());
        }
        ;
	virtual TVector3 prodVtx() const {
	  TVector3 zero;
	  if (_vertices.size()>0) return _vertices[0];
	  return zero;
	};
	virtual TVector3 decayVtx() const {
	  TVector3 zero;
	  if (_vertices.size()>1) return _vertices[1];
	  return zero;
	};
	virtual void setProdVtx(const TVector3 &vtx) {
	  if (_vertices.size()) _vertices[0]=vtx;
	  else _vertices.push_back(vtx);
	};
	virtual void setId(const int id) { // not meant for analysis use
	  _id=id;
	}
	virtual void setDecayVtx(const TVector3 &vtx) {
	  if (_vertices.size()>1) _vertices[1]=vtx;
	  else if (_vertices.size()>0) _vertices.push_back(vtx);
	  else {
	    TVector3 zero;
	    _vertices.push_back(zero);
	    _vertices.push_back(vtx);
	  }
	};
    private:
        int _charge; //not used for jets or photons
        int _id;
        AnalysisObjectType _type;
	int _motherID;
        int _orgIndex;
	float _ptcone;
        float _etcone;
	std::vector<TVector3> _vertices;
};

AnalysisObject operator+(const AnalysisObject& lhs, const AnalysisObject& rhs);

//typedef std::vector<AnalysisObject> AnalysisObjects;

class AnalysisObjects: public std::vector<AnalysisObject> {
  using std::vector<AnalysisObject>::vector; //reuse vector initializers
  //force range check when accessing objects directly
 public:
  AnalysisObject& operator[](std::vector<AnalysisObject>::size_type ii) { return this->at(ii); };
  const AnalysisObject& operator[](std::vector<AnalysisObject>::size_type ii) const { return this->at(ii); };
};

AnalysisObjects operator+(const AnalysisObjects& lhs, const AnalysisObjects& rhs);

class AnalysisEvent {
    public:
        virtual AnalysisObjects getElectrons(float ptCut, float etaCut, int isolation = 1)=0;
        virtual AnalysisObjects getMuons(float ptCut, float etaCut, int isolation = 1)=0;
        virtual AnalysisObjects getTaus(float ptCut, float etaCut, int isolation = 1)=0;
        virtual AnalysisObjects getPhotons(float ptCut, float etaCut, int isolation = 1)=0;
        virtual AnalysisObjects getJets(float ptCut, float etaCut, int btag = 0)=0;
        virtual AnalysisObjects getFatJets(float ptCut, float etaCut, int btag = 0)=0;
	virtual AnalysisObjects getHSTruth(float ptCut=0, float etaCut=100, int pid = 0)=0;
        virtual AnalysisObject getMET()=0;
	virtual float getMETSignificance()=0;
        virtual float getSumET()=0;
        virtual float getGenMET()=0;
        virtual float getGenHT()=0;
        virtual int getMCNumber()=0; //Temporary until better solution found
        virtual int getSUSYChannel()=0; //Temporary until better solution found
        virtual int getPDF_id1() =0;
        virtual float getPDF_x1() =0;
        virtual float getPDF_pdf1() =0;
        virtual int getPDF_id2() =0;
        virtual float getPDF_x2() =0;
        virtual float getPDF_pdf2() =0;
        virtual float getPDF_scale()=0;
        virtual std::vector<float> getMCWeights()=0; //Temporary until better solution found
	virtual AnalysisObject getTruthParticle(const AnalysisObject& obj) =0;
        virtual ~AnalysisEvent() {
        }
        ;
};

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
class BTaggingTruthTaggingTool;
#endif

class AnalysisClass;

std::vector<AnalysisClass*> *getAnalysisList(); //for automatically tracking which analyses have been defined

class AnalysisClass
{
 public:
  SUSY::CrossSectionDB *my_XsecDB; //!  


 AnalysisClass(const std::string &name) : _name(name),_output(0) { getAnalysisList()->push_back(this); };
  AnalysisClass() {};
  virtual void Init() {};
  virtual void ProcessEvent(AnalysisEvent *event)=0;
  virtual void Final() {};
  virtual ~AnalysisClass() {};
  virtual const std::string& name() {return _name; };
  virtual void setOutput(OutputHandler *output) { _output=output; };
  OutputHandler* getOutput() { return _output; };
  virtual void addRegion(const std::string &label) { _output->addEntry(label); };
  virtual void addRegions(const std::vector<std::string> &labels) { _output->addEntries(labels); };
  virtual void addHistogram(const std::string &label, int bins, float min, float max) { _output->addHistogram(label, bins, min, max); };
  virtual void addHistogram(const std::string &label,int bins,float *edges) { _output->addHistogram(label, bins, edges); }
  virtual void addHistogram(const std::string &label,std::vector<float> &edges) { _output->addHistogram(label, edges); }
  virtual void addHistogram(const std::string &label, int binsX, float minX, float maxX,
			    int binsY,float minY,float maxY) { _output->addHistogram(label, binsX, minX, maxX, binsY, minY, maxY); };
  virtual void accept(const std::string &name,double weight=1) { _output->pass(name, weight); };
  virtual void fill(const std::string &name,double x) { _output->fillHistogram(name, x); };
  virtual void fill(const std::string &name,double x,double y) { _output->fillHistogram(name, x, y); };
  virtual void fillWeighted(const std::string &name,double x,double y, double val) { _output->fillHistogramWeighted(name, x, y, val); };
  void adjustEventWeight(double weight) { _output->adjustEventWeight(weight); };
  void ntupVar(const std::string &label,int value)   { _output->ntupVar(label,value); };
  void ntupVar(const std::string &label,float value) { _output->ntupVar(label,value); };
  void ntupVar(const std::string &label,double value) { _output->ntupVar(label,(float) value); };
  void ntupVar(const std::string &label,std::vector<int> values) { _output->ntupVar(label,values); };
  void ntupVar(const std::string &label,std::vector<float> values) { _output->ntupVar(label,values); };
  void ntupVar(const std::string &label,AnalysisObject &object, bool saveMass=false, bool saveType=false, bool saveVtx=false);
  void ntupVar(const std::string &label,AnalysisObjects &objects, bool saveMass=false, bool saveType=false, bool saveVtx=false);

  // Helper functions
  std::vector<float> fakeJER(const AnalysisObjects &jets);
  static AnalysisObjects overlapRemoval(const AnalysisObjects &cands,
					const AnalysisObjects &others,
					float deltaR, int passId=0);

  static AnalysisObjects overlapRemoval(const AnalysisObjects &cands,
					const AnalysisObjects &others,
					std::function<float(const AnalysisObject&,const AnalysisObject&)> radiusFunc,
					int passId=0);

  static AnalysisObjects lowMassRemoval(const AnalysisObjects & cand , std::function<bool(const AnalysisObject&, const AnalysisObject&)>, float MinMass= 0, float MaxMass = FLT_MAX, int type = -1);

  static AnalysisObjects filterObjects(const AnalysisObjects& cands,
				       float ptCut,float etaCut=100.,int id=0,unsigned int maxNum=10000);

  static AnalysisObjects filterCrack(const AnalysisObjects& cands,float minEta=1.37,float maxEta=1.52);

  static int countObjects(const AnalysisObjects& cands,
			  float ptCut,float etaCut=100.,int id=0);

  static float sumObjectsPt(const AnalysisObjects& cands,
			    unsigned int maxNum=10000,float ptCut=0);

  static float sumObjectsM(const AnalysisObjects& cands,
			    unsigned int maxNum=10000,float mCut=0);

  static void sortObjectsByPt(AnalysisObjects& cands);
  static float minDphi(const AnalysisObject &met, const AnalysisObjects& cands, unsigned int maxNum=10000, float ptCut=0);
  static float minDphi(const AnalysisObject &met, const AnalysisObject& cand);
  static float minDR(const AnalysisObject &cand,const AnalysisObjects& cands, unsigned int maxNum=10000, float ptCut=0);
  static float minDR(const AnalysisObjects &cands, unsigned int maxNum=10000, float ptCut=0);

  static float calcMCT(const AnalysisObject &o1, const AnalysisObject& o2);
  static float calcMT(const AnalysisObject &lepton, const AnalysisObject &met);
  static float calcMTmin(const AnalysisObjects& cands, const AnalysisObject& met, int maxNum=10000);
  static float calcMT2(const AnalysisObject &o1, const AnalysisObject &o2, const AnalysisObject &met);
  static float calcAMT2(const AnalysisObject &o1, const AnalysisObject &o2, const AnalysisObject &met, float m1, float m2);
  static float calcMTauTau(const AnalysisObject &o1, const AnalysisObject &o2, const AnalysisObject &met);

  static float aplanarity(const AnalysisObjects& jets);
  static float sphericity(const AnalysisObjects& jets);

  static bool IsSFOS(const AnalysisObject& L, const AnalysisObject& L1);
  static std::pair<float, float> DiZSelection(const AnalysisObjects& electrons, const AnalysisObjects& muons);
  static bool PassZVeto(const AnalysisObjects& electrons, const AnalysisObjects &Muons, float Window = 10);


  static AnalysisObjects reclusterJets(const AnalysisObjects &jets, float radius, float ptmin, float rclus=-1, float ptfrac=-1);

  std::vector<BDT::BDTReader*> m_BDTReaders;


#ifdef ROOTCORE_PACKAGE_Ext_RestFrames
        RestFramesHelper m_RF_helper;
#endif

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
        BTaggingTruthTaggingTool *m_btt;
#endif

    protected:
        std::string _name;
        OutputHandler *_output;

};

#define DefineAnalysis(ANALYSISNAME)					\
  class ANALYSISNAME : public AnalysisClass {				\
  public:								\
  ANALYSISNAME() : AnalysisClass(#ANALYSISNAME) {};			\
    void Init();							\
    void ProcessEvent(AnalysisEvent *event);				\
  };									\
  static const AnalysisClass * ANALYSISNAME_instance __attribute__((used)) = new ANALYSISNAME();

std::string FindFile(const std::string& name);

// debugging functions

void printObject(const AnalysisObject& obj); 

void printObjects(const AnalysisObjects& objs);

#define DebugObject(obj,num)		    \
  do {                                      \
    static int cnt=0;                       \
    if (num || cnt<num) printObject(obj); \
    cnt++;                                  \
  } while(0)                                

#define DebugObjects(objs,num)		    \
  do {                                      \
    static int cnt=0;                       \
    if (num || cnt<num) printObjects(objs); \
    cnt++;                                  \
  } while(0)                                

#endif

