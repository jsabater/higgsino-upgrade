#ifndef ANALYSISRUNNER_H
#define ANALYSISRUNNER_H

#include <vector>
#include <string>

#include "SimpleAnalysis/TruthEvent.h"
#include "SimpleAnalysis/AnalysisClass.h"
#include "SimpleAnalysis/TruthSmear.h"
#include "SimpleAnalysis/Reweight.h"
#include "SimpleAnalysis/OutputHandler.h"


class AnalysisRunner
{
 public:
 AnalysisRunner(std::vector<AnalysisClass*>& analysisList) : _analysisList(analysisList), _reweighter(0), _runs(1) {};
  void init() { for(const auto& analysis : _analysisList) { analysis->getOutput()->init(); analysis->Init(); } };
  void final() { for(const auto& analysis : _analysisList) analysis->Final(); };
  void SetSmearing(TruthSmear *smear) { _smear=smear; };
  void SetMultiRuns(int runs) { _runs=runs; };
  void AddReweighting(Reweighter *reweighter) { _reweighter.push_back(reweighter); };
  void SetMCWeightIndex(int mcwidx) { _mcwindex=mcwidx; };

  int getMCWeightIndex(){ return _mcwindex; };

  void processEvent(TruthEvent *event,int eventNumber); 
 private:
  std::vector<AnalysisClass*>& _analysisList;
  TruthSmear *_smear;
  std::vector<Reweighter *> _reweighter;
  int _mcwindex;
  int _runs;
};

class Reader
{
 public:
  Reader(std::vector<AnalysisClass*>& analysisList) {_analysisRunner=new AnalysisRunner(analysisList); }
  virtual ~Reader() {};
  virtual void SetSmearing(TruthSmear *smear) { _analysisRunner->SetSmearing(smear); };
  virtual void SetMultiRuns(int runs) { _analysisRunner->SetMultiRuns(runs); };
  virtual void AddReweighting(Reweighter *reweighter) { _analysisRunner->AddReweighting(reweighter); };
  virtual void SetMCWeightIndex(int mcwidx) { _analysisRunner->SetMCWeightIndex(mcwidx); };

  virtual int getMCWeightIndex(){ return _analysisRunner->getMCWeightIndex(); };

  virtual void processFiles(const std::vector<std::string>& inputNames, unsigned int nevents=-1) {
    _analysisRunner->init();
    processFilesInternal(inputNames, nevents);
    _analysisRunner->final();
  }
 protected:
  virtual void processFilesInternal(const std::vector<std::string>& inputNames, unsigned int nevents) = 0;
  AnalysisRunner *_analysisRunner;
 private:
		    
};


#endif
