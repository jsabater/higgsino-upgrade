#include "xAODRootAccess/TEvent.h"
#include <TFile.h>
#include <TTree.h>
#include <vector>
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "MCTruthClassifier/MCTruthClassifier.h"
#include "SimpleAnalysis/AnalysisClass.h"
#include "SimpleAnalysis/AnalysisRunner.h"

using std::vector;

class xAODTruthReader : public Reader {

 public:
  xAODTruthReader(std::vector<AnalysisClass*>& analysisList, bool useVisTau=true, bool useTruthBSM=true);
  ~xAODTruthReader();

 protected:
  bool processEvent(xAOD::TEvent *event,xAOD::TStore *store);
  void processFilesInternal(const std::vector<std::string>& inputNames, unsigned int nevents);
  int getTruthOrigin(const xAOD::TruthParticle *part);
  int getTruthType(const xAOD::TruthParticle *part);
  int getMotherID(const xAOD::TruthParticle *part);
  xAOD::TruthParticleContainer* findTruthParticles(xAOD::TStore *store,
						   const xAOD::TruthParticleContainer* truthparticles,
						   std::vector<int> pdgIds, int status=1);
  xAOD::TruthParticleContainer* findTruthBSMParticles(xAOD::TStore *store,
						      const xAOD::TruthParticleContainer* truthparticles);

 private:
  ST::SUSYObjDef_xAOD *_susytools;
  MCTruthClassifier* _mctool;
  xAOD::TEvent* _event;
  bool _useVisTau;
  bool _useTruthBSM;
};
