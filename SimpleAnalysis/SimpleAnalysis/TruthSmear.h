#ifndef TRUTHSMEAR_H
#define TRUTHSMEAR_H

#include <vector>
#include <string>

#include "TRandom3.h"

class TruthEvent;
class AnalysisEvent;
namespace Upgrade {
  class UpgradePerformanceFunctions;
}

class TruthSmear
{
 public:
  TruthSmear(std::vector<std::string>&);
  TruthEvent *smearEvent(AnalysisEvent*);

 private:
  bool smearElectrons;
  bool smearMuons;
  bool smearTaus;
  bool smearPhotons;
  bool smearJets;
  bool smearMET;
  bool addPileupJets;
  bool useHGTD0;
  bool useHGTD1;
  bool useMuonHighEta;
  bool optPhotons;
  Upgrade::UpgradePerformanceFunctions *m_upgrade;
  Upgrade::UpgradePerformanceFunctions *m_upgradeMuonTight;
  Upgrade::UpgradePerformanceFunctions *m_upgradeMuonHighPt;
  TRandom3 m_random;
};


#endif
