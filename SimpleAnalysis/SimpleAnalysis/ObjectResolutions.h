#ifndef OBJECTRESOLUTIONS_H
#define OBJECTRESOLUTIONS_H

#include "SimpleAnalysis/AnalysisClass.h"

void getObjectResolution(AnalysisObject& obj, double& pt_reso, double& phi_reso);

#endif
