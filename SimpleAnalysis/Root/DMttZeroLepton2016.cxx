#include "SimpleAnalysis/AnalysisClass.h"
#include "TMath.h"

DefineAnalysis(DMttZeroLepton2016)

void DMttZeroLepton2016::Init()
{
  addRegions({"SRHigh","SRLow"});
}

void DMttZeroLepton2016::ProcessEvent(AnalysisEvent *event)
{
    auto candJets   = event->getJets(20., 2.8); //jets
    auto electrons =  event->getElectrons(7, 2.47, EVeryLooseLH);

    //Muons
    auto muons      = event->getMuons(6, 2.5, MuLoose);

    // not doing overlap removal between electrons and muons with identical track
    candJets   = overlapRemoval(candJets,electrons,0.2,NOT(BTag85MV2c10));
    electrons  = overlapRemoval(electrons,candJets,0.4);
    candJets   = overlapRemoval(candJets, muons, 0.4, LessThan3Tracks);
    muons      = overlapRemoval(muons, candJets, 0.4);

    if (countObjects(candJets, 20, 2.8, NOT(LooseBadJet))!=0) return;
   
    candJets = filterObjects(candJets, 20, 2.8, JVT50Jet);
    
    auto signalElectrons = filterObjects(electrons, 25, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
    auto signalMuons     = filterObjects(muons, 25, 2.5, MuMedium|MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
    auto signalLeptons   = signalElectrons + signalMuons;

    auto metVec     = event->getMET();
    double met   = metVec.Et();

    auto signalJets      = filterObjects(candJets, 20);
    auto signalBjets     = filterObjects(signalJets, 20., 2.5, BTag77MV2c10);

    int nJets  = signalJets.size();
    int nBjets = signalBjets.size();
    
    AnalysisObjects RecJets1p2 = reclusterJets(signalJets,1.2,5.0);
    AnalysisObjects RecJets0p8 = reclusterJets(signalJets,0.8,5.0);
    
    if (signalLeptons.size()!=0) return;
    if (nJets<4) return;
    if (signalJets[0].Pt() < 80 ||  signalJets[1].Pt() < 80 || signalJets[2].Pt() < 40 || signalJets[3].Pt() < 40) return;
    if (met < 250.) return;
        
    float dphiMin4  = minDphi(metVec, signalJets, 4);
    if(dphiMin4 < 0.4) return;
    
    if (nBjets<2) return;

    // Other cut
    
    float drbb    = signalBjets[0].DeltaR(signalBjets[1]);
    float HT = sumObjectsPt(signalJets);
    float signif = met / sqrt(HT);
    
    double dMin=999;
    double dMax=-1;
    double dp;
    double mTBmin=0;
    double mTBmax=0;
    for (unsigned int i=0; i<signalBjets.size();i++) {
        dp=metVec.DeltaPhi(signalBjets[i]);
        if (fabs(dp)<dMin) {dMin=fabs(dp);mTBmin=calcMT(signalBjets[i], metVec);}
        if (fabs(dp)>dMax) {dMax=fabs(dp);mTBmax=calcMT(signalBjets[i], metVec);}
    }

    if (RecJets1p2.size()<2) return;
    if (RecJets0p8.size()<2) return;
    
    if (met>300. && RecJets1p2[0].M()>140. && RecJets1p2[1].M()>80. && drbb >1.5 && signif > 12. && mTBmin> 200.) accept("SRHigh");
    if (met>300. && RecJets0p8[0].M()>80. && RecJets0p8[1].M()>80. && drbb >1.5  && mTBmin> 150. && mTBmax> 250.) accept("SRLow");
 
    return;
}

	
	
