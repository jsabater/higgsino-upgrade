#include "SimpleAnalysis/AnalysisClass.h"
#include "SimpleAnalysis/PDFReweight.h"
#include <LHAPDF/LHAPDF.h>
//#include "SimpleAnalysis/METParam.h"
#include <TSystem.h>
#include "SUSYTools/SUSYCrossSection.h"

DefineAnalysis(EwkTwoLeptonTwoJets_Upgrade)

namespace {
PDFReweighter *ReweightingClass = nullptr;
}

void EwkTwoLeptonTwoJets_Upgrade::Init()
{

  // Book 1/2D histograms
  addHistogram("SumOfWeights", 1, 0.5, 1.5);

   addHistogram("ptl1", 50, 0, 150);
   addHistogram("ptl2", 50, 0, 150);
   addHistogram("MET",100,0,2000);
  // addHistogram("EtCone",50,-0.01,0.01);
  // addHistogram("PtCone",50,-0.01,0.01);

  
   my_XsecDB = new SUSY::CrossSectionDB(PathResolverFindCalibFile("dev/PMGTools/PMGxsecDB_mc16.txt"), true);

  //Do PDF reweighting
  std::vector<std::string> PDFOptions;
  PDFOptions.push_back("NNPDF30_nlo_as_0118");
  PDFOptions.push_back("13");
  PDFOptions.push_back("14");
  //  ReweightingClass = new PDFReweighter();
  //  ReweightingClass->init(PDFOptions);
}

void EwkTwoLeptonTwoJets_Upgrade::ProcessEvent(AnalysisEvent *event)
{

 
  

  //   SetRecordEvent(false);

  // Retrieve basic object lists


  auto baselineElectrons = event->getElectrons(3, 2.47, ELooseLH);
  auto baselineMuons = event->getMuons(3, 2.5, MuMedium | MuQoPSignificance);
  auto jets = event->getJets(25., 2.8);
  auto metVec = event->getMET();
  //et and pt cone for the muon isolation


  // fill("EtCone",etcone);
  // fill("PtCone",ptcone);
  
  float gen_met = event->getGenMET();
  
  double xsec = -1;
  int mc_channel = event->getMCNumber();
  if (xsec < 0) {
    xsec = my_XsecDB->xsectTimesEff(mc_channel);
  }
  

  //  if (xsec < 0) {
  // std::cout << mc_channel << " " << xsec << std::endl;
  //}

  //C1C1_130_100 xsec
  //if (mc_channel == 960000) {
  //xsec = 1.8768145;
    // printf("the xsec for C1C1 is %f \n", xsec);
  //}


  //xsec in pb
  
  if (mc_channel == 364231) xsec = 0.14536; //Zmumu_280_500_Mll130 
  if (mc_channel == 364232) xsec = 0.033616; //Zmumu 500_100_Mll130
  if (mc_channel == 364233) xsec = 0.0030715; //Zmumu 100_E_CMS_Mll130
  
  if (mc_channel == 364235) xsec = 0.14567; //Zee 280_500_Mll130
  if (mc_channel == 364236) xsec = 0.033665; //Zee 500_1000_Mll130
  if (mc_channel == 364237) xsec = 0.0030774; //Zee 1000_E_CMS_Mll130
  
  if (mc_channel == 364280) xsec = 44.894579; //Zee_280_E_CMS_Mll2ml250
  if (mc_channel == 364281) xsec = 35.484864; //Zmumu_280_E_CMS_Mll2ml250
  if (mc_channel == 364282) xsec = 9.1378; //Ztautau_280_E_CMS_Mll2ml250
  
 
  if (mc_channel == 407099) xsec = 863.990*0.10462; //ttbar MET100 //Multiplied times the filter efficiency
  
  if (mc_channel == 410503 && gen_met > 100.) return; //remove ttbar vs MET100 overlap 
  


   
   double weight = 0;
   for (unsigned int iw = 0; iw < event->getMCWeights().size(); iw++) {
     if (iw == 0) weight = event->getMCWeights()[iw];
   }
   fill("SumOfWeights", 1);


   

  //Testing histogram
  double met      = metVec.Et();
  fill("MET",met);

  // Reject events with bad jets
  if (countObjects(jets, 50, 2.8, NOT(LooseBadJet)) != 0) return;

  // SUSY overlap removal
  jets               = overlapRemoval(jets, baselineElectrons, 0.2);
  jets               = overlapRemoval(jets, baselineMuons, 0.4, LessThan3Tracks);
  
  baselineElectrons  = overlapRemoval(baselineElectrons, jets, 0.4);
  baselineMuons      = overlapRemoval(baselineMuons, jets, 0.4);

  //Signal collections
  auto signalElectrons = filterObjects(baselineElectrons, 3, 2.47, EMediumLH | EZ05mm | EIsoGradientLoose);
  auto signalMuons     = filterObjects(baselineMuons, 3, 2.5, MuD0Sigma3 | MuZ05mm | MuIsoGradientLoose);
    //auto signalJets = filterObjects(jets, 30);
  //auto bjets           = filterObjects(jets, 25., 2.8, BTag77MV2c20);
  auto bjets           = filterObjects(jets, 25., 2.8, BTag77MV2c10);



  //PDF reweight from 13 to 14 TeV
  //Only the Sherpa samples are in 13 TeV, the rest are already 14 TeV
  double PDFReweighting = 1.;
  if (mc_channel != 407099 && mc_channel != 410503 && mc_channel != 960000 && mc_channel != 0) {
    //    PDFReweighting = ReweightingClass->reweightEvent(event);
  }
  
  AnalysisObjects signalLeptons = signalElectrons + signalMuons;
  int nLeptons = signalLeptons.size();

  float etcone_lep1 = -1.;
  float ptcone_lep1 = -1.;
  float etcone = -1.;
  float ptcone = -1.;
  // Reject events with less than 1 leptons
   if (nLeptons < 1) return;


  
   //   SetRecordEvent(true);
   // etcone = event->getGenEtCone20();
   // ptcone = event->getGenPtCone30();


   auto lep0 = signalLeptons[0];
   double pt_lep1   = lep0.Pt();
   double eta_lep1 = lep0.Eta();
   double phi_lep1 = lep0.Phi();
   int flavlep1 = lep0.type() * lep0.charge();
   int lep1_Mother= lep0.motherID();
   ptcone_lep1 = lep0.ptcone();
   etcone_lep1 = lep0.etcone();

 

  // Reject events with less than 2 jets

  //int nJets = signalJets.size();
  
  // if (nJets < 2) return;

  // Variables we'll be cutting on
  double MET    = metVec.Et();
  //double meff   = sumObjectsPt(jets, 2, 25) + lep0.Pt() + lep1.Pt() + MET;

  double mT      = calcMT(lep0, metVec);




 
  double EtMiss_Phi     = metVec.Phi();
  
 




  double HT      = 0.;
  TLorentzVector sumJet;
  for (unsigned int i = 0; i < jets.size(); i++) {
    sumJet += jets[i];
  }
  HT = sumJet.Pt();
  

  int m_nbjets    = countObjects(bjets, 25, 2.8);
  int m_nbjets50  = countObjects(bjets, 50, 2.8);
  int m_nbjets100 = countObjects(bjets, 100, 2.8);

  int m_njets     = countObjects(jets, 25, 2.8);
  int m_njets50   = countObjects(jets, 50, 2.8);
  int m_njets60   = countObjects(jets, 60, 2.8);
  int m_njets100  = countObjects(jets, 100, 2.8);

 

  // bool m_OS = false;
 
  // if (lep0.M() == lep1.M()) isSF = true;
 
  //  if (lep0.charge() != lep1.charge()) m_OS = true;
  //Reject events with same flavour leptons
  //if (isSF) return;
  


  //  double dPhiEtmisspbll = fabs(metVec.DeltaPhi(metVec + lep0 + lep1));


 //Opposite Sign leptons
  bool m_OS = false;
  bool isSF = false;
  double pTbll          = -1.;
  double ptll           = -1.;
  double MT2            = -1.;
  double dPhiEtmisspbll = -1.;
  int DX                = -1.;
  double mll            = -1.;
  double meff           = -1.;
  double dPhiMETll      = -1.;
  double dRMETll        = -1.;
  double R1             = -1.;
  double pt_lep2        = -1.;
  double eta_lep2       = -4.;
  double phi_lep2       = -4.;
  int flavlep2          = -4.;
  int flavlep3          = -4.;
  double dEtall         = -10.;


  int lep2_Mother=0;
  int lep3_Mother=0;
  
  //ditau inv mass definition
  double Mtt = -1000.;
  float etcone_lep2 = -1.;
  float ptcone_lep2 = -1.;
  float etcone_lep3 = -1.;
  float ptcone_lep3 = -1.;

   if (nLeptons > 1) {

     auto lep1 = signalLeptons[1];
     
     pt_lep2  = lep1.Pt();
     eta_lep2 = lep1.Eta();
     phi_lep2 = lep1.Phi();
     flavlep2 = lep1.type() * lep1.charge();

     if (lep0.charge() != lep1.charge()) m_OS = true;
     if (lep0.type() == lep1.type()) isSF = true;
     pTbll = (metVec + lep0 + lep1).Pt();
     ptll  = (lep0 + lep1).Pt();
     MT2   = calcMT2(lep0, lep1, metVec);;
     meff  = sumObjectsPt(jets) + lep0.Pt() + lep1.Pt() + MET;
     R1    = MET / meff;
     dPhiEtmisspbll = fabs(metVec.DeltaPhi(metVec + lep0 + lep1));
     DX    = fabs((2 * (lep0.Pz() + lep1.Pz())) / 14000.);  
     mll   = (lep0 + lep1).M();
     dPhiMETll = fabs(metVec.DeltaPhi(lep0 + lep1));
     dRMETll = metVec.DeltaR(lep0 + lep1);
     dEtall    = lep0.Eta() - lep1.Eta();

     
     //Calculating Di-tau invariant mass from MET and L1pT and L2pT
     // Calculate M2ditau: refer to e.g. arXiv:1501.02511
     Mtt = calcMTauTau(lep0,lep1,metVec);

     lep2_Mother= lep1.motherID();
     ptcone_lep2 = lep1.ptcone();
     etcone_lep2 = lep1.etcone();

     if (nLeptons == 3){

       auto lep2 = signalLeptons[2];

       ptcone_lep3 = lep2.ptcone();
       etcone_lep3 = lep2.etcone();
       lep3_Mother= lep2.motherID();
       flavlep3 = lep2.type() * lep2.charge();
     }
     
   } //nLeptons > 1
    // Minimum of the delta phi between all jets with MET
   float minDPhiAllJetsMet = minDphi(metVec,jets);

   double pt_jet1 = -1;
   double eta_jet1  = -5;
   double phi_jet1  = -5;
   double dPhiJ1MET = -1.;
   double m_jet1 = -1.;
 
  if (m_njets > 0) {
    pt_jet1          = jets[0].Pt();
    eta_jet1         = jets[0].Eta();
    phi_jet1         = jets[0].Phi();
    dPhiJ1MET        = fabs( jets[0].DeltaPhi( metVec ) );
    m_jet1 = jets[0].M();
  }
  double pt_jet2   = -1;
  double eta_jet2  = -5;
  double phi_jet2  = -5;
  double dPhiJ2MET = -1.;
  double dPhiJ1J2MET = -1.;
  double m_jet2 = -1.;
  //double dEtajj = -4.;
  //double dabsEtajj = -4.;
  //double mjj = -1.;
  if (m_njets > 1) {
    pt_jet2          = jets[1].Pt();
    eta_jet2         = jets[1].Eta();
    phi_jet2         = jets[1].Phi();
    dPhiJ2MET        = fabs( jets[1].DeltaPhi( metVec ) );
    dPhiJ1J2MET      = fabs(metVec.DeltaPhi(jets[0] + jets[1]));
    m_jet2 = jets[1].M();
    //dEtajj           = (jets[0] - jets[1]).Eta();
    // dabsEtajj        = fabs(jets[0] - jets[1]).Eta();
    //    mjj              = (jets[0] + jets[1]).M();
  }
  double pt_jet3   = -1;
  double eta_jet3  = -5;
  double phi_jet3  = -5;

  if (m_njets > 2) {
    pt_jet3          = jets[2].Pt();
    eta_jet3         = jets[2].Eta();
    phi_jet3         = jets[2].Phi();
  }

  double pt_jet4p = -1;
  if (m_njets > 3) {
     pt_jet4p = 0.;
    for (unsigned int i = 3; i < jets.size(); i++) {
      pt_jet4p += jets[i].Pt();
    }
  }
 
  double pt_bjet1 = -1;
  double eta_bjet1  = -5;
  double phi_bjet1  = -5;
  if (m_nbjets > 0) {
    pt_bjet1          = bjets[0].Pt();
    eta_bjet1         = bjets[0].Eta();
    phi_bjet1         = bjets[0].Phi();
  }
  double pt_bjet2   = -1;
  double eta_bjet2  = -5;
  double phi_bjet2  = -5;
   if (m_nbjets > 1) {
    pt_bjet2          = bjets[1].Pt();
    eta_bjet2         = bjets[1].Eta();
    phi_bjet2         = bjets[1].Phi();
   }

   double pt_bjet3p = -1;
  if (m_nbjets > 2) {
    pt_bjet3p = 0.;
   for (unsigned int i = 2; i < bjets.size(); i++) {
     pt_bjet3p += bjets[i].Pt();
   }
}

  // Access to parentID
  //  - for now only available for muons, electrons and photons and BSM particles and depends on input
  int leptonsFromZ=0;


  for(auto lepton : signalLeptons) {
    if (lepton.motherID() == 23) leptonsFromZ++;
  }


  //Fill pt 1st lepton
  fill("ptl1", pt_lep1);
  fill("ptl2", pt_lep2);

  ntupVar("ChannelNumber" , mc_channel);
  ntupVar("xsec"          , xsec);  
  ntupVar("MT2"           , MT2);
  ntupVar("MET"           , MET);
  ntupVar("METphi"        , EtMiss_Phi);

  ntupVar("meff"          , meff);
  ntupVar("HT"            , HT);
  ntupVar("mT"            , mT);
  ntupVar("R1"            , R1);
  ntupVar("Pbll"          , pTbll);
  ntupVar("dPhib"         , dPhiEtmisspbll);

  ntupVar("m_njet25"      , m_njets);
  ntupVar("m_njet50"      , m_njets50);
  ntupVar("m_njet60"      , m_njets60);
  ntupVar("m_njet100"     , m_njets100);
  ntupVar("nbjet"         , m_nbjets);
  ntupVar("nbjet50"       , m_nbjets50);
  ntupVar("nbjet100"      , m_nbjets100);

  ntupVar("Mll"           , mll);
  ntupVar("isOS"          , m_OS);
  ntupVar("dPhiMETll"     , dPhiMETll);
  ntupVar("dRMETll"       , dRMETll);
  ntupVar("pt_lep1"       , pt_lep1);
  ntupVar("eta_lep1"      , eta_lep1);
  ntupVar("phi_lep1"      , phi_lep1);
  ntupVar("pt_lep2"       , pt_lep2);
  ntupVar("eta_lep2"      , eta_lep2);
  ntupVar("phi_lep2"      , phi_lep2);
  ntupVar("ptll"          , ptll);
  ntupVar("flavlep1"      , flavlep1);
  ntupVar("flavlep2"      , flavlep2);
  ntupVar("flavlep3"      , flavlep3);
  ntupVar("pt_jet1"       , pt_jet1);
  ntupVar("m_jet1"        , m_jet1);
  ntupVar("pt_jet2"       , pt_jet2);
  ntupVar("m_jet2"        , m_jet2);
  ntupVar("pt_jet3"       , pt_jet3);
  ntupVar("pt_jet4p"      , pt_jet4p);
  ntupVar("pt_bjet1"      , pt_bjet1);
  ntupVar("pt_bjet2"      , pt_bjet2);
  ntupVar("pt_bjet3p"     , pt_bjet3p);
  ntupVar("minDPhiAllJetsMet", minDPhiAllJetsMet);

  ntupVar("isSF"          , isSF);
  ntupVar("NLEP"          , nLeptons);
  ntupVar("DX"            , DX);
  ntupVar("eta_jet1"      , eta_jet1);
  ntupVar("eta_jet2"      , eta_jet2);
  ntupVar("eta_jet3"      , eta_jet3);
  ntupVar("eta_bjet1"     , eta_bjet1);
  ntupVar("eta_bjet2"     , eta_bjet2);
  ntupVar("phi_jet1"      , phi_jet1);
  ntupVar("phi_jet2"      , phi_jet2);
  ntupVar("phi_jet3"      , phi_jet3);
  ntupVar("phi_bjet1"     , phi_bjet1);
  ntupVar("phi_bjet2"     , phi_bjet2);
  ntupVar("Z_leptons"     , leptonsFromZ);
  ntupVar("dEtall"        , dEtall);
  ntupVar("dPhib"         , dPhiEtmisspbll);
  ntupVar("dPhiJ1MET"     , dPhiJ1MET);
  ntupVar("dPhiJ2MET"     , dPhiJ2MET);
  ntupVar("dPhiJ1J2MET"   , dPhiJ1J2MET);
  ntupVar("Mtt"           , Mtt);
  ntupVar("lep1_Mother"   , lep1_Mother);
  ntupVar("lep2_Mother"   , lep2_Mother);
  ntupVar("lep3_Mother"   , lep3_Mother);
  ntupVar("PDF_weight"    , PDFReweighting);
  ntupVar("etcone_lep1"   , etcone_lep1);
  ntupVar("ptcone_lep1"   , ptcone_lep1);
  ntupVar("etcone_lep2"   , etcone_lep2);
  ntupVar("ptcone_lep2"   , ptcone_lep2);
  ntupVar("etcone_lep3"   , etcone_lep3);
  ntupVar("ptcone_lep3"   , ptcone_lep3);

  return;
}
