#include "SimpleAnalysis/AnalysisClass.h"
#include "TMath.h"

DefineAnalysis(DMbb2016)

void DMbb2016::Init()
{
  addRegions({"SRB","SRBFH","SRB1","SRB2","SRB3", "SRB4"});
}

void DMbb2016::ProcessEvent(AnalysisEvent *event)
{
    auto candJets   = event->getJets(20., 2.8); //jets
    candJets = filterObjects(candJets, 20, 2.8, JVT50Jet);
    auto electrons =  event->getElectrons(7, 2.47, ELooseBLLH);
    auto muons      = event->getMuons(6, 2.5, MuMedium);
    
    auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };

    // not doing overlap removal between electrons and muons with identical track
    
    candJets   = overlapRemoval(candJets,electrons,0.2,NOT(BTag85MV2c10));
    electrons  = overlapRemoval(electrons,candJets,0.4);
    candJets   = overlapRemoval(candJets, muons, 0.2, LessThan3Tracks);
    muons      = overlapRemoval(muons, candJets, radiusCalcMuon);
    
    if (countObjects(candJets, 20, 2.8, NOT(LooseBadJet))!=0) return;
   
    auto signalElectrons = filterObjects(electrons, 25, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoLooseTrack);
    auto signalMuons     = filterObjects(muons, 25, 2.5, MuD0Sigma3|MuZ05mm|MuIsoLooseTrack);
    auto signalLeptons   = signalElectrons + signalMuons;

    auto metVec     = event->getMET();
    double met   = metVec.Et();

    auto signalJets      = filterObjects(candJets, 20);
    auto signalBjets     = filterObjects(signalJets, 20., 2.5, BTag60MV2c10);

    int nJets  = signalJets.size();
    int nBjets = signalBjets.size();

    if (nJets<1)  return;
    if (signalLeptons.size()!=0) return;
    
    float dphiMin3  = minDphi(metVec, signalJets, 3);
    float dphiMin2  = minDphi(metVec, signalJets, 2);

    //Imbalance
    float Imb = (nBjets>1 ? (signalBjets[0].Pt()-signalBjets[1].Pt()) / (signalBjets[0].Pt()+signalBjets[1].Pt()) : -999);

    //dphibb
    float dphibb =  (nBjets>1 ? fabs(signalBjets[0].DeltaPhi(signalBjets[1])) : -999);

    //j1/HT
    float HT = sumObjectsPt(signalJets);
    float j1HT = signalJets[0].Pt() / HT;
    
    //float x1
    float x1 = dphiMin3 - dphibb;
    
    //float y1
    float y1 = TMath::Pi() - dphiMin3 - dphibb;

    
//    Monojet
    double ht3 = 0;
    for (unsigned int i=2; i<signalJets.size();i++){ht3+=signalJets[i].Pt();}
    
    if (signalJets.size()>1) {
        if (signalBjets.size()>0){
            if (signalBjets[0].Pt()>160.0 && signalJets[0].Pt()>160.0 &&  signalJets[1].Pt()>160.0 && met>650.0 && dphiMin2 >0.6 &&  ht3 < 100){
                accept("SRBFH");
            }
        }
    }
    
    if (nJets<2)  return;
    if (nJets>=4) return;
    if (nBjets<2) return;
    
    if (met < 180.) return;
    
    if(signalJets[0].Pt() < 150 ||  signalJets[1].Pt() < 20 ) return;
    if(nJets >2 &&  signalJets[2].Pt() > 60) return;

    double costheta = fabs(tanh(((signalBjets[0].Eta())-(signalBjets[1].Eta()))/2));

    if(dphiMin3 < 0.4) return;
 
    if( nBjets>=2 && signalJets[0].Pt() > 150.  && signalBjets[0].Pt() >150. && met > 180. && Imb > 0.6 && dphibb > 0.5 && j1HT > 0.75 && x1 < 0 && fabs(y1) < 0.5)  accept("SRB");
    
    if( costheta <= 0.25 && nBjets>=2 && signalJets[0].Pt() > 150.  && signalBjets[0].Pt() >150. && met > 180. && Imb > 0.6 && dphibb > 0.5 && j1HT > 0.75 && x1 < 0 && fabs(y1) < 0.5) {accept("SRB1");}

    if( costheta > 0.25 && costheta <= 0.5 && nBjets>=2 && signalJets[0].Pt() > 150.  && signalBjets[0].Pt() >150. && met > 180. && Imb > 0.6 && dphibb > 0.5 && j1HT > 0.75 && x1 < 0 && fabs(y1) < 0.5)   {  accept("SRB2");}
 
    if( costheta > 0.5 && costheta <= 0.75 && nBjets>=2 && signalJets[0].Pt() > 150.  && signalBjets[0].Pt() >150. && met > 180. && Imb > 0.6 && dphibb > 0.5 && j1HT > 0.75 && x1 < 0 && fabs(y1) < 0.5)   {  accept("SRB3");}

    if( costheta > 0.75 && costheta <= 1 && nBjets>=2 && signalJets[0].Pt() > 150.  && signalBjets[0].Pt() >150. && met > 180. && Imb > 0.6 && dphibb > 0.5 && j1HT > 0.75 && x1 < 0 && fabs(y1) < 0.5)  { accept("SRB4");}
    return;
}

	
	
