#include "SimpleAnalysis/AnalysisClass.h"

DefineAnalysis(TauStrong2016)

#include "SUSYTools/SUSYCrossSection.h"

using namespace std;
void TauStrong2016::Init() {
  // Define signal/control regions
  addRegions({"CompressedSR_1t", "MediumMassSR_1t"});  // 1 tau regions
  addRegions({"CompressedSR_2t", "HighMassSR_2t",
              "OpenShapeSR_2t",  "GMSB_2t"});  // 2 tau regions
  addRegions({"OpenShapeSR_150_200", "OpenShapeSR_200_250",
              "OpenShapeSR_250_300", "OpenShapeSR_300_400",
              "OpenShapeSR_400_500", "OpenShapeSR_500_600",
              "OpenShapeSR_600"});  // ShapeSR bins
}

void TauStrong2016::ProcessEvent(AnalysisEvent *event) {
  // Retrieve basic object lists
  // PLEASE NOTE UNITS ARE ALL IN GEV, AND NOT ATLAS STANDARD MEV!!!

  if (event->getSUSYChannel() == 0) {
    return;
  }

  double weight_signal = 1;

  auto electrons = event->getElectrons(10, 2.47);
  auto muons = event->getMuons(10, 2.7);
  auto jets = event->getJets(20., 2.8);
  auto taus = event->getTaus(20, 2.5, TauOneProng || TauThreeProng);
  auto metVec = event->getMET();
  double met = metVec.Et();
  auto purejets = event->getJets(20, 2.8, NOT(BTag60MV2c10));

  // attempting to reproduce OR from paper
  taus = overlapRemoval(taus, electrons, 0.2);
  taus = overlapRemoval(taus, muons, 0.2);
  electrons = overlapRemoval(electrons, muons, 0.01);
  jets = overlapRemoval(jets, electrons, 0.2, NOT(BTag60MV2c10));
  electrons = overlapRemoval(electrons, jets, 0.4);
  jets = overlapRemoval(jets, muons, 0.2, NOT(BTag60MV2c10));
  muons = overlapRemoval(muons, jets, 0.4);
  jets = overlapRemoval(jets, taus, 0.2);

  auto signalElectrons = filterObjects(electrons, 25, 2.47);
  auto signalMuons = filterObjects(muons, 25, 2.5);
  auto bjets = filterObjects(jets, 20., 2.5, TrueBJet);
  auto signalLeptons = signalElectrons + signalMuons;

  if (taus.size() == 0) {
    return;
  }
  if (jets.size() <= 1) {
    return;
  }

  // define two tiles
  double lbc5_eta1 = 0.9;
  double lbc5_eta2 = 0;
  double lbc5_phi1 = 4. * 2. * 3.14 / 64.;
  double lbc5_phi2 = 5. * 2. * 3.14 / 64.;

  double lba52_eta1 = 0.9;
  double lba52_eta2 = 0;
  double lba52_phi1 = 51. * 2. * 3.14 / 64. - 6.18;
  double lba52_phi2 = 51. * 2. * 3.14 / 64. - 6.18 + 2. * 3.14 / 64.;

  double rjet = 0.4;
  double rtau = 0.2;

  bool lbc = 0;
  bool lba = 0;

  float dphijetmet = 100000;
  int jetcount = 0;

  // Fraction of 2016 data for which tile vetoes are relevant. Change if working
  // with other datasets.
  float lba_w = 23. / 36.1;
  float lbc_w = 18.7 / 36.1;

  // finding jet closest to met in phi plane
  for (int i = 0; i < jets.size(); i++) {
    if (fabs(metVec.DeltaPhi(jets[i])) < dphijetmet) {
      dphijetmet = fabs(metVec.DeltaPhi(jets[i]));
      jetcount = i;
    }
  }

  // veto leading jet/jet closest to met in phi that hit the broken tiles.

  if ((jets[jetcount].Eta() + rjet < lbc5_eta1) &&
      (jets[jetcount].Eta() + rjet > lbc5_eta2) &&
      (fabs(TVector2::Phi_mpi_pi(jets[jetcount].Phi() -
                                 (lbc5_phi2 + lbc5_phi1) / 2.0)) <
       (rjet + (lbc5_phi2 - lbc5_phi1) / 2.0))) {
    lbc = 1;
  }

  if ((jets[jetcount].Eta() + rjet < lba52_eta1) &&
      (jets[jetcount].Eta() + rjet > lba52_eta2) &&
      (fabs(TVector2::Phi_mpi_pi(jets[jetcount].Phi() -
                                 (lba52_phi2 + lba52_phi1) / 2.0)) <
       (rjet + (lba52_phi2 - lba52_phi1) / 2.0))) {
    lba = 1;
  }

  float dphiMin2 = minDphi(metVec, jets, 2);

  bool plateau = 0;
  if (met > 180. && jets.size() > 1 &&
      (jets[0].Pt() > 120. && jets[1].Pt() > 25.) && taus.size() > 0 &&
      dphiMin2 > 0.4) {
    plateau = 1;
  }

  double HT = sumObjectsPt(jets) + sumObjectsPt(taus);
  double tau1_mT = calcMT(taus[0], metVec);
  double tau2_mT = -1.;
  if (taus.size() >= 2) {
    tau2_mT = calcMT(taus[1], metVec);
  }

  float mt2 = -1;

  float mjets = 0;
  for (int i = 0; i < jets.size(); i++) {
    mjets += calcMT(jets[i], metVec);
  }

  bool lba2 = 0;
  bool lbc2 = 0;
  float tile_weight;

  // Compressed 1-tau SR selection

  lba2 = 0;
  lbc2 = 0;
  tile_weight = 1;

  if ((taus[0].Eta() + rtau < lbc5_eta1) &&
      (taus[0].Eta() + rtau > lbc5_eta2) &&
      (fabs(TVector2::Phi_mpi_pi(taus[0].Phi() -
                                 (lbc5_phi2 + lbc5_phi1) / 2.0)) <
       (rtau + (lbc5_phi2 - lbc5_phi1) / 2.0))) {
    lbc2 = 1;
  }

  if ((taus[0].Eta() + rtau < lba52_eta1) &&
      (taus[0].Eta() + rtau > lba52_eta2) &&
      (fabs(TVector2::Phi_mpi_pi(taus[0].Phi() -
                                 (lba52_phi2 + lba52_phi1) / 2.0)) <
       (rtau + (lba52_phi2 - lba52_phi1) / 2.0))) {
    lba2 = 1;
  }

  if (plateau && met > 400 && calcMT(taus[0], metVec) > 80 &&
      taus[0].Pt() < 45 && taus.size() == 1) {
    if (lba || lba2) {
      tile_weight = tile_weight * lba_w;
    }
    if (lbc || lbc2) {
      tile_weight = tile_weight * lbc_w;
    }
    accept("CompressedSR_1t", tile_weight);
  }

  // Medium Mass 1-tau SR selection

  lba2 = 0;
  lbc2 = 0;
  tile_weight = 1;

  if ((taus[0].Eta() + rtau < lbc5_eta1) &&
      (taus[0].Eta() + rtau > lbc5_eta2) &&
      (fabs(TVector2::Phi_mpi_pi(taus[0].Phi() -
                                 (lbc5_phi2 + lbc5_phi1) / 2.0)) <
       (rtau + (lbc5_phi2 - lbc5_phi1) / 2.0))) {
    lbc2 = 1;
  }

  if ((taus[0].Eta() + rtau < lba52_eta1) &&
      (taus[0].Eta() + rtau > lba52_eta2) &&
      (fabs(TVector2::Phi_mpi_pi(taus[0].Phi() -
                                 (lba52_phi2 + lba52_phi1) / 2.0)) <
       (rtau + (lba52_phi2 - lba52_phi1) / 2.0))) {
    lba2 = 1;
  }

  if (plateau && calcMT(taus[0], metVec) > 250 && taus[0].Pt() > 45 &&
      HT > 1000 && met > 400 && taus.size() == 1) {
    if (lba || lba2) {
      tile_weight = tile_weight * lba_w;
    }
    if (lbc || lbc2) {
      tile_weight = tile_weight * lbc_w;
    }
    accept("MediumMassSR_1t", tile_weight);
  }

  lba2 = 0;
  lbc2 = 0;
  tile_weight = 1;

  // Check if taus hit the vetoed tiles

  if (taus.size() >= 2) {
    if ((taus[0].Eta() + rtau < lbc5_eta1) &&
        (taus[0].Eta() + rtau > lbc5_eta2) &&
        (fabs(TVector2::Phi_mpi_pi(taus[0].Phi() -
                                   (lbc5_phi2 + lbc5_phi1) / 2.0)) <
         (rtau + (lbc5_phi2 - lbc5_phi1) / 2.0))) {
      lbc2 = 1;
    }

    if ((taus[0].Eta() + rtau < lba52_eta1) &&
        (taus[0].Eta() + rtau > lba52_eta2) &&
        (fabs(TVector2::Phi_mpi_pi(taus[0].Phi() -
                                   (lba52_phi2 + lba52_phi1) / 2.0)) <
         (rtau + (lba52_phi2 - lba52_phi1) / 2.0))) {
      lba2 = 1;
    }

    if ((taus[1].Eta() + rtau < lbc5_eta1) &&
        (taus[1].Eta() + rtau > lbc5_eta2) &&
        (fabs(TVector2::Phi_mpi_pi(taus[1].Phi() -
                                   (lbc5_phi2 + lbc5_phi1) / 2.0)) <
         (rtau + (lbc5_phi2 - lbc5_phi1) / 2.0))) {
      lbc2 = 1;
    }

    if ((taus[1].Eta() + rtau < lba52_eta1) &&
        (taus[1].Eta() + rtau > lba52_eta2) &&
        (fabs(TVector2::Phi_mpi_pi(taus[1].Phi() -
                                   (lba52_phi2 + lba52_phi1) / 2.0)) <
         (rtau + (lba52_phi2 - lba52_phi1) / 2.0))) {
      lba2 = 1;
    }
  }

  if (lba || lba2) {
    tile_weight = tile_weight * lba_w;
  }
  if (lbc || lbc2) {
    tile_weight = tile_weight * lbc_w;
  }

  // Compressed 2-tau SR selection
  if (taus.size() >= 2) {
    mt2 = calcMT2(taus[0], taus[1], metVec);

    if (plateau && met > 180 && jets[0].Pt() > 120 && jets[1].Pt() > 25 &&
        dphiMin2 > 0.4 && mt2 > 70 && HT < 1100 &&
        ((mjets + calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 1600)) {
      accept("CompressedSR_2t", tile_weight);
    }
  }

  // High Mass 2-tau SR selection
  if (taus.size() >= 2) {
    if (plateau && met > 180 && jets[0].Pt() > 120 && jets[1].Pt() > 25 &&
        dphiMin2 > 0.4 && HT > 1100 &&
        (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 350) {
      accept("HighMassSR_2t", tile_weight);
    }
  }

  // Open SHAPE 2-tau SR selection
  if (taus.size() >= 2) {
    if (plateau && met > 180 && jets[0].Pt() > 120 && jets[1].Pt() > 25 &&
        dphiMin2 > 0.4 &&
        (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 150 && HT > 800) {
      accept("OpenShapeSR_2t", tile_weight);

      if ((calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) < 200) {
        accept("OpenShapeSR_150_200", tile_weight);
      } else if ((calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 200 &&
                 (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) < 250) {
        accept("OpenShapeSR_200_250", tile_weight);
      } else if ((calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 250 &&
                 (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) < 300) {
        accept("OpenShapeSR_250_300", tile_weight);
      } else if ((calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 300 &&
                 (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) < 400) {
        accept("OpenShapeSR_300_400", tile_weight);
      } else if ((calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 400 &&
                 (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) < 500) {
        accept("OpenShapeSR_400_500", tile_weight);
      } else if ((calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 500 &&
                 (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) < 600) {
        accept("OpenShapeSR_500_600", tile_weight);
      } else if ((calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 600) {
        accept("OpenShapeSR_600", tile_weight);
      }
    }
  }

  // GMSB 2-tau SR selection
  if (taus.size() >= 2) {
    if (plateau && met > 180 && jets[0].Pt() > 120 && jets[1].Pt() > 25 &&
        dphiMin2 > 0.4 &&
        (calcMT(taus[0], metVec) + calcMT(taus[1], metVec)) > 150 &&
        HT > 1900) {
      accept("GMSB_2t", tile_weight);
    }
  }

  return;
}
