#include "SimpleAnalysis/AnalysisClass.h"
#include "TMath.h"

DefineAnalysis(DMttTwoLepton2016)

void DMttTwoLepton2016::Init()
{
  addRegions({"SR"});
}

void DMttTwoLepton2016::ProcessEvent(AnalysisEvent *event)
{
    auto candJets   = event->getJets(20., 2.5); //jets
    auto electrons =  event->getElectrons(7, 2.47, ELooseBLLH);

    //Muons
    auto muons      = event->getMuons(6, 2.4, MuMedium);

    // not doing overlap removal between electrons and muons with identical track
    candJets   = overlapRemoval(candJets,electrons,0.2,NOT(BTag85MV2c10));
    electrons  = overlapRemoval(electrons,candJets,0.4);
    candJets   = overlapRemoval(candJets, muons, 0.4, LessThan3Tracks);
    muons      = overlapRemoval(muons, candJets, 0.4);

    if (countObjects(candJets, 20, 2.5, NOT(LooseBadJet))!=0) return;
   
    candJets = filterObjects(candJets, 20, 2.5, JVT50Jet);
    
    auto signalElectrons = filterObjects(electrons, 25, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
    auto signalMuons     = filterObjects(muons, 20, 2.4, MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
    auto signalLeptons   = signalElectrons + signalMuons;

    auto metVec     = event->getMET();
    double met   = metVec.Et();
//
    auto signalJets      = filterObjects(candJets, 20);
    auto signalBjets     = filterObjects(signalJets, 20., 2.5, BTag77MV2c10);

    //    int nJets  = signalJets.size();
    int nBjets = signalBjets.size();
   
    if (signalLeptons.size()!=2) return;
    if(signalLeptons[0].Pt() < 25 ||  signalLeptons[1].Pt() < 20) return;
    if (signalLeptons[0].charge()==signalLeptons[1].charge()) return;
    
    double mll   = (signalLeptons[0] +signalLeptons[1]).M();
    if(mll<20) return;
    
    if (nBjets<1) return;
    
    double mt2   = calcMT2(signalLeptons[0],signalLeptons[1],metVec);
    if(mt2<100.) return;
    
    //Other cuts
    double cem   = mt2+0.2*met;
    double minmbl= ((signalBjets[0]+signalLeptons[0]).M() > (signalBjets[0]+signalLeptons[1]).M()) ? (signalBjets[0]+signalLeptons[1]).M() : (signalBjets[0]+signalLeptons[0]).M();
    
    auto ptll = (metVec + signalLeptons[0] + signalLeptons[1]);
    double deltaphimin = metVec.DeltaPhi(ptll);
        
    if (mt2 >100. && cem >170. && minmbl <170. && fabs(deltaphimin) < 0.8 && signalBjets[0].Pt() > 30.)
        accept("SR");

    return;
}

	
	
