#include "SimpleAnalysis/AnalysisClass.h"
#include <unordered_set>

DefineAnalysis(EwkTwoTau2016)

void EwkTwoTau2016::Init()
{
  addRegions({"SRC1C1","SRC1N2"});
}

void EwkTwoTau2016::ProcessEvent(AnalysisEvent *event)
{
  static int entry=-1;
  entry++;

  auto baselineElectrons  = event->getElectrons(10, 2.47);
  auto baselineMuons      = event->getMuons(10, 2.5);
  auto baselineTaus       = event->getTaus(20., 2.5, TauMedium);
  auto baselineJets       = event->getJets(20., 2.8);
  auto metVec     = event->getMET();
  float met = metVec.Pt();

  auto tightaus       = event->getTaus(20., 2.5, TauTight);

  // Extended SUSY overlap removal      
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineElectrons, 0.05);
  baselineTaus       = overlapRemoval(baselineTaus, baselineElectrons, 0.2);
  baselineTaus       = overlapRemoval(baselineTaus, baselineMuons, 0.2);
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineMuons, 0.01);
  baselineJets       = overlapRemoval(baselineJets, baselineElectrons, 0.2);
  
  if (baselineTaus.size() < 2) return;

  float mll = (baselineTaus[0]+baselineTaus[1]).M();
  if (baselineTaus[0].Pt() < 50 || baselineTaus[1].Pt() < 40) return;
  if (mll < 12) return;
  int njets_B20 = countObjects(baselineJets, 20., 2.5,true);
  if (njets_B20 != 0) return;  //bVeto
  if (fabs(mll-79)<10) return;  //zVeto

  bool isOS = baselineTaus[0].charge()!=baselineTaus[1].charge();
  bool trig1 = (baselineTaus[0].Pt() >50 && baselineTaus[1].Pt() >40 && met>150);
  bool trig2 = (baselineTaus[0].Pt() >95 && baselineTaus[1].Pt() >65);
    float mt2     = calcMT2(baselineTaus[0],baselineTaus[1],metVec);

  if ( (trig1 || trig2) && isOS && tightaus.size()>=1 && mt2>90 && met>110 && baselineTaus[0].Pt()>80 && mll>110) accept("SRC1C1");
  if ( (trig1 ) && isOS && mt2>70 ) accept("SRC1N2");
}

