#include "SimpleAnalysis/AnalysisClass.h"
DefineAnalysis(EwkWhDiphoton2016)
void EwkWhDiphoton2016::Init()
{
  //Define signal regions
  addRegions({"Acc2ph1L", "AccBjetVeto", "SRinc"});
  addRegions({"SR1Lgamgam_a", "SR1Lgamgam_b"});
} 	

/*float EwkWhDiphoton2016::calc_3body_mt(const TLorentzVector& p1, const TLorentzVector& p2, const TLorentzVector& p3)
{
  float dphi12 = fabs(p1.DeltaPhi(p2));
  float dphi13 = fabs(p1.DeltaPhi(p3));
  float dphi23 = fabs(p2.DeltaPhi(p3));
  return sqrt( 2.0 * p1.Et() * p2.Et() * (1.0 - cos( dphi12 )) + 2.0 * p1.Et() * p3.Et() * (1.0 - cos( dphi13 )) + 2.0 * p2.Et() * p3.Et() * (1.0 - cos( dphi23 )) );
}*/

void EwkWhDiphoton2016::ProcessEvent(AnalysisEvent *event)
{
  // Retrieve basic object lists
  // PLEASE NOTE UNITS ARE ALL IN GEV, AND NOT ATLAS STANDARD MEV!!!
  auto electronsBase  = event->getElectrons(15, 2.47, EMediumLH); // Filter on pT, eta and "ID"
  auto photonsBase      = event->getPhotons(27, 2.37, PhotonLoose);
  auto muonsBase      = event->getMuons(10, 2.7, MuMedium);
  auto jetsBase = event->getJets(30., 2.8);
  auto  MET_Vect  = event->getMET();
  //  float eT_miss = MET_Vect.Pt();

  //std::cout <<photonsBase.size()<<"\t"<<electronsBase.size()+muonsBase.size()<<"\t";
  // Overlap removal 
  jetsBase = overlapRemoval(jetsBase, electronsBase, 0.2);
  electronsBase = overlapRemoval(electronsBase, jetsBase, 0.4);
  muonsBase= overlapRemoval(muonsBase, jetsBase, 0.4);
  jetsBase = overlapRemoval(jetsBase, photonsBase, 0.2);
  photonsBase = overlapRemoval(photonsBase, jetsBase, 0.4);
  photonsBase = overlapRemoval(photonsBase, electronsBase, 0.4);
  photonsBase = overlapRemoval(photonsBase, muonsBase, 0.4);
  //std::cout <<photonsBase.size()<<"\t"<<electronsBase.size()+muonsBase.size()<<"\t";
  
  // Basic filtering by pT, eta and "ID"
  auto electronsSignal = filterObjects(electronsBase, 15, 2.47, ETightLH | ED0Sigma5  | EZ05mm  | EIsoGradientLoose);
  auto photonsSignal = filterObjects(photonsBase, 27, 2.37, PhotonTight | PhotonIsoFixedCutTight);
  auto muonsSignal = filterObjects(muonsBase, 10, 2.7, MuMedium | MuIsoGradientLoose | MuD0Sigma3 | MuZ05mm );
  auto jetsSignal = filterObjects(jetsBase, 30, 2.8, JVT50Jet);
  auto bJetsSignal = filterObjects(jetsSignal,30.,2.8,BTag77MV2c10 | GoodBJet);

  // Remove objects in crack region
  photonsSignal = filterCrack(photonsSignal, 1.37, 1.52);
  electronsSignal = filterCrack(electronsSignal, 1.37, 1.52);
  
  unsigned int N_Electrons = electronsSignal.size();
  unsigned int N_Photons = photonsSignal.size();
  unsigned int N_Muons = muonsSignal.size();
  //  unsigned int N_Jets = jetsSignal.size();
  unsigned int N_bJets = bJetsSignal.size();
  unsigned int N_Lep = N_Electrons + N_Muons;
  
  // Jet Cleaning 
  if (countObjects(jetsBase, 20, 2.8,NOT(LooseBadJet)) != 0) return;
 

  // Reject events with cosmic muon:
  if (countObjects(muonsBase, 10, 2.4, NOT(MuNotCosmic)) !=0) return; 

  if(N_Photons!=2||N_Lep!=1)return;
  accept("Acc2ph1L");
  if(N_bJets!=0)return;
  accept("AccBjetVeto");

  auto leptonSignal = electronsSignal + muonsSignal;// only one object in those vector.
  
  sortObjectsByPt(photonsSignal);
  //auto mt_W_ph1 = calc_3body_mt(photonsSignal[0], leptonSignal[0], MET_Vect);
  float mt_W_ph1 = calcMT(photonsSignal[0],leptonSignal[0]) + calcMT(photonsSignal[0],MET_Vect);
  //auto mt_W_ph2 = calc_3body_mt(photonsSignal[1], leptonSignal[0], MET_Vect);
  float mt_W_ph2 = calcMT(photonsSignal[1],leptonSignal[0]) + calcMT(photonsSignal[1],MET_Vect);

  float mt_W = calcMT(leptonSignal[0],MET_Vect);

  auto W_Vect = MET_Vect + leptonSignal[0];
  auto yy_Vect = photonsSignal[0] + photonsSignal[1];
  
  auto dphi_WH = fabs(yy_Vect.DeltaPhi(W_Vect));
  if(photonsSignal[0].Pt()>40 && photonsSignal[1].Pt()>31 && leptonSignal[0].Pt()>25 && dphi_WH>2.25 && yy_Vect.M()>120 
      && yy_Vect.M()<130 && mt_W_ph1>150 && mt_W_ph2>80){
    accept("SRinc");
    if(mt_W_ph2>140&&mt_W>110)accept("SR1Lgamgam_a");
    else accept("SR1Lgamgam_b");
  }
  return;
}
