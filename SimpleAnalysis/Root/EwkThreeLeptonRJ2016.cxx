#include "SimpleAnalysis/AnalysisClass.h"
#ifdef ROOTCORE_PACKAGE_Ext_RestFrames

DefineAnalysis(EwkThreeLeptonRJ2016)

void EwkThreeLeptonRJ2016::Init() {
    // add signal regions
    addRegions({"SR3L_High","SR3L_Int","SR3L_Low","SR3L_ISR"});

    // set up RestFrames using the helper
    LabRecoFrame* LAB_3L  = m_RF_helper.addLabFrame("LAB_3L");
    DecayRecoFrame* C1N2_3L = m_RF_helper.addDecayFrame("C1N2_3L");
    DecayRecoFrame* C1a_3L  = m_RF_helper.addDecayFrame("C1a_3L");
    DecayRecoFrame* N2b_3L  = m_RF_helper.addDecayFrame("N2b_3L");

    VisibleRecoFrame* L1a_3L = m_RF_helper.addVisibleFrame("L1a_3L");
    VisibleRecoFrame* L1b_3L = m_RF_helper.addVisibleFrame("L1b_3L");
    VisibleRecoFrame* L2b_3L = m_RF_helper.addVisibleFrame("L2b_3L");

    InvisibleRecoFrame* X1a_3L  = m_RF_helper.addInvisibleFrame("X1a_3L");
    InvisibleRecoFrame* X1b_3L  = m_RF_helper.addInvisibleFrame("X1b_3L");


    LAB_3L->SetChildFrame(*C1N2_3L);

    C1N2_3L->AddChildFrame(*C1a_3L);
    C1N2_3L->AddChildFrame(*N2b_3L);

    C1a_3L->AddChildFrame(*L1a_3L);
    C1a_3L->AddChildFrame(*X1a_3L);

    N2b_3L->AddChildFrame(*L1b_3L);
    N2b_3L->AddChildFrame(*L2b_3L);
    N2b_3L->AddChildFrame(*X1b_3L);


    LAB_3L->InitializeTree();

    //setting the invisible components
    InvisibleGroup* INV_3L = m_RF_helper.addInvisibleGroup("INV_3L");
    INV_3L->AddFrame(*X1a_3L);
    INV_3L->AddFrame(*X1b_3L);

    // Set di-LSP mass to minimum Lorentz-invariant expression
    InvisibleJigsaw* X1_mass_3L = m_RF_helper.addInvisibleJigsaw("X1_mass_3L", kSetMass);
    INV_3L->AddJigsaw(*X1_mass_3L);



    // Set di-LSP rapidity to that of visible particles and neutrino
    InvisibleJigsaw* X1_eta_3L = m_RF_helper.addInvisibleJigsaw("X1_eta_3L", kSetRapidity);
    INV_3L->AddJigsaw(*X1_eta_3L);
    X1_eta_3L->AddVisibleFrames(C1N2_3L->GetListVisibleFrames());



    InvisibleJigsaw* X1X1_contra_3L = m_RF_helper.addInvisibleJigsaw("X1X1_contra_3L",kContraBoost);
    INV_3L->AddJigsaw(*X1X1_contra_3L);
    X1X1_contra_3L->AddVisibleFrames(C1a_3L->GetListVisibleFrames(),0);
    X1X1_contra_3L->AddVisibleFrames(N2b_3L->GetListVisibleFrames(),1);
    X1X1_contra_3L->AddInvisibleFrames(C1a_3L->GetListInvisibleFrames(),0);
    X1X1_contra_3L->AddInvisibleFrames(N2b_3L->GetListInvisibleFrames(),1);

    LAB_3L->InitializeAnalysis();

    // Compressed tree time
    // combinatoric (transverse) tree
    // for jet assignment
    LabRecoFrame* LAB_comb = m_RF_helper.addLabFrame("LAB_comb");
    DecayRecoFrame* CM_comb  = m_RF_helper.addDecayFrame("CM_comb");
    DecayRecoFrame* S_comb   = m_RF_helper.addDecayFrame("S_comb");
    VisibleRecoFrame* ISR_comb = m_RF_helper.addVisibleFrame("ISR_comb");
    VisibleRecoFrame* J_comb   =  m_RF_helper.addVisibleFrame("J_comb");
    VisibleRecoFrame* L_comb   = m_RF_helper.addVisibleFrame("L_comb");
    InvisibleRecoFrame* I_comb   =  m_RF_helper.addInvisibleFrame("I_comb");
    //cout << "test 5" << endl;
    LAB_comb->SetChildFrame(*CM_comb);
    CM_comb->AddChildFrame(*ISR_comb);
    CM_comb->AddChildFrame(*S_comb);
    S_comb->AddChildFrame(*L_comb);
    S_comb->AddChildFrame(*J_comb);
    S_comb->AddChildFrame(*I_comb);
    // cout << "test 6" << endl;
    LAB_comb->InitializeTree();

    // 2L+1L tree (Z->ll + Z/W->l)
    LabRecoFrame* LAB_2L1L = m_RF_helper.addLabFrame("LAB_2L1L");
    DecayRecoFrame* CM_2L1L  =  m_RF_helper.addDecayFrame("CM_2L1L");
    DecayRecoFrame* S_2L1L   = m_RF_helper.addDecayFrame("S_2L1L");
    VisibleRecoFrame* ISR_2L1L = m_RF_helper.addVisibleFrame("ISR_2L1L");
    DecayRecoFrame* Ca_2L1L  = m_RF_helper.addDecayFrame("Ca_2L1L");
    DecayRecoFrame* Z_2L1L   = m_RF_helper.addDecayFrame("Z_2L1L");
    VisibleRecoFrame* L1_2L1L  = m_RF_helper.addVisibleFrame("L1_2L1L");
    VisibleRecoFrame* L2_2L1L  = m_RF_helper.addVisibleFrame("L2_2L1L");
    DecayRecoFrame* Cb_2L1L  = m_RF_helper.addDecayFrame("Cb_2L1L");
    VisibleRecoFrame* Lb_2L1L  = m_RF_helper.addVisibleFrame("Lb_2L1L");
    InvisibleRecoFrame* Ia_2L1L  = m_RF_helper.addInvisibleFrame("Ia_2L1L");
    InvisibleRecoFrame* Ib_2L1L  = m_RF_helper.addInvisibleFrame("Ib_2L1L");

    LAB_2L1L->SetChildFrame(*CM_2L1L);
    CM_2L1L->AddChildFrame(*ISR_2L1L);
    CM_2L1L->AddChildFrame(*S_2L1L);
    S_2L1L->AddChildFrame(*Ca_2L1L);
    S_2L1L->AddChildFrame(*Cb_2L1L);
    Ca_2L1L->AddChildFrame(*Z_2L1L);
    Ca_2L1L->AddChildFrame(*Ia_2L1L);
    Z_2L1L->AddChildFrame(*L1_2L1L);
    Z_2L1L->AddChildFrame(*L2_2L1L);
    Cb_2L1L->AddChildFrame(*Lb_2L1L);
    Cb_2L1L->AddChildFrame(*Ib_2L1L);

    LAB_2L1L->InitializeTree();

    ////////////// Jigsaw rules set-up /////////////////

    // combinatoric (transverse) tree
    // for jet assignment
    InvisibleGroup* INV_comb = m_RF_helper.addInvisibleGroup("INV_comb");
    INV_comb->AddFrame(*I_comb);

    InvisibleJigsaw* InvMass_comb = m_RF_helper.addInvisibleJigsaw("InvMass_comb", kSetMass);
    INV_comb->AddJigsaw(*InvMass_comb);

    CombinatoricGroup* JETS_comb = m_RF_helper.addCombinatoricGroup("JETS_comb");
    JETS_comb->AddFrame(*ISR_comb);
    JETS_comb->SetNElementsForFrame(*ISR_comb, 1,false);
    JETS_comb->AddFrame(*J_comb);
    JETS_comb->SetNElementsForFrame(*J_comb, 0,false);

    MinMassesCombJigsaw* SplitJETS_comb = m_RF_helper.addCombinatoricJigsaw("SplitJETS_comb", kMinMasses);
    JETS_comb->AddJigsaw(*SplitJETS_comb);
    SplitJETS_comb->AddFrame(*ISR_comb, 0);
    SplitJETS_comb->AddFrame(*J_comb, 1);
    SplitJETS_comb->AddObjectFrame(*ISR_comb,0);
    SplitJETS_comb->AddObjectFrame(*S_comb,1);

    LAB_comb->InitializeAnalysis();

    // 2L+1L tree (Z->ll + Z/W->l)
    InvisibleGroup* INV_2L1L = m_RF_helper.addInvisibleGroup("INV_2L1L");
    INV_2L1L->AddFrame(*Ia_2L1L);
    INV_2L1L->AddFrame(*Ib_2L1L);

    InvisibleJigsaw* InvMass_2L1L = m_RF_helper.addInvisibleJigsaw("InvMass_2L1L", kSetMass);
    INV_2L1L->AddJigsaw(*InvMass_2L1L);
    InvisibleJigsaw* InvRapidity_2L1L = m_RF_helper.addInvisibleJigsaw("InvRapidity_2L1L", kSetRapidity);
    INV_2L1L->AddJigsaw(*InvRapidity_2L1L);
    InvRapidity_2L1L->AddVisibleFrames(S_2L1L->GetListVisibleFrames());
    InvisibleJigsaw* SplitINV_2L1L = m_RF_helper.addInvisibleJigsaw("SplitINV_2L1L", kContraBoost);
    INV_2L1L->AddJigsaw(*SplitINV_2L1L);
    SplitINV_2L1L->AddVisibleFrames(Ca_2L1L->GetListVisibleFrames(), 0);
    SplitINV_2L1L->AddVisibleFrames(Cb_2L1L->GetListVisibleFrames(), 1);
    SplitINV_2L1L->AddInvisibleFrame(*Ia_2L1L, 0);
    SplitINV_2L1L->AddInvisibleFrame(*Ib_2L1L, 1);

    LAB_2L1L->InitializeAnalysis();



}

void EwkThreeLeptonRJ2016::ProcessEvent(AnalysisEvent *event) {

    auto electrons = event->getElectrons(10.,2.47, ELooseLH);
    auto muons = event->getMuons(10.,2.7, MuMedium);
    auto loosejets = event->getJets(20.,4.5);
    auto loosebjets = filterObjects(loosejets, 20., 4.5, BTag77MV2c10);
    auto metVec = event->getMET();
    //    double met = metVec.Et();

    // Overlap removal 
    auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };

    electrons = overlapRemoval(electrons,muons,0.01);
    electrons = overlapRemoval(electrons,loosebjets,0.2,BTag85MV2c10);
    loosejets = overlapRemoval(loosejets,electrons,0.2,NOT(BTag85MV2c10));
    electrons = overlapRemoval(electrons,loosejets,0.4);
    loosejets = overlapRemoval(loosejets,muons,radiusCalcJet,LessThan3Tracks);
    muons = overlapRemoval(muons, loosejets, 0.4);

    // Filter objects to selection standard 
    auto sigelectrons = filterObjects(electrons,10,2.47,EMediumLH|ED0Sigma5|EZ05mm|EIsoBoosted);
    auto sigmuons = filterObjects(muons,10,2.4, MuD0Sigma3|MuZ05mm|MuIsoBoosted|MuNotCosmic);
    auto jets = filterObjects(loosejets,20,2.4);
    auto bjets = filterObjects(loosebjets,20,2.4);
    auto leptons = sigelectrons+sigmuons;

    // sort objects by pt just in case 
    sortObjectsByPt( jets );
    sortObjectsByPt( leptons );

    if (leptons.size()!=3) {
        return;
    }

    int njets = jets.size();

    int nbjets = bjets.size();

    if (nbjets>0) return;

    // some preselections 
    auto lep1 = leptons[0];
    auto lep2 = leptons[1];
    auto lep3 = leptons[2];

    if (lep1.Pt()<25.) return;
    if (lep2.Pt()<25.) return;
    if (lep3.Pt()<20.) return;

    bool iscomp = false;
    if (njets>0 && nbjets==0) iscomp = true;

    // let's do the open tree
    LabRecoFrame* LAB_3L = m_RF_helper.getLabFrame("LAB_3L");
    InvisibleGroup* INV_3L = m_RF_helper.getInvisibleGroup("INV_3L");
    VisibleRecoFrame* L1a_3L = m_RF_helper.getVisibleFrame("L1a_3L");
    VisibleRecoFrame* L1b_3L = m_RF_helper.getVisibleFrame("L1b_3L");
    VisibleRecoFrame* L2b_3L = m_RF_helper.getVisibleFrame("L2b_3L");

    LAB_3L->ClearEvent();

    // SFOS criteria
    double diff = 10000000000.0;
    int Zlep1 = -99;
    int Zlep2 = -99;
    double Zmass = -999.0;
    bool foundSFOS = false;

    for(unsigned int i=0; i<leptons.size(); i++)
    {
        for(unsigned int j=i+1; j<leptons.size(); j++)
        {
            if(IsSFOS(leptons[i],leptons[j]))
            {
                double mass = (leptons[i]+leptons[j]).M();
                double massdiff = fabs(mass-91.1876);

                if(massdiff<diff)
                {
                    diff=massdiff;
                    Zmass=mass;
                    Zlep1 = i;
                    Zlep2 = j;
                    foundSFOS = true;
                }
            }


        }
    }

    if(!foundSFOS)
    {

        return;
    }
    int Wlep1 = -999;
    if( (Zlep1==0 && Zlep2==1) || (Zlep1==1 && Zlep2==0) ) Wlep1=2;
    else if( (Zlep1==0 && Zlep2==2) || (Zlep1==2 && Zlep2==0) ) Wlep1=1;
    else if((Zlep1==1 && Zlep2==2) || (Zlep1==2 && Zlep2==1) ) Wlep1=0;
    double mll = Zmass;
    double wlepMetphi = leptons[Wlep1].DeltaPhi(metVec);
    double mTW = sqrt(2*leptons[Wlep1].Pt()*metVec.Pt()*(1-TMath::Cos(wlepMetphi)));

    // Send the leptons to their place
    L1a_3L->SetLabFrameFourVector(leptons[Wlep1]);
    L1b_3L->SetLabFrameFourVector(leptons[Zlep1]);
    L2b_3L->SetLabFrameFourVector(leptons[Zlep2]);

    // met
    INV_3L->SetLabFrameThreeVector(metVec.Vect());

    LAB_3L->AnalyzeEvent();

    // Set up the variables we cut on

    // PP frame 
    DecayRecoFrame* C1N2_3L = m_RF_helper.getDecayFrame("C1N2_3L");
    InvisibleRecoFrame* X1a_3L = m_RF_helper.getInvisibleFrame("X1a_3L");
    InvisibleRecoFrame* X1b_3L = m_RF_helper.getInvisibleFrame("X1b_3L");

    TLorentzVector vP_V1aPP  = L1a_3L->GetFourVector(*C1N2_3L);
    TLorentzVector vP_V1bPP  = L1b_3L->GetFourVector(*C1N2_3L);
    TLorentzVector vP_V2bPP  = L2b_3L->GetFourVector(*C1N2_3L);
    TLorentzVector vP_I1aPP  = X1a_3L->GetFourVector(*C1N2_3L);
    TLorentzVector vP_I1bPP  = X1b_3L->GetFourVector(*C1N2_3L);

    double H4PP = vP_V1aPP.P() + vP_V1bPP.P() + vP_V2bPP.P() + (vP_I1aPP + vP_I1bPP).P();//H(3,1)PP
    double HT4PP = vP_V1aPP.Pt() + vP_V1bPP.Pt() + vP_V2bPP.Pt() + (vP_I1aPP + vP_I1bPP).Pt();//HT(3,1)PP

    double R_HT4PP_H4PP = HT4PP/H4PP;

    TVector3 vP_PP = C1N2_3L->GetFourVector(*LAB_3L).Vect();
    double Pt_PP = vP_PP.Pt();
    double RPT_HT4PP = Pt_PP/(Pt_PP + HT4PP);

    // P frame
    DecayRecoFrame* C1a_3L = m_RF_helper.getDecayFrame("C1a_3L");
    DecayRecoFrame* N2b_3L = m_RF_helper.getDecayFrame("N2b_3L"); 
    TLorentzVector vP_V1aPa  = L1a_3L->GetFourVector(*C1a_3L);
    TLorentzVector vP_I1aPa  = X1a_3L->GetFourVector(*C1a_3L);

    TLorentzVector vP_V1bPb = L1b_3L->GetFourVector(*N2b_3L);
    TLorentzVector vP_V2bPb = L2b_3L->GetFourVector(*N2b_3L);
    TLorentzVector vP_I1bPb = X1b_3L->GetFourVector(*N2b_3L);

    //double H2Pa = (vP_V1aPa).P() + (vP_I1aPa).P(); //H(1,1)P
    double H2Pb = (vP_V1bPb + vP_V2bPb).P() + vP_I1bPb.P();//H(1,1)P

    //double H3Pa = vP_V1aPa.P() + vP_I1aPa.P();//H(1,1)P
    double H3Pb = vP_V1bPb.P() + vP_V2bPb.P() + vP_I1bPb.P();//H(2,1)P

    //double minH2P = std::min(H2Pa,H2Pb);
    //double minH3P = std::min(H3Pa,H3Pb);
    //double R_H2Pa_H2Pb = H2Pa/H2Pb;
    //double R_H3Pa_H3Pb = H3Pa/H3Pb;
    double R_minH2P_minH3P = H2Pb/H3Pb;

    // we are ready to go

    // Low mass
    if (njets==0 && leptons[0].Pt()>60. && leptons[1].Pt()>40. && leptons[2].Pt()>30. && mll>75. && mll<105. && mTW>100. && H4PP>250. && RPT_HT4PP<0.05 && R_HT4PP_H4PP>0.9) accept("SR3L_Low");

    // Intermediate
    if (njets<3 && leptons[0].Pt()>60. && leptons[1].Pt()>50. && leptons[2].Pt()>30. && mll>75. && mll<105.&& mTW>130. && H4PP>450. && RPT_HT4PP<0.15 && R_HT4PP_H4PP>0.8 && R_minH2P_minH3P>0.75) accept("SR3L_Int");

    // High
    if (njets<3 && leptons[0].Pt()>60. && leptons[1].Pt()>60. && leptons[2].Pt()>40. && mll>75. && mll<105.&& mTW>150. && H4PP>550. && RPT_HT4PP<0.2 && R_HT4PP_H4PP>0.75 && R_minH2P_minH3P>0.8) accept("SR3L_High");


    // Compressed time 
    if(iscomp) {
        // Combinatoric setup for jets
        LabRecoFrame* LAB_comb = m_RF_helper.getLabFrame("LAB_comb");
        //VisibleRecoFrame* ISR_comb = m_RF_helper.getVisibleFrame("ISR_comb");
        CombinatoricGroup* JETS_comb = m_RF_helper.getCombinatoricGroup("JETS_comb");    
        VisibleRecoFrame* J_comb = m_RF_helper.getVisibleFrame("J_comb");
        VisibleRecoFrame* L_comb = m_RF_helper.getVisibleFrame("L_comb");
        InvisibleGroup* INV_comb = m_RF_helper.getInvisibleGroup("INV_comb");
        LAB_comb->ClearEvent();

        // do mass minimisation for jets 
        std::vector<RFKey> jetID;
        for (const auto jet : jets ) {
            jetID.push_back(JETS_comb->AddLabFrameFourVector(jet.transFourVect()));
        } 

        TLorentzVector lepSys(0.,0.,0.,0.);
        for(const auto lep1 : leptons){
	  lepSys = lepSys + lep1.transFourVect();
        }
        L_comb->SetLabFrameFourVector(lepSys);

        INV_comb->SetLabFrameThreeVector(metVec.Vect());

        LAB_comb->AnalyzeEvent();

        LabRecoFrame* LAB_2L1L = m_RF_helper.getLabFrame("LAB_2L1L");
        VisibleRecoFrame* ISR_2L1L = m_RF_helper.getVisibleFrame("ISR_2L1L");
        VisibleRecoFrame* L1_2L1L = m_RF_helper.getVisibleFrame("L1_2L1L");
        VisibleRecoFrame* L2_2L1L = m_RF_helper.getVisibleFrame("L2_2L1L");
        VisibleRecoFrame* Lb_2L1L = m_RF_helper.getVisibleFrame("Lb_2L1L");
        InvisibleGroup* INV_2L1L = m_RF_helper.getInvisibleGroup("INV_2L1L");
        DecayRecoFrame* CM_2L1L = m_RF_helper.getDecayFrame("CM_2L1L");
        InvisibleRecoFrame* Ia_2L1L = m_RF_helper.getInvisibleFrame("Ia_2L1L");       
        InvisibleRecoFrame* Ib_2L1L = m_RF_helper.getInvisibleFrame("Ib_2L1L");       


        LAB_2L1L->ClearEvent();

        TLorentzVector vISR(0.0,0.0,0.0,0.0);
        //std::cout << "no of jets: " << jetID.size() << std::endl;
        for (unsigned int i=0; i<jets.size(); i++) {
            if (JETS_comb->GetFrame(jetID[i])!=*J_comb) vISR += jets[i];
        }
        ISR_2L1L->SetLabFrameFourVector(vISR);

        L1_2L1L->SetLabFrameFourVector(leptons[Zlep1]);
        L2_2L1L->SetLabFrameFourVector(leptons[Zlep2]);
        Lb_2L1L->SetLabFrameFourVector(leptons[Wlep1]);

        INV_2L1L->SetLabFrameThreeVector(metVec.Vect());

        LAB_2L1L->AnalyzeEvent();

        // Make the variables 
        TLorentzVector vP_CM;
        TLorentzVector vP_ISR;
        TLorentzVector vP_Ia;
        TLorentzVector vP_Ib;
        TLorentzVector vP_I;
        vP_Ia = Ia_2L1L->GetFourVector();
        vP_Ib = Ib_2L1L->GetFourVector();
        vP_CM  = CM_2L1L->GetFourVector();
        vP_ISR = ISR_2L1L->GetFourVector();
        vP_I   = vP_Ia + vP_Ib;

        double PTCM = vP_CM.Pt();

        TVector3 boostZ = vP_CM.BoostVector();
        boostZ.SetX(0.);
        boostZ.SetY(0.);

        vP_CM.Boost(-boostZ);
        vP_ISR.Boost(-boostZ);
        vP_I.Boost(-boostZ);

        TVector3 boostT = vP_CM.BoostVector();
        vP_ISR.Boost(-boostT);
        vP_I.Boost(-boostT);

        TVector3 vPt_ISR = vP_ISR.Vect();
        TVector3 vPt_I   = vP_I.Vect();
        vPt_ISR -= vPt_ISR.Dot(boostZ.Unit())*boostZ.Unit();
        vPt_I   -= vPt_I.Dot(boostZ.Unit())*boostZ.Unit();

        double PTISR =  vPt_ISR.Mag();
        double RISR  = -vPt_I.Dot(vPt_ISR.Unit()) / PTISR;
        double PTI = vPt_I.Mag();
        double dphiISRI = fabs(vPt_ISR.Angle(vPt_I));

        // finally do the selections
        if (njets<4 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && leptons[2].Pt()>20.&& mll>75. && mll<105. && mTW>100. && dphiISRI>2.0 && RISR>0.55 && RISR<1.0 && PTISR>100. && PTI>80. && PTCM<25.) accept("SR3L_ISR");
    }
    return;
}

#endif
