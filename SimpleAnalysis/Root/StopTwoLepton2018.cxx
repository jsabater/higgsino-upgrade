#include "SimpleAnalysis/AnalysisClass.h"
#ifdef ROOTCORE_PACKAGE_Ext_RestFrames

DefineAnalysis(StopTwoLepton2018)

void StopTwoLepton2018::Init()
{
  // 3-body CR/VR/SRs
  addRegions({"CR3BodyT", "VR3BodyT", "CR3BodyVVDF", "CR3BodyVVSF", "VR3BodyVVDF", "VR3BodyVVSF", "SR3BodyTSF", "SR3BodyTDF", "SR3BodyT", "SR3BodyWSF", "SR3BodyWDF"});
  addHistogram("MET",100,0,2000);
  addHistogram("genMET",100,0,2000);
  addHistogram("METPreSelect",100,0,2000);
  addHistogram("METCR3BodyT",100,0,2000);
  addHistogram("METVR3BodyT",100,0,2000);
  addHistogram("METCR3BodyVVDF",100,0,2000);
  addHistogram("METCR3BodyVVSF",100,0,2000);
  addHistogram("METVR3BodyVVDF",100,0,2000);
  addHistogram("METVR3BodyVVSF",100,0,2000);
  addHistogram("METSR3BodyTSF",100,0,2000);
  addHistogram("METSR3BodyTDF",100,0,2000);
  addHistogram("METSR3BodyT",100,0,2000);
  addHistogram("METSR3BodyWSF",100,0,2000);
  addHistogram("METSR3BodyWDF",100,0,2000);

  // 4-body SR
  addRegions({"preselection4body", "CR4BodyT", "VR4BodyT", "CR4BodyVV", "VR4BodyVV", "VR4bodyVV3L", "SRA4body", "SRB4body"});
  addHistogram("METPreSelect4body",100,0,2000);
  addHistogram("ptLep1PreSelect4body",100,0,100);
  addHistogram("ptLep2PreSelect4body",100,0,100);
  addHistogram("ptLep1_SRA",100,0,100);
  addHistogram("ptLep2_SRA",100,0,100);
  addHistogram("ptLep1_SRB",100,0,100);
  addHistogram("ptLep2_SRB",100,0,100);


  addRegions({"preselection2body", "SR2BodySF0", "SR2BodyDF0", "SR2BodySF1", "SR2BodyDF1", "SR2BodySF2", "SR2BodyDF2","SR2BodySF3", "SR2BodyDF3","SR2BodySF4", "SR2BodyDF4","SR2BodySF5", "SR2BodyDF5"});
  addRegions({"SR2BodyDisc0","SR2BodyDisc1","SR2BodyDisc2","SR2BodyDisc3","SR2BodyDisc4","SR2BodyDisc5","SR2BodyDisc6",});
  // Set up RestFrames trees through m_RF_Helper (RestFramesHelper class)
  //
  LabRecoFrame*       lab  = m_RF_helper.addLabFrame("lab");
  DecayRecoFrame*     ss   = m_RF_helper.addDecayFrame("ss");
  DecayRecoFrame*     s1   = m_RF_helper.addDecayFrame("s1");
  DecayRecoFrame*     s2   = m_RF_helper.addDecayFrame("s2");
  VisibleRecoFrame*   v1   = m_RF_helper.addVisibleFrame("v1");
  VisibleRecoFrame*   v2   = m_RF_helper.addVisibleFrame("v2");
  InvisibleRecoFrame* i1   = m_RF_helper.addInvisibleFrame("i1");
  InvisibleRecoFrame* i2   = m_RF_helper.addInvisibleFrame("i2");

  // Connect the frames
  lab->SetChildFrame(*ss);
  ss->AddChildFrame(*s1);
  ss->AddChildFrame(*s2);
  s1->AddChildFrame(*v1);
  s1->AddChildFrame(*i1);
  s2->AddChildFrame(*v2);
  s2->AddChildFrame(*i2);

  // Initialize the tree
  lab->InitializeTree();

  // Define groups
  InvisibleGroup* inv = m_RF_helper.addInvisibleGroup("inv");
  inv->AddFrame(*i1);
  inv->AddFrame(*i2);

  CombinatoricGroup* vis = m_RF_helper.addCombinatoricGroup("vis");
  vis->AddFrame(*v1);
  vis->SetNElementsForFrame(*v1, 1, false);
  vis->AddFrame(*v2);
  vis->SetNElementsForFrame(*v2, 1, false);

  InvisibleJigsaw* MinMassJigsaw = m_RF_helper.addInvisibleJigsaw("MinMassJigsaw", kSetMass);
  inv->AddJigsaw(*MinMassJigsaw);

  InvisibleJigsaw* RapidityJigsaw = m_RF_helper.addInvisibleJigsaw("RapidityJigsaw", kSetRapidity);
  inv->AddJigsaw(*RapidityJigsaw);
  RapidityJigsaw->AddVisibleFrames(lab->GetListVisibleFrames());

  InvisibleJigsaw* ContraBoostJigsaw = m_RF_helper.addInvisibleJigsaw("ContraBoostJigsaw", kContraBoost);
  inv->AddJigsaw(*ContraBoostJigsaw);
  ContraBoostJigsaw->AddVisibleFrames((s1->GetListVisibleFrames()), 0);
  ContraBoostJigsaw->AddVisibleFrames((s2->GetListVisibleFrames()), 1);
  ContraBoostJigsaw->AddInvisibleFrame(*i1, 0);
  ContraBoostJigsaw->AddInvisibleFrame(*i2, 1);

  MinMassesCombJigsaw* HemiJigsaw = m_RF_helper.addCombinatoricJigsaw("HemiJigsaw", kMinMasses);
  vis->AddJigsaw(*HemiJigsaw);
  HemiJigsaw->AddFrame(*v1, 0);
  HemiJigsaw->AddFrame(*v2, 1);

  // Initialize analysis
  lab->InitializeAnalysis();
}

void StopTwoLepton2018::ProcessEvent(AnalysisEvent *event)
{



  const float mZ = 91.2;

  auto baselineElectrons = event->getElectrons(4.5, 2.47, ELooseBLLH | EZ05mm);     //Jared: event->getElectrons(4.5, 2.47, ELooseBLLH|ED0Sigma5|EZ05mm);
  auto baselineMuons = event->getMuons(4., 2.7, MuMedium | MuZ05mm);
  auto jets          = event->getJets(20., 2.8); ///JVT cut to be added
  auto metVec        = event->getMET();
  double metSignificance =  event->getMETSignificance(); 
  

  //Sliding delta-R cone for boosted lepton analyses
  auto radiusCalcEl = [] (const AnalysisObject& ele, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/ele.Pt()); };
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  AnalysisObjects jetsLower100;
  for (const auto& jet : jets){if(jet.Pt()<=100) jetsLower100.push_back(jet);}
  AnalysisObjects jetsHigher100;
  for (const auto& jet : jets){if(jet.Pt()>100) jetsHigher100.push_back(jet);}
  jets               = overlapRemoval(jetsLower100, baselineElectrons, 0.2, NOT(BTag85MV2c10)); //bORaware 
  for (const auto& jet : jetsHigher100){ jets.push_back(jet);};

  baselineElectrons  = overlapRemoval(baselineElectrons, jets, radiusCalcEl);
  jets               = overlapRemoval(jets, baselineMuons, 0.2, LessThan3Tracks);
  baselineMuons      = overlapRemoval(baselineMuons, jets, radiusCalcMuon);

  //UPTOHERE
  auto signalElectrons = filterObjects(baselineElectrons, 20., 2.47, EMediumLH | ED0Sigma5 | EIsoGradient);
  auto signalMuons     = filterObjects(baselineMuons, 20., 2.4, MuD0Sigma3 | MuIsoFixedCutLoose); //2.4 needed for the trigger coverage

  auto signalElectrons_fourBody = filterObjects(baselineElectrons, 4.5, 2.47, EMediumLH | ED0Sigma5 | EIsoGradient);
  auto signalMuons_fourBody     = filterObjects(baselineMuons, 4, 2.7, MuD0Sigma3 | MuIsoFixedCutLoose);

  auto bjets           = filterObjects(jets, 20., 2.8, BTag77MV2c10);
  jets            = filterObjects(jets, 20., 2.8);

  auto n_bjets          = countObjects(bjets, 20, 2.8);
  auto n_jets20         = countObjects(jets, 20, 2.8);

  AnalysisObjects signalLeptons = signalElectrons + signalMuons;
  AnalysisObjects signalLeptons_fourBody = signalElectrons_fourBody + signalMuons_fourBody;

  sortObjectsByPt(signalLeptons);
  sortObjectsByPt(signalLeptons_fourBody);
  sortObjectsByPt(jets);
  sortObjectsByPt(bjets);

  int nLeptons_fourBody = signalLeptons_fourBody.size();
  int nLeptons = signalLeptons.size();

  double gen_met   = event->getGenMET();
  if ( event->getMCNumber()==410472 && gen_met > 200000 ) return;
  if ( event->getMCNumber()==407345 && ( gen_met < 200000 || gen_met > 300000 ) ) return;
  if ( event->getMCNumber()==407346 && ( gen_met < 300000 || gen_met > 400000 ) ) return;
  if ( event->getMCNumber()==407347 && gen_met < 400000 ) return;

  double MET = metVec.Et();
  fill("MET", MET);
  fill("genMET", gen_met);

  if (nLeptons_fourBody < 2 && nLeptons < 2) return;

  auto closeMll3L = [] (const AnalysisObjects& Objs){
    std::vector<std::pair<int,int>> OSSFpair;
    for (unsigned int i=0;i<Objs.size();i++){
      for (unsigned int j=0;j<Objs.size();j++){
        if(i>j){
          if ((Objs[i].charge() != Objs[j].charge()) && (Objs[i].type() == Objs[j].type())){
             OSSFpair.push_back(std::make_pair(i,j)); 
          }
        }
      } 
    }
    if (OSSFpair.size() == 1) return (Objs[OSSFpair[0].first]+Objs[OSSFpair[0].second]).M();
    else if (OSSFpair.size() == 2){
       double m1 = (Objs[OSSFpair[0].first]+Objs[OSSFpair[0].second]).M();
       double m2 = (Objs[OSSFpair[1].first]+Objs[OSSFpair[1].second]).M();
       double m3L = fabs(m1-91.2) < fabs(m2-91.2) ? m1 : m2;
       return m3L;
    }else return -1.;
    
  }; 

  if ( nLeptons_fourBody == 2 || nLeptons_fourBody == 3) {

    auto lep0 = signalLeptons_fourBody[0];
    auto lep1 = signalLeptons_fourBody[1];
    int nLep = 2;
    if (nLeptons_fourBody == 3)nLep = 3;

    
    // Variables we'll be cutting on
    double ptJet1 = 0., ptJet2 = 0.;
    bool isB1 = false;
    if ( n_jets20 >= 1){
        ptJet1 = jets[0].Pt();
        isB1 =jets[0].pass(BTag77MV2c10);
    }
    if ( n_jets20 >= 2) ptJet2 = jets[1].Pt();

    double mll                            = (lep0 + lep1).M();
    auto pbll_TLV                         = metVec + lep0 + lep1;
    double meff                           = sumObjectsPt(jets, 4, 20) + lep0.Pt() + lep1.Pt() + MET;
    auto pll                              = lep0 + lep1;

    double R2l                            = MET / pll.Pt();
    double R2l4j                          = MET / meff;
    double dPhiEtmisspbll                 = fabs(metVec.DeltaPhi(pbll_TLV));
 
    bool isSF = false;
    if (lep0.type() == lep1.type()) isSF = true;

    //Opposite Sign leptons
    bool isSS = false;
    if (lep0.charge() == lep1.charge()) isSS = true;

    double mll3L = 0.;
    bool isSS3L = false;
    if (nLeptons_fourBody == 3){
        if (lep0.charge() == lep1.charge() && lep1.charge() == signalLeptons_fourBody[2].charge()) isSS3L = true;
        if (!isSS3L) mll3L = closeMll3L(signalLeptons_fourBody);
    }

    //SR4Body
    bool ZVeto = (!isSF || (isSF && fabs(mll-91.2)>10.));
    bool preselection = (nLep==2 && !isSS && lep0.Pt()<100. && lep1.Pt()<100. && mll>10. && MET>250. && ptJet1>150.);
    bool CRtop_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && n_bjets>1 && MET<400.);
    bool VRtop_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && isB1 && MET>400. && R2l<13.);
    bool CRVV_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && mll>45. && ZVeto && (( n_jets20==1 && n_bjets==0)||(n_jets20==2 && n_bjets==0 && ptJet2<40.)) && R2l<4.);
    bool VRVV_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && mll>45. && ZVeto && ( (n_jets20==1 && n_bjets==0) || (n_jets20==2 && n_bjets==0) || (n_jets20==3 && n_bjets==0) || (n_jets20==4 && n_bjets==0) ) && R2l>4 && R2l<5);
    bool VRVV3L_4body = false;
    if (nLep==3) VRVV3L_4body = (nLep==3 && !isSS3L && lep0.Pt()<100. && lep1.Pt()<100. && mll>10. && MET>250. && ptJet1>150. && mll3L>10. && (n_jets20<5 && n_bjets==0) && fabs(mll3L-91.2)>10.);
    bool SRA_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<10. && lep0.Pt()<25. && R2l>25. && R2l4j>0.44 && MET>400.);
    bool SRB_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()>10. && lep1.Pt()<50. && R2l>13. && R2l4j>0.38 && MET>500.);

    //addRegions({"preselection4body", "CR4BodyT", "VR4BodyT", "CR4BodyVV", "VR4BodyVV", "VR4bodyVV3L", "SRA4body", "SRB4body"});
    // addHistogram("ptLep1_SRA",100,0,100);
    // addHistogram("ptLep2_SRA",100,0,100);
    // addHistogram("ptLep1_SRB",100,0,100);
    // addHistogram("ptLep2_SRB",100,0,100);

    if(preselection){
       accept("preselection4body");
       fill("METPreSelect4body", MET);
       fill("ptLep1PreSelect4body", lep0.Pt());
       fill("ptLep2PreSelect4body", lep1.Pt());
    }
    if(CRtop_4body) accept("CR4BodyT");
    if(VRtop_4body) accept("VR4BodyT");
    if(CRVV_4body) accept("CR4BodyVV");
    if(VRVV_4body) accept("VR4BodyVV");
    if(VRVV3L_4body) accept("VR4bodyVV3L");
    if(SRA_4body){ 
      accept("SRA4body");
      fill("ptLep1_SRA", lep0.Pt());
      fill("ptLep2_SRA", lep1.Pt());
    }
    if(SRB_4body){
      accept("SRB4body");
      fill("ptLep1_SRB", lep0.Pt());
      fill("ptLep2_SRB", lep1.Pt());
    }

    

  }

  if (nLeptons == 2) {

    auto lep0 = signalLeptons[0];
    auto lep1 = signalLeptons[1];

    // Variables we'll be cutting on
    double meff                           = sumObjectsPt(jets, 2, 25) + lep0.Pt() + lep1.Pt() + MET;
    auto pbll_TLV                         = metVec + lep0 + lep1;
    auto pll                              = lep0 + lep1;

    double MT2                            = calcMT2(lep0, lep1, metVec);;
    double R1                             = MET / meff;
    double dPhiEtmisspbll                 = fabs(metVec.DeltaPhi(pbll_TLV));

    double mll                            = (lep0 + lep1).M();

    int DX                                = fabs((2 * (lep0.Pz() + lep1.Pz())) / 13000.);

    bool isSF = false;
    if (lep0.type() == lep1.type()) isSF = true;

    //Opposite Sign leptons
    if (lep0.charge() == lep1.charge()) return;

    //Leading lepton with pT>25 GeV
    if (lep0.Pt() < 25.) return;
    //SubLeading lepton with pT>25 GeV
    if (lep1.Pt() < 20.) return;

    //Reject low mass DY etc
    if (mll < 20.) return;

    //
    // Here BEGINs the 3-Body SRs
    //

    // Get the RJ tree
    LabRecoFrame* lab      = m_RF_helper.getLabFrame("lab");
    DecayRecoFrame* ss     = m_RF_helper.getDecayFrame("ss");
    DecayRecoFrame* s1     = m_RF_helper.getDecayFrame("s1");
    InvisibleGroup* inv    = m_RF_helper.getInvisibleGroup("inv");
    CombinatoricGroup* vis = m_RF_helper.getCombinatoricGroup("vis");
    VisibleRecoFrame* v1   = m_RF_helper.getVisibleFrame("v1");

    // Clear the event
    lab->ClearEvent();

    // Set MET
    inv->SetLabFrameThreeVector(metVec.Vect());

    // Add leptons to the visible group
    vis->AddLabFrameFourVector(lep0);
    vis->AddLabFrameFourVector(lep1);

    // Analyze the event
    lab->AnalyzeEvent();

    // Get the variables

    // RPT
    double shat_jigsaw = ss->GetMass();
    TVector3 vPTT      = (ss->GetFourVector(*lab)).Vect();
    double RPT         = vPTT.Pt() / (vPTT.Pt() + shat_jigsaw / 4.);

    // MDR
    double MDR         = 2.0 * v1->GetEnergy(*s1);

    // gamInvRp1
    double gamInvRp1   = ss->GetVisibleShape();

    // DPB_vSS
    double DPB_vSS     = ss->GetDeltaPhiBoostVisible();

    // cosTheta_b
    auto lepPos = lep0, lepNeg = lep1;
    if (signalLeptons[0].charge() < 0.) {
      lepPos = lep1;
      lepNeg = lep0;
    }

    TVector3 boost = pll.BoostVector();
    lepPos.Boost(-boost);
    lepNeg.Boost(-boost);

    double cosTheta_b = tanh((lepPos.Eta() - lepNeg.Eta()) / 2);

    ntupVar("MET", MET);

    fill("METPreSelect", MET);

    // Apply selection

    //CR3Body
    if ( !isSF && n_bjets > 0 && MDR > 80. && RPT > 0.7 && DPB_vSS < (0.9*abs(cosTheta_b)+1.6) ) {
      accept("CR3BodyT");
      fill("METCR3BodyT", MET);
    }
    if ( !isSF && n_bjets == 0 && MDR > 50. && RPT < 0.5 && gamInvRp1 > 0.7 && DPB_vSS < (0.9*abs(cosTheta_b)+1.6) ) {
      accept("CR3BodyVVDF");
      fill("METCR3BodyVVDF", MET);
    }
    if ( isSF && fabs(mll - mZ) > 20. && n_bjets == 0 && MDR > 70. && RPT < 0.5 && gamInvRp1 > 0.7 && DPB_vSS < (0.9*abs(cosTheta_b)+1.6) ) {
      accept("CR3BodyVVSF");
      fill("METCR3BodyVVSF", MET);
    }
    //VR3Body
    if ( !isSF && n_bjets == 0 && MDR > 80. && RPT < 0.7 && DPB_vSS > (0.9*abs(cosTheta_b)+1.6) ) {
      accept("VR3BodyT");
      fill("METVR3BodyT", MET);
    }
    if ( !isSF && n_bjets == 0 && MDR > 50. && MDR < 95. && RPT < 0.7 && gamInvRp1 > 0.7 && DPB_vSS > (0.9*abs(cosTheta_b)+1.6) ) {
      accept("VR3BodyVVDF");
      fill("METVR3BodyVVDF", MET);
    }
    if ( isSF && fabs(mll - mZ) > 20. && n_bjets == 0 && MDR > 60. && MDR < 95. && RPT < 0.4 && gamInvRp1 > 0.7 && DPB_vSS > (0.9*abs(cosTheta_b)+1.6) ) {
      accept("VR3BodyVVSF");
      fill("METVR3BodyVVSF", MET);
    }

    // SR3Body W SF
    if (isSF  && RPT > 0.7 && gamInvRp1 > 0.7 && DPB_vSS > (0.9 * fabs(cosTheta_b) + 1.6) && MDR > 95.  && n_bjets == 0 && fabs(mll - mZ) > 20. ) {
      accept("SR3BodyWSF");
      fill("METSR3BodyWSF", MET);
    }
    // SR3Body W DF
    if (!isSF && RPT > 0.7 && gamInvRp1 > 0.7 && DPB_vSS > (0.9 * fabs(cosTheta_b) + 1.6) && MDR > 95.  && n_bjets == 0                     ) {
      accept("SR3BodyWDF");
      fill("METSR3BodyWDF", MET);
    }
    // SR3Body T SF
    if (isSF  && RPT > 0.7 && gamInvRp1 > 0.7 && DPB_vSS > (0.9 * fabs(cosTheta_b) + 1.6) && MDR > 110. && n_bjets >= 1 && fabs(mll - mZ) > 20. ) {
      accept("SR3BodyTSF");
      fill("METSR3BodyTSF", MET);
    }
    // SR3Body T DF
    if (!isSF && RPT > 0.7 && gamInvRp1 > 0.7 && DPB_vSS > (0.9 * fabs(cosTheta_b) + 1.6) && MDR > 110. && n_bjets >= 1                     ) {
      accept("SR3BodyTDF");
      fill("METSR3BodyTDF", MET);
    }
    // SR3Body T
    if (RPT > 0.7 && gamInvRp1 > 0.7 && DPB_vSS > (0.9 * fabs(cosTheta_b) + 1.6) && MDR > 100. && n_bjets >= 1                     ) {
      accept("SR3BodyT");
      fill("METSR3BodyT", MET);
    }

    //
    // Here ENDs the 3-Body SRs
    //

    //
    // Here STARTs the 2-Body and DM SRs
    //
   if (MT2 < 100.) return;
  //  if (n_bjets > 0 && !isSF && MT2>100 && MT2<110 && dPhiEtmisspbll > 1.5 && metSignificance > 12.) accept("CR2BodyT")
   if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 110. && MT2 < 120. && metSignificance > 12.)  accept("SR2BodySF0");
   if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 110. && MT2 < 120. && metSignificance > 12.)  accept("SR2BodyDF0");
   if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 120. && MT2 < 140. && metSignificance > 12.)  accept("SR2BodySF1");
   if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 120. && MT2 < 140. && metSignificance > 12.)  accept("SR2BodyDF1");
   if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 140. && MT2 < 160. && metSignificance > 12.)  accept("SR2BodySF2");
   if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 140. && MT2 < 160. && metSignificance > 12.)  accept("SR2BodyDF2");
   if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 160. && MT2 < 180. && metSignificance > 12.)  accept("SR2BodySF3");
   if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 160. && MT2 < 180. && metSignificance > 12.)  accept("SR2BodyDF3");
   if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 180. && MT2 < 220. && metSignificance > 12.)  accept("SR2BodySF4");
   if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 180. && MT2 < 220. && metSignificance > 12.)  accept("SR2BodyDF4");
   if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 220.               && metSignificance > 12.)  accept("SR2BodySF5");
   if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 220.               && metSignificance > 12.)  accept("SR2BodyDF5");

   if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 220. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc6");
   if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 200. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc5");
   if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 180. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc4");
   if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 160. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc3");
   if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 140. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc2");
   if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 120. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc1");
   if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 110. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc0");
  }

  return;
}
#endif
