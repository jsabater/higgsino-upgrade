#include "SimpleAnalysis/AnalysisClass.h"
#include <bitset>

DefineAnalysis(MultiJets2016)

void MultiJets2016::Init()
{
  // For now only signal regions
  /**********************************************
  *          Flavour Stream (50 GeV)           *
  **********************************************/
  // Template regions
  addRegions({"SR_6ej50_0ib", "SR_6ej50_1ib", "SR_6ej50_2ib"});
  // Validation regions
  addRegions({"SR_7ej50_0ib", "SR_7ej50_1ib", "SR_7ej50_2ib"});
  // Signal regions
  for (const std::string& num : {"8", "9", "10", "11"} ) {
    for (const std::string& bNum : {"0", "1", "2"} ) {
      addRegions({"SR_"+num+"ij50_"+bNum+"ib"});
    }
  }
  // Control regions (NB: combine 1e and 1mu regions)
  for (const std::string& num : {"7", "8", "9", "10"} ) {
    for (const std::string& bStr : {"0eb", "1ib"} ) {
      //addRegions("CR1l_"+num+"ij50_"+bStr);
    }
  }
  /**********************************************
  *             Mass Stream (50 GeV)            *
  **********************************************/
  // Template regions
  addRegions({"SR_6ej50_MJ340", "SR_6ej50_MJ500"});
  // Validation regions
  addRegions({"SR_7ej50_MJ340", "SR_7ej50_MJ500"});
  // Signal regions
  for (const std::string& num : {"8", "9", "10"} ) {
    for (const std::string& mj : {"340", "500"} ) {
      addRegions({"SR_"+num+"ij50_MJ"+mj});
    }
  }
  // Control regions (NB: combine 1e and 1mu regions)
  for (const std::string& num : {"7", "8", "9", "10"} ) {
    for (const std::string& bStr : {"0eb", "1ib"} ) {
      //addRegions("CR1l_"+num+"ij50_"+mj);
    }
  }
  /**********************************************
  *          Flavour Stream (80 GeV)           *
  **********************************************/
  // Template regions
  addRegions({"SR_5ej80_0ib", "SR_5ej80_1ib", "SR_5ej80_2ib"});
  // Validation regions
  addRegions({"SR_6ej80_0ib", "SR_6ej80_1ib", "SR_6ej80_2ib"});
  // Signal regions
  for (const std::string& num : {"7", "8", "9"} ) {
    for (const std::string& bNum : {"0", "1", "2"} ) {
      addRegions({"SR_"+num+"ij80_"+bNum+"ib"});
    }
  }
  // Control regions
  for (const std::string& num : {"7", "8", "9", "10"} ) {
    for (const std::string& bStr : {"0eb", "1ib"} ) {
      //addRegions("CR_"+num+"ij80_"+bStr);
    }
  }
}

AnalysisObjects manualJetRecluster(AnalysisObjects smallJets, double radius = 1.0)
{
  // Manual implementation of the anti-kT algorithm to to re-cluster smaller radius jets into
  // larger radius jets. The input vector "smallJets" may include multiple leptons if they are input 
  int nSmallJets = smallJets.size();

  AnalysisObjects largeJets;

  while (nSmallJets>0){

    Double_t dRij=99999999999.;
    Double_t dRi=99999999999.;
    int dRiindex=0;
    int dRijindex1=0;
    int dRijindex2=0;
    for (Int_t i=0; i<nSmallJets; i++){
      if (pow(1./(smallJets.at(i)).Pt(),2)<dRi){
        dRi=pow(1.0/(smallJets.at(i)).Pt(),2);
        dRiindex=i;
      }
    }
    for (Int_t i=0; i<nSmallJets-1; i++){
      for (Int_t j=i+1; j<nSmallJets; j++){
        Double_t tempdRij = std::min(pow(smallJets.at(i).Pt(), -2), pow(smallJets.at(j).Pt(), -2) )
          * smallJets.at(i).DeltaR(smallJets.at(j) ) / radius;
        if (tempdRij<dRij){
          dRij=tempdRij;
          dRijindex1=i;
          dRijindex2=j;
        }
      }
    }

    int deleteIndex=0;
    if (dRi<dRij){//add jet to large jet collection
      deleteIndex=dRiindex;

      AnalysisObject largeJet(smallJets.at(dRiindex) );
      largeJets.push_back( largeJet );

    }else{//add jet j to jet i
      smallJets.at(dRijindex1) += smallJets.at(dRijindex2);
      deleteIndex=dRijindex2;
    }
    //remove the jet from the collection
    smallJets.erase(smallJets.begin()+deleteIndex);
    --nSmallJets;
  }
  return largeJets;
}

void MultiJets2016::ProcessEvent(AnalysisEvent *event)
{

  auto electrons  = event->getElectrons(10, 2.47, ELooseLH);
  auto muons      = event->getMuons(10, 2.5, MuMedium);
  auto jets       = event->getJets(20., 2.8, JVT50Jet);
  auto metVec     = event->getMET();
  double met      = metVec.Et();

  // Reject events with bad jets
  if (countObjects(jets, 20, 2.8, NOT(LooseBadJet))!=0) return;
  // FIXME: not doing event cleaning for JVT-miss jets near MET or inefficient hadronic calo

  // Standard SUSY overlap removal      
  jets       = overlapRemoval(jets, electrons, 0.2);
  jets       = overlapRemoval(jets, muons, 0.4, LessThan3Tracks);
  electrons  = overlapRemoval(electrons, jets, 0.4);
  muons      = overlapRemoval(muons, jets, 0.4);
  //not doing overlap removal between electrons and muons with identical track

  // Count jets
  int n50   = countObjects(jets, 50., 2.0);
  int n80   = countObjects(jets, 80., 2.0);
  int nbtag = countObjects(jets, 40., 2.5, BTag70MV2c10);
  // Everything needs at least 5 jets
  if (n50 < 5) return;

  // Leptons for control regions
  auto signalMuons     = filterObjects(muons, 10, 2.5, MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
  auto signalElectrons = filterObjects(electrons, 10, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signalLeptons   = signalMuons+signalElectrons;

  int nBaselineLeptons = muons.size() + electrons.size();

  AnalysisObjects largeJets = ( signalLeptons.size()> 0 && signalLeptons.front().Pt() > 50)
    ? manualJetRecluster(jets + AnalysisObjects({signalLeptons.front()}) ) 
    : manualJetRecluster(jets);

  float MJSigma = 0;
  for (const AnalysisObject& jet : largeJets)
    if (jet.Pt() > 100 && fabs(jet.Eta() ) < 1.5)
      MJSigma += jet.M();


  // Find the jet closest to the MET
  const AnalysisObject* closestJet = 0;
  for (const AnalysisObject& jet : jets) {
    if (!closestJet) closestJet = &jet;
    else if (metVec.DeltaPhi(jet) < metVec.DeltaPhi(*closestJet) ) closestJet = &jet;
  }
      
  // Define HT and MetSig
  Double_t SUSY_MetSig=0.;
  Double_t SUSY_HT=sumObjectsPt(jets, 999, 40);
  if (SUSY_HT>0.) SUSY_MetSig=met/sqrt(SUSY_HT);

  // nLepton cuts
  if (nBaselineLeptons == 0) {
    // Signal/template regions
    // METSig cut (same for all)
    if (SUSY_MetSig < 5) return;
    // Dead tile veto
    if ( (-0.1  < closestJet->Eta() && closestJet->Eta() <  1.0
       && -1.45 < closestJet->Phi() && closestJet->Phi() < -1.1)
      || (-1.0  < closestJet->Eta() && closestJet->Eta() <  0.1
       &&  0.3  < closestJet->Phi() && closestJet->Phi() < 0.6) ) return;
    for (int ii : {0, 1, 2}) {
      if (nbtag < ii) break;
      for ( int jj : {8, 9, 10, 11}) {
        if (n50 < jj) break;
        accept("SR_"+std::to_string(jj)+"ij50_"+std::to_string(ii)+"ib");
      }
      for (int jj : {7, 8, 9}) {
        if (n80 < jj) break;
        accept("SR_"+std::to_string(jj)+"ij80_"+std::to_string(ii)+"ib");
      }
    }
    for (unsigned int ii : {340, 500}) {
      if (MJSigma < ii) break;
      for (int jj : {8, 9, 10}) {
        if (n50 < jj) break;
        accept("SR_"+std::to_string(jj)+"ij50_MJ"+std::to_string(ii));
      }
    }
  }
  else if (nBaselineLeptons == 1 && signalLeptons.size() == 1) {
    // Control regions TODO - not included in this at the moment
  }
  return;
}
