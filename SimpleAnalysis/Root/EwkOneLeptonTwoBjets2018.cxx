#include "SimpleAnalysis/AnalysisClass.h"
#include "SimpleAnalysis/NtupleMaker.h"
#include "SimpleAnalysis/PDFReweight.h"
#include <LHAPDF/LHAPDF.h>
#include "TMath.h"

DefineAnalysis(EwkOneLeptonTwoBjets2018)
// Wh->1l+bb+met analysis (Run2 data)

void EwkOneLeptonTwoBjets2018::Init()

{ 
  // Define signal/control regions
  addRegions({"SR_h_Low", "SR_h_Med", "SR_h_High"} ); 
  addRegions({"SR_h_Low_Incl", "SR_h_Med_Incl", "SR_h_High_Incl"} ); 
  addRegions({"SR_h_Low_bin1", "SR_h_Low_bin2", "SR_h_Low_bin3"});
  addRegions({"SR_h_Med_bin1", "SR_h_Med_bin2", "SR_h_Med_bin3"});
  addRegions({"SR_h_High_bin1", "SR_h_High_bin2", "SR_h_High_bin3"});
  addRegions({"CR_tt_Low", "CR_tt_Med", "CR_tt_High"}); 
  addRegions({"WCR", "STCR"});
  addRegions({"VR_off_Low", "VR_off_Med", "VR_off_High"});  
  addRegions({"VR_on_Low", "VR_on_Med", "VR_on_High"});  
}

void EwkOneLeptonTwoBjets2018::ProcessEvent(AnalysisEvent *event)
{
  //
  // Soft Objects
  //
  auto  Soft_Elec = event->getElectrons(7,2.47,ELooseBLLH | EZ05mm);
  auto  Soft_Muon = event->getMuons(6,2.70, MuMedium| MuNotCosmic | MuGood | MuZ05mm);
  auto  Soft_Jets = event->getJets(20.,4.5);
  auto  met_Vect  = event->getMET();
  float met       = met_Vect.Pt();
   //
  // Baseline objects
  //
  // Reject events with bad jets
  auto radiusCalcLep = [] (const AnalysisObject& lep,const AnalysisObject&) {return (0.04 + 10/lep.Pt()) > 0.4 ? 0.4 : 0.04 + 10/lep.Pt();};
  auto Base_Elec      = overlapRemoval(Soft_Elec,Soft_Elec,0.01);
  Base_Elec      = overlapRemoval(Soft_Elec,Soft_Muon,0.01);
//  auto Base_Jets      = overlapRemoval(Soft_Jets,Base_Elec,0.2, NOT(BTag77MV2c10));
//  Base_Elec           = overlapRemoval(Base_Elec,Base_Jets,radiusCalcLep);
  Base_Elec           = overlapRemoval(Base_Elec,Soft_Jets,radiusCalcLep);
  auto Base_Jets      = overlapRemoval(Soft_Jets, Soft_Muon, 0.2);
  auto Base_Muon      = overlapRemoval(Soft_Muon,Base_Jets,radiusCalcLep);

   auto Signal_Elec = filterObjects(Base_Elec,7.,2.47,ETightLH | ED0Sigma5  | EZ05mm  | EIsoGradientLoose);
  auto Signal_Muon = filterObjects(Base_Muon,6.,2.7,MuMedium | MuD0Sigma3 | MuZ05mm |MuIsoGradientLoose);
  auto Signal_Jets = filterObjects(Base_Jets,30.,2.80,JVT50Jet);  
  auto Signal_BJet = filterObjects(Signal_Jets,30.,2.8,BTag77MV2c10);

  //
  auto Signal_Lep=  Signal_Elec + Signal_Muon;
  //Count the number of signal objects
  unsigned int N_Signal_Elec = Signal_Elec.size();
  unsigned int N_Signal_Muon = Signal_Muon.size();
  unsigned int N_Signal_Lept = N_Signal_Elec + N_Signal_Muon;
  unsigned int N_Signal_Jets = Signal_Jets.size();
  unsigned int N_Signal_BJet = Signal_BJet.size();
  float mt=0, m_CT=0,  mbb=0, b2jetpt=0, mlb1=0;
  if (Signal_Lep.size()==1   && Signal_BJet.size()==2 ) {
    mt   = calcMT(Signal_Lep[0], met_Vect);
    m_CT = calcMCT(Signal_BJet[0],Signal_BJet[1]);
    mbb= (Signal_BJet[0]+Signal_BJet[1]).M();
    mlb1= (Signal_BJet[0]+Signal_Lep[0]).M();
	b2jetpt = Signal_BJet[1].Pt();
  }  

  //Define ntupleVar variables for Hamish
  int SR_h_Low(0), SR_h_Med(0), SR_h_High(0); 
  int SR_h_Low_Incl(0), SR_h_Med_Incl(0), SR_h_High_Incl(0); 
  int SR_h_Low_bin1(0), SR_h_Low_bin2(0), SR_h_Low_bin3(0);
  int SR_h_Med_bin1(0), SR_h_Med_bin2(0), SR_h_Med_bin3(0);
  int SR_h_High_bin1(0), SR_h_High_bin2(0), SR_h_High_bin3(0);
  int CR_tt_Low(0), CR_tt_Med(0), CR_tt_High(0); 
  int WCR(0), STCR(0);
  int VR_off_Low(0), VR_off_Med(0), VR_off_High(0);  
  int VR_on_Low(0), VR_on_Med(0), VR_on_High(0);  
 
  // Preselection
  if(N_Signal_Lept != 1) return;
  if(N_Signal_BJet != 2) return;
  if(N_Signal_Jets>3 || N_Signal_Jets < 2) return;
  if(mt< 50.) return; 

   if(met< 220.) return; 
  // Signal regions
  if(met>240 && mbb>100 && mbb<=140 && m_CT>180){
    if(mt>100 && mt<160){
		accept("SR_h_Low");
		SR_h_Low = 1;
		if(m_CT > 180 && m_CT <= 230){
			accept("SR_h_Low_bin1");
			SR_h_Low_bin1 = 1;
		}else if(m_CT > 230 && m_CT <= 280){
			accept("SR_h_Low_bin2");
			SR_h_Low_bin2 = 1;
		}else{
			accept("SR_h_Low_bin3");
			SR_h_Low_bin3 = 1;
		}
	}
    else if(mt>160 && mt<240){
		accept("SR_h_Med");
		SR_h_Med = 1;
		if(m_CT > 180 && m_CT <= 230){
			accept("SR_h_Med_bin1");
			SR_h_Med_bin1 = 1;
		}else if(m_CT > 230 && m_CT <= 280){
			accept("SR_h_Med_bin2");
			SR_h_Med_bin2 = 1;
		}else{
			accept("SR_h_Med_bin3");
			SR_h_Med_bin3 = 1;
		}
	}
    else if(mt>240 && mlb1 > 120){
		accept("SR_h_High");
		SR_h_High = 1;
		if(m_CT > 180 && m_CT <= 230){
			accept("SR_h_High_bin1");
			SR_h_High_bin1 = 1;
		}else if(m_CT > 230 && m_CT <= 280){
			accept("SR_h_High_bin2");
			SR_h_High_bin2 = 1;
		}else{
			accept("SR_h_High_bin3");
			SR_h_High_bin3 = 1;
		}
	}
  }
  if(met>240 && mbb>100 && mbb<=140 && m_CT>180){
	if(mt > 100){
		accept("SR_h_Low_Incl");
		SR_h_Low_Incl = 1;
	}
	if(mt >= 160){
		accept("SR_h_Med_Incl");
		SR_h_Med_Incl = 1;
	}
	if(mt > 240 && mlb1 > 120){
		accept("SR_h_High_Incl");
		SR_h_High_Incl = 1;
	}
  }

  // Control  regions W
  if(met>240 && mbb<80 && mbb > 50 && mt<100 && mt>50 && m_CT>180){
	accept("WCR");
	WCR = 1;
  }
   
  // Control  regions tt
  if(met>240 && mt>100 && m_CT<180 && ((mbb < 100 && mbb > 50)|| mbb > 140)){
	if(mt < 160){
		accept("CR_tt_Low");
		CR_tt_Low = 1;
	}else if(mt < 240){
		accept("CR_tt_Med");
		CR_tt_Med = 1;
	}else if(mlb1 > 120){
		accept("CR_tt_High");
		CR_tt_High = 1;
	}
  } 
  
  // Control  regions ST
  if(met>240 && m_CT>180 && mt > 100 && mbb > 195){
    accept("STCR");
	STCR = 1;
  }

  // validation regions on
  if(mbb>100 && mbb<140 && m_CT<180 && mt > 100){
    if(mt < 160 && met > 240){
		accept("VR_on_Low");
		VR_on_Low = 1;
	}else if(met > 240 && mt < 240 && mt > 160){
		accept("VR_on_Med");
		VR_on_Med = 1;
	}else if(mt > 240){
		accept("VR_on_High");
		VR_on_High = 1;
	}
  } 

   // validation regions off 
  if(((mbb > 50 && mbb<80) ||( mbb>160  && mbb<195))&& m_CT>180 && mt > 100){
    if(mt<160){
		accept("VR_off_Low");
		VR_off_Low = 1;
	}else if(met > 240 && mt < 240 && mt > 160){
		accept("VR_off_Med");
		VR_off_Med = 1;
	}else if(mt > 240 && ((mbb > 50 && mbb < 80)||(mbb > 160 && mbb < 195))){
		accept("VR_off_High");
		VR_off_High = 1;
	}
  } 
  ntupVar("SR_h_Low", SR_h_Low);
  ntupVar("SR_h_Med", SR_h_Med);
  ntupVar("SR_h_High", SR_h_High); 
  ntupVar("SR_h_Low_Incl",SR_h_Low_Incl);
  ntupVar("SR_h_Med_Incl",SR_h_Med_Incl);
  ntupVar("SR_h_High_Incl",SR_h_High_Incl); 
  ntupVar("SR_h_Low_bin1",SR_h_Low_bin1);
  ntupVar("SR_h_Low_bin2",SR_h_Low_bin2);
  ntupVar("SR_h_Low_bin3",SR_h_Low_bin3);
  ntupVar("SR_h_Med_bin1",SR_h_Med_bin1);
  ntupVar("SR_h_Med_bin2",SR_h_Med_bin2);
  ntupVar("SR_h_Med_bin3",SR_h_Med_bin3);
  ntupVar("SR_h_High_bin1",SR_h_High_bin1);
  ntupVar("SR_h_High_bin2",SR_h_High_bin2);
  ntupVar("SR_h_High_bin3",SR_h_High_bin3);
  ntupVar("CR_tt_Low",CR_tt_Low);
  ntupVar("CR_tt_Med",CR_tt_Med);
  ntupVar("CR_tt_High",CR_tt_High); 
  ntupVar("WCR",WCR);
  ntupVar("STCR",STCR);
  ntupVar("VR_off_Low",VR_off_Low);
  ntupVar("VR_off_Med",VR_off_Med);
  ntupVar("VR_off_High",VR_off_High);  
  ntupVar("VR_on_Low",VR_on_Low);
  ntupVar("VR_on_Med",VR_on_Med);
  ntupVar("VR_on_High",VR_on_High);  

  return;    
}
