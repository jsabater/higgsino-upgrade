#include "SimpleAnalysis/AnalysisClass.h"

DefineAnalysis(PhotonJets2016)

void PhotonJets2016::Init()
{
  addRegions({"SRL200", "SRL", "SRH"});

  addRegions({"CRQ", "CRW", "CRT"});
  addRegions({"VRM1L", "VRM2L", "VRM3L"});
  addRegions({"VRM1H", "VRM2H", "VRM3H"});
  addRegions({"VRL1", "VRL2", "VRL3", "VRL4"});
}

void PhotonJets2016::ProcessEvent(AnalysisEvent *event)
{
  // Baseline objects
  auto photons   = event->getPhotons(25., 2.37, PhotonGood); 
  auto electrons = event->getElectrons(10., 2.47, ELooseLH);
  auto muons     = event->getMuons(10., 2.7, MuMedium);
  auto jets      = event->getJets(20., 2.8);
  auto met       = event->getMET();

  // Remove events with one bad jet
  if (countObjects(jets, 20, 2.8, NOT(LooseBadJet)) != 0) return;

  // Standard SUSY overlap removal
  photons    = overlapRemoval(photons, electrons, 0.01);
  jets       = overlapRemoval(jets, electrons, 0.2);
  jets       = overlapRemoval(jets, photons, 0.2);
  electrons  = overlapRemoval(electrons, jets, 0.4);
  photons    = overlapRemoval(photons, jets, 0.4);
  muons      = overlapRemoval(muons, jets, 0.4);

  // Signal objects
  auto signal_photons   = filterObjects(photons,   75., 2.37, PhotonIsoGood);
  auto signal_muons     = filterObjects(muons,     25., 2.7,  MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
  auto signal_electrons = filterObjects(electrons, 25., 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signal_jets      = filterObjects(jets,      30., 2.5);
  auto signal_bjets     = filterObjects(signal_jets, 30., 2.5, BTag77MV2c20);

  signal_photons   = filterCrack(signal_photons);
  signal_electrons = filterCrack(signal_electrons);

  sortObjectsByPt(signal_photons);
  sortObjectsByPt(signal_jets);

  // At least one photon with pt>145 GeV
  int ph_n = signal_photons.size();
  if (ph_n == 0)
    return;

  float ph_pt = signal_photons[0].Pt();
  if (ph_pt < 145.)
    return;
  
  // Object multiplicity
  int el_n  = signal_electrons.size();
  int mu_n  = signal_muons.size();
  int lep_n = el_n + mu_n;

  int jet_n  = signal_jets.size();
  int bjet_n = signal_bjets.size();

  // Other variables
  double met_et = met.Et();
  float ht = sumObjectsPt(signal_jets) + ph_pt;
  float meff = ht + met_et;

  float dphi_gammet = minDphi(met, signal_photons, 1);
  float dphi_jetmet = minDphi(met, signal_jets, 2);

  float rt4 = 0;
  if (jet_n >= 4)
    rt4 = sumObjectsPt(signal_jets, 4)/sumObjectsPt(signal_jets);

  // Signal regions
  if (lep_n==0 && jet_n>4 && met_et>200 && dphi_gammet>0.4 && dphi_jetmet>0.4 && meff>2000 && rt4<0.9) {
    accept("SRL200");

    if (met_et>300)
      accept("SRL");
  }
  if (lep_n==0 && ph_pt>400 && jet_n>2 && met_et>400 && dphi_gammet>0.4 && dphi_jetmet>0.4 && meff>2400)
      accept("SRH");

  // Control regions
  if (lep_n==0 && jet_n>2 && met_et>100. && dphi_jetmet<0.4 && dphi_gammet>0.4 && meff>2000.)
    accept("CRQ");

  if (lep_n>=1 && jet_n>=1 && met_et>100. && met_et<200. && dphi_jetmet>0.4 && meff>500. && bjet_n==0)
    accept("CRW");

  if (lep_n>=1 && jet_n>=2 && met_et>50. && met_et<200. && dphi_jetmet>0.4 && meff>500. && bjet_n>=2)
    accept("CRT");

  // Validation regions
  //// Intermediate MET
  if (lep_n==0 && ph_pt>145 && met_et>50 && met_et<175 && jet_n>4 && dphi_jetmet>0.4 && dphi_gammet>0.4 && meff>2000 && rt4<0.9)
    accept("VRM1L");
  if (lep_n==0 && ph_pt>145 && met_et>75 && met_et<175 && jet_n>4 && dphi_jetmet>0.4 && dphi_gammet>0.4 && meff>2000 && rt4<0.9)
    accept("VRM2L");
  if (lep_n==0 && ph_pt>145 && met_et>100 && met_et<175 && jet_n>4 && dphi_jetmet>0.4 && dphi_gammet>0.4 && meff>2000 && rt4<0.9)
    accept("VRM3L");

  if (lep_n==0 && ph_pt>145 && met_et>100 && met_et<175 && jet_n>2 && dphi_jetmet>0.4 && meff>2000)
    accept("VRM1H");
  if (lep_n==0 && ph_pt>145 && met_et>125 && met_et<175 && jet_n>2 && dphi_jetmet>0.4 && meff>2000)
    accept("VRM2H");
  if (lep_n==0 && ph_pt>145 && met_et>150 && met_et<175 && jet_n>2 && dphi_jetmet>0.4 && meff>2000)
    accept("VRM3H");
  
  //// lepton VR: Wgamma/ttbarg
  if (lep_n>0 && ph_pt>145 && jet_n>1 && met_et<200 && dphi_jetmet>0.4 && meff>1000)
    accept("VRL1");
  if (lep_n>0 && ph_pt>145 && jet_n>1 && met_et<200 && dphi_jetmet>0.4 && meff>1500)
    accept("VRL2");
  if (lep_n>0 && ph_pt>145 && jet_n>1 && met_et>200 && dphi_jetmet>0.4 && meff>1000 && meff<2000)
    accept("VRL3");
  if (lep_n>0 && ph_pt>145 && jet_n>1 && met_et>200 && dphi_jetmet<0.4 && meff>1500)
    accept("VRL4");

  return;
}
