#include "SimpleAnalysis/AnalysisClass.h"

DefineAnalysis(EwkTwoLepton2016)

void EwkTwoLepton2016::Init()
{
  addRegions({"SFa","SFb","SFc","SFd","SFe","SFf","SFg","SFh","SFi","SFj","SFk","SFl","SFm"}); 
  addRegions({"DFa","DFb","DFc","DFd"}); 
  addRegions({"SFloose","SFtight"}); 
  addRegions({"DF100","DF150","DF200","DF300"}); 
}

void EwkTwoLepton2016::ProcessEvent(AnalysisEvent *event)
{
  auto baselineElectrons  = event->getElectrons(10, 2.47, ELooseBLLH);
  auto baselineMuons       = event->getMuons(10, 2.4, MuMedium|MuNotCosmic|MuQoPSignificance);
  auto baselineJets       = event->getJets(20., 4.5);
  auto metVec             = event->getMET();
  double met              = metVec.Et();

  // overlap removal
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineMuons, 0.01);
  baselineJets      = overlapRemoval(baselineJets, baselineElectrons, 0.2, NOT(BTag77MV2c20));
  baselineElectrons = overlapRemoval(baselineElectrons, baselineJets, 0.2);
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineJets, 0.4);
  auto muJetSpecial = [] (const AnalysisObject& jet, const AnalysisObject& muon) {
    if (jet.pass(NOT(BTag77MV2c20)) && (jet.pass(LessThan3Tracks) || muon.Pt()/jet.Pt()>0.7)) return 0.2;
    else return 0.;
  };
  baselineJets = overlapRemoval(baselineJets, baselineMuons, muJetSpecial, NOT(BTag77MV2c20));
  baselineMuons      = overlapRemoval(baselineMuons, baselineJets, 0.4);

  auto baselineLeptons   = baselineElectrons + baselineMuons;
  int nBaselineLeptons = baselineLeptons.size();
  
  auto signalJets      = filterObjects(baselineJets, 20, 2.4, JVT50Jet);
  auto signalElectrons = filterObjects(baselineElectrons, 20, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signalMuons     = filterObjects(baselineMuons, 20, 2.4, MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);

  auto bJets = filterObjects(signalJets, 20., 2.4, BTag77MV2c10);
  auto nonbJets = filterObjects(signalJets, 60., 2.4, NOT(BTag77MV2c10));

  auto signalLeptons   = signalElectrons + signalMuons;
  
  int nJets = signalJets.size(); 
  int nBjets = bJets.size();
  int nNonBjets = nonbJets.size();
  int nLeptons = signalLeptons.size();
  int nElectrons = signalElectrons.size();
  int nMuons = signalMuons.size();

  sortObjectsByPt(signalLeptons);
  sortObjectsByPt(nonbJets);
  sortObjectsByPt(signalJets);
  sortObjectsByPt(signalElectrons);
  sortObjectsByPt(signalMuons);

  //start preselecting 
  //exactly 2 baseline and signal leptons
  if(nBaselineLeptons != 2) return;
  if(nLeptons != 2) return;

  //leading lepton > 25 GeV 
  if(signalLeptons[0].Pt() < 25.) return;

  //require opposite charge leptons
  if(signalLeptons[0].charge() == signalLeptons[1].charge()) return;

  //all events have mll > 40 GeV
  if((signalLeptons[0] + signalLeptons[1]).M() < 40.) return;
  
  //vetoing on two jet categories                                                                                    
  //central non-b-tagged and central b-tagged 
  if(nNonBjets>0) return;
  if(nBjets>0) return;

  //computing relevant variables  
  float mt2 = calcMT2(signalLeptons[0],signalLeptons[1],metVec);
  float mll = (signalLeptons[0] + signalLeptons[1]).M();
  bool isSF = true;
  if(signalLeptons[0].type() != signalLeptons[1].type()) isSF = false;

  if(mt2 > 100. && mt2 < 150. && !isSF) accept("DFa");
  if(mt2 > 100. && mt2 < 150. && isSF && mll > 111. && mll < 150.) accept("SFa");
  if(mt2 > 100. && mt2 < 150. && isSF && mll > 150. && mll < 200.) accept("SFb");
  if(mt2 > 100. && mt2 < 150. && isSF && mll > 200. && mll < 300.) accept("SFc");
  if(mt2 > 100. && mt2 < 150. && isSF && mll > 300.) accept("SFd");

  if(mt2 > 150. && mt2 < 200. && !isSF) accept("DFb");
  if(mt2 > 150. && mt2 < 200. && isSF && mll > 111. && mll < 150.) accept("SFe");
  if(mt2 > 150. && mt2 < 200. && isSF && mll > 150. && mll < 200.) accept("SFf");
  if(mt2 > 150. && mt2 < 200. && isSF && mll > 200. && mll < 300.) accept("SFg");
  if(mt2 > 150. && mt2 < 200. && isSF && mll > 300.) accept("SFh");

  if(mt2 > 200. && mt2 < 300. && !isSF) accept("DFc");
  if(mt2 > 200. && mt2 < 300. && isSF && mll > 111. && mll < 150.) accept("SFi");
  if(mt2 > 200. && mt2 < 300. && isSF && mll > 150. && mll < 200.) accept("SFj");
  if(mt2 > 200. && mt2 < 300. && isSF && mll > 200. && mll < 300.) accept("SFk");
  if(mt2 > 200. && mt2 < 300. && isSF && mll > 300.) accept("SFl");

  if(mt2 > 300. && !isSF) accept("DFd");
  if(mt2 > 300. && isSF && mll > 111.) accept("SFm");

  if(mt2 > 100. && isSF && mll > 111.) accept("SFloose");
  if(mt2 > 130. && isSF && mll > 300.) accept("SFtight");

  if(mt2 > 100. && !isSF) accept("DF100");
  if(mt2 > 150. && !isSF) accept("DF150");
  if(mt2 > 200. && !isSF) accept("DF200");
  if(mt2 > 300. && !isSF) accept("DF300");

  ntupVar("met",met);
  ntupVar("mll",mll);
  ntupVar("mt2",mt2);
  ntupVar("nJets",nJets);
  ntupVar("nBjets",nBjets);
  ntupVar("nNonBjets",nNonBjets);
  ntupVar("nLeptons",nLeptons);
  ntupVar("nElectrons",nElectrons);
  ntupVar("nMuons",nMuons);
  ntupVar("firstLepton",signalLeptons[0]);
  ntupVar("secondLepton",signalLeptons[1]);

  return;
}
