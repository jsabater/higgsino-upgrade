#include "SimpleAnalysis/AnalysisClass.h"
#include "xAODEventInfo/EventInfo.h"

#include "TMath.h"
#include <bitset>

DefineAnalysis(Scharm2016)
//
// Scharm analysis (Run2 2015+2016 data) [SUSY-2016-26-001]
// Support Doc: ATL-COM-PHYS-2016-1698
//              https://cds.cern.ch/record/2234856/

void Scharm2016::Init()
{
  //** stop/scharm -> charm analysis
  //signal regions
  addRegions({"SRAll", "SR1", "SR2", "SR3", "SR4", "SR5"});
  
}

void Scharm2016::ProcessEvent(AnalysisEvent *event)
{

  auto electrons  = event->getElectrons(7, 2.47, ELooseBLLH);
  auto muons      = event->getMuons(7, 2.7, MuMedium);
  auto candJets   = event->getJets(5., 2.8);
  auto metVec     = event->getMET();
  double met      = metVec.Et(); 

  float gen_met   = event->getGenMET();
  //float gen_ht    = event->getGenHT();
  int  mc_channel = event->getMCNumber();
  
  //MC OR
  if(mc_channel==410000 && gen_met>200.) return; //remove ttbar inc vs MET200 overlap
  if(mc_channel==407012 && gen_met>300.) return; //remove ttbar MET200 vs MET300 overlap
  if(mc_channel==407322 && gen_met>400.) return; //remove ttbar MET300 vs MET400 overlap
  
  for(unsigned int iw=0; iw < event->getMCWeights().size(); iw++)
    ntupVar(("mc_weight"+std::to_string(iw)).c_str(), event->getMCWeights()[iw]);
 
  // In the analysis, a jet is labelled as BAD if it is either loose bad
  // or leading and labelled tight bad

  if (candJets.size() == 0) return;
  if (countObjects(candJets, 20, 2.8, NOT(LooseBadJet))!=0) return; 
  if (candJets[0].pass(NOT(TightBadJet))) return;

  candJets = filterObjects(candJets, 20, 2.8, JVT50Jet); 

  // Overlap removal
  // not doing overlap removal between electrons and muons with identical track

  auto radiusCalcLepton = [] (const AnalysisObject& lepton, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/lepton.Pt()); };

  candJets   = overlapRemoval(candJets,electrons,0.2);
  electrons  = overlapRemoval(electrons,candJets,0.4);
  candJets   = overlapRemoval(candJets, muons, 0.2); 
  muons      = overlapRemoval(muons, candJets, radiusCalcLepton); 

  auto baselineLeptons   = electrons + muons;

  auto signalJets20    = filterObjects(candJets, 20, 2.8);
  auto signalJets30    = filterObjects(candJets, 30, 2.5);
  auto truthCJets30   = filterObjects(candJets, 30, 2.5, TrueCJet); 
  auto signalElectrons = filterObjects(electrons, 10, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signalMuons     = filterObjects(muons, 10, 2.7, MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
  auto signalLeptons   = signalElectrons + signalMuons;

  //Fill in optional ntuple variables
  ntupVar("met",met);
  ntupVar("jet_N20",(int)signalJets20.size());
  ntupVar("jet_N30",(int)signalJets30.size());
  ntupVar("ctruth_N30",(int)truthCJets30.size());
  ntupVar("el_N",(int)electrons.size());
  ntupVar("mu_N",(int)muons.size());
  ntupVar("jets",candJets);  
  ntupVar("cjets",truthCJets30);  

  //Common Selection 
  //int nJets20  = signalJets20.size();
  int nJets30  = signalJets30.size();
  int nCJets = truthCJets30.size();

  accept("SRAll"); //dummy selection to save variables/histos for the full sample
  
  //super-common pre-selection
  if (nCJets == 0) return;
  if (nJets30 == 0) return;
  
  ntupVar("jet1TrueC",signalJets30[0].pass(NOT(TrueCJet)));

  //** SR regions definitions

  bool zeroLep = (baselineLeptons.size()==0);
  
  auto myjets  = signalJets30;   //30 GeV jets for A and B regions
  auto cjets = truthCJets30;

  float dphiMin = minDphi(metVec, myjets);
  float mct = calcMTmin(cjets, metVec);

  ntupVar("MTCharmMin",mct);  
  ntupVar("dphiMin",dphiMin);  

  //------ SR1
  
  if( zeroLep && nJets30>=2 &&
      dphiMin > 0.4 && met > 500 &&
      myjets[0].Pt() > 250 && 
      cjets[0].Pt() < 100 &&
      mct > 120 && mct < 250 &&
      myjets[0].pass(NOT(TrueCJet)) ){
    accept("SR1"); 
  }

  if( zeroLep && nJets30>=3 &&
      dphiMin > 0.4 && met > 500 &&
      myjets[0].Pt() > 250 && 
      cjets[0].Pt() > 60 &&
      mct > 120 && mct < 250 &&
      myjets[0].pass(NOT(TrueCJet)) ){
    accept("SR2"); 
  }

  if( zeroLep && nJets30>=3 &&
      dphiMin > 0.4 && met > 500 &&
      myjets[0].Pt() > 250 && 
      myjets[1].Pt() > 120 && 
      myjets[2].Pt() > 80 && 
      cjets[0].Pt() > 80 &&
      mct > 175 && mct < 400 &&
      myjets[0].pass(NOT(TrueCJet)) ){
    accept("SR3"); 
  }

  if( zeroLep && nJets30>=3 &&
      dphiMin > 0.4 && met > 500 &&
      myjets[0].Pt() > 250 && 
      myjets[1].Pt() > 140 && 
      myjets[2].Pt() > 120 && 
      cjets[0].Pt() > 100 &&
      mct > 200 &&
      myjets[0].pass(NOT(TrueCJet)) ){
    accept("SR4"); 
  }

  if( zeroLep && nJets30>=3 &&
      dphiMin > 0.4 && met > 500 &&
      myjets[0].Pt() > 300 && 
      myjets[1].Pt() > 200 && 
      myjets[2].Pt() > 150 && 
      cjets[0].Pt() > 150 &&
      mct > 400 ){
    accept("SR5"); 
  }
  
  return;
}
