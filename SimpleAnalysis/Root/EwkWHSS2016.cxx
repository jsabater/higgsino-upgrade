#include "SimpleAnalysis/AnalysisClass.h"
DefineAnalysis(EwkWHSS2016)

static AnalysisObjects pileupRemoval(const AnalysisObjects& cands,
                                     float pt, float eta, int id) {
    AnalysisObjects reducedList;
    for(const auto& cand : cands) {
      if  ( cand.Pt()<pt && fabs(cand.Eta())<eta ){
	if (cand.pass(id)) reducedList.push_back(cand); 
      }
      else  reducedList.push_back(cand);
    }
    return reducedList;
}


void EwkWHSS2016::Init() {

  addRegions({"SRSS_j1","SRSS_j23"});
 
}

void EwkWHSS2016::ProcessEvent(AnalysisEvent *event){

  //Get objects (baseline):
  auto baseline_electrons=event->getElectrons(10, 2.47, ELooseBLLH);  
  auto baseline_muons=event->getMuons(10, 2.4,MuMedium);
  auto baseline_jets=event->getJets(20., 2.8);


  // Reject events with bad muon (**NOT SURE if this cut is doing that...**)
  if (countObjects(baseline_muons,10,2.4,NOT(MuQoPSignificance)) != 0) return;


  // Overlap removal:
  baseline_jets = overlapRemoval(baseline_jets,baseline_electrons, 0.2, NOT(BTag77MV2c10)); //removing jets withing DR=0.2 of electrons (applied to NO b-jets)
  baseline_electrons  = overlapRemoval(baseline_electrons,baseline_jets, 0.4); //removing electrons within DR=0.4 of jets
  baseline_muons = overlapRemoval(baseline_muons,baseline_jets, 0.4); //removing muons within DR=0.4 of jets

  //Overlap removal for objects sharing the same track ID (simulated by Delta R < 0.01)
  baseline_muons = overlapRemoval(baseline_muons,baseline_electrons,0.01,MuCaloTaggedOnly); //removing Calo-tagged muons from electrons' cone
  baseline_electrons = overlapRemoval(baseline_electrons,baseline_muons,0.01,NOT(MuCaloTaggedOnly)); //removing electrons from muons' cone  

  
  // Jet Cleaning 
  if (countObjects(baseline_jets, 20, 2.8,NOT(LooseBadJet)) != 0) return;
 

  // Reject events with cosmic muon:
  if (countObjects(baseline_muons, 10, 2.4, NOT(MuNotCosmic)) !=0) return; 
 


  auto baseline_leptons=baseline_electrons+baseline_muons;

  // Select ==2 baseline leptons:
  if (baseline_leptons.size() != 2) return;
 


  //Get signal leptons:
  auto signal_electrons = filterObjects(baseline_electrons,25.,2.47,EMediumLH && EIsoFixedCutTight && EZ05mm && ED0Sigma5); 
  auto signal_muons = filterObjects(baseline_muons,25,2.4,MuIsoGradientLoose && MuZ05mm && MuD0Sigma3);
  auto signal_jets = filterObjects(baseline_jets,20,2.8);  

 
  signal_jets=pileupRemoval(signal_jets, 60., 2.4, JVT50Jet);
 
  auto bjets = filterObjects(signal_jets, 20., 2.4,BTag77MV2c10);


  auto signal_leptons = signal_electrons + signal_muons;

  // Select ==2 signal leptons:
  if (signal_leptons.size() !=2 ) return;  
 

  sortObjectsByPt(signal_leptons);
  sortObjectsByPt(signal_jets);

  auto lep1 = signal_leptons[0];
  auto lep2 = signal_leptons[1];


  // Two leptons must be same-sign.
  if (lep1.charge() != lep2.charge() ) return; 
    


  // No b-jets
  if (bjets.size() != 0) return;
 
  

  // Reducing to njets >= 1 and <=3 
  
  int njets=signal_jets.size();
  if (njets<1 || njets>3) return;
 

  //Defining variables to cut:
  
  auto MetVec = event->getMET(); 


  float DeltaEta = fabs(lep1.Eta()-lep2.Eta());
  float Met = MetVec.Et();
  float Mt = calcMT(lep1, MetVec); // Mt from leading lepton
  float Meff = Met + sumObjectsPt(signal_jets) + sumObjectsPt(signal_leptons);
  float Mt2 = calcMT2(lep1, lep2, MetVec);

  //Computing the invariant mass of jet system and the closest lepton
  float Mlj=-1;
  TLorentzVector jetSystem;
  if (njets == 1) jetSystem = signal_jets[0];   
  else jetSystem = signal_jets[0]+signal_jets[1];
 
  TLorentzVector closest_lepton=lep1; 
  if (jetSystem.DeltaR(lep2) < jetSystem.DeltaR(lep1)) closest_lepton=lep2;

  TLorentzVector jetSystem_lepton=jetSystem+closest_lepton;
  
  Mlj=jetSystem_lepton.M();

 
  if (njets ==1 && DeltaEta<=1.5 && Met>=100 && Mt>=140 && Meff>=260 && Mlj<180 && Mt2>=80) accept("SRSS_j1");
  else if (njets>=2 && njets<=3 && Met>=100 && Mt>=120 && Meff>=240 && Mlj<130 && Mt2>=70) accept("SRSS_j23");
 

 return;
}
