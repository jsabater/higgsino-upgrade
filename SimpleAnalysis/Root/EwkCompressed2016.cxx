#include "SimpleAnalysis/AnalysisClass.h"
#include <unordered_set>
#include <algorithm>

//---------------------------------------------------------
//  
//  SUSY EWK Compressed 'Higgsino' Analysis
//  ATL-SUSY-2016-25
//
//---------------------------------------------------------

DefineAnalysis(EwkCompressed2016)

void EwkCompressed2016::Init()
{
  addRegions( {"SRSF_iMLLa","SRSF_iMLLb","SRSF_iMLLc","SRSF_iMLLd","SRSF_iMLLe","SRSF_iMLLf","SRSF_iMLLg",
               "SRSF_eMLLa","SRSF_eMLLb","SRSF_eMLLc","SRSF_eMLLd","SRSF_eMLLe","SRSF_eMLLf","SRSF_eMLLg",
               "winobino_iMLLg","winobino_eMLLa","winobino_eMLLb","winobino_eMLLc","winobino_eMLLd","winobino_eMLLe","winobino_eMLLf","winobino_eMLLg",
               "SRSF_iMT2a","SRSF_iMT2b","SRSF_iMT2c","SRSF_iMT2d","SRSF_iMT2e","SRSF_iMT2f" } );
}

//---------------------------------------------------------

//---------------------------------------------------------  
double getWinoBinoFuncMllDistr(double x, double *par){
  double m1=par[1];
  double m2=par[2];
  double mu=m2-m1;
  double m=x;
  double M=m1+m2;
  double m_Z=91;
  double norm=par[0]*m;

  double radice=sqrt( pow(m,4) -pow(m,2)*(pow(mu,2) + pow(M,2) ) + pow(mu*M,2) );

  double normalizzazione = pow(  pow(m,2) -pow(m_Z,2),2);
  double var = (norm* radice)/ normalizzazione  ;
  var = var* (-2*pow(m,4)+ pow(m,2)*(2*pow(M,2) - pow(mu,2)) +pow((mu*M),2));
  if(x>fabs(m2) - fabs(m1)) var=1;
  return var; 
}
//---------------------------------------------------------

//--------------------------------------------------------- 
// Calculate wino bino Mll reweighting
float getWinoBinoMllWeight(int DSID, double mass){
  // See https://its.cern.ch/jira/browse/HIGGSINO-27:

  std::vector<double> parHvec;
  double winoBinoMllWeight = 1.0;

  if (DSID == 393596 || DSID == 393597) parHvec = {3.00003, 80.0, -82.0}; 
  else if (DSID == 393400 || DSID == 393449) parHvec = {3.00008, 80.0, -83.0};
  else if (DSID == 393401 || DSID == 393450) parHvec = {3.00031, 80.0, -85.0};
  else if (DSID == 393402 || DSID == 393451) parHvec = {3.00199, 80.0, -90.0};
  else if (DSID == 393403 || DSID == 393452) parHvec = {3.01389, 80.0, -100.0};
  else if (DSID == 393404 || DSID == 393453) parHvec = {3.10528, 80.0, -120.0};
  else if (DSID == 393405 || DSID == 393454) parHvec = {3.40493, 80.0, -140.0};
  else if (DSID == 394173 || DSID == 394174) parHvec = {3.00022, 100.0, -102.0};
  else if (DSID == 393407 || DSID == 393456) parHvec = {3.00049, 100.0, -103.0};
  else if (DSID == 393408 || DSID == 393457) parHvec = {3.00142, 100.0, -105.0};
  else if (DSID == 393409 || DSID == 393458) parHvec = {3.0061, 100.0, -110.0};
  else if (DSID == 393410 || DSID == 393459) parHvec = {3.02817, 100.0, -120.0};
  else if (DSID == 393411 || DSID == 393460) parHvec = {3.15156, 100.0, -140.0};
  else if (DSID == 393412 || DSID == 393461) parHvec = {3.50164, 100.0, -160.0};
  else if (DSID == 394177 || DSID == 394178) parHvec = {3.0004, 150.0, -152.0};
  else if (DSID == 393463 || DSID == 393414) parHvec = {3.00091, 150.0, -153.0};
  else if (DSID == 393415 || DSID == 393464) parHvec = {3.00254, 150.0, -155.0};
  else if (DSID == 393416 || DSID == 393465) parHvec = {3.01033, 150.0, -160.0};
  else if (DSID == 393417 || DSID == 393466) parHvec = {3.0435, 150.0, -170.0};
  else if (DSID == 393418 || DSID == 393467) parHvec = {3.20529, 150.0, -190.0};
  else if (DSID == 393419 || DSID == 393468) parHvec = {3.6223, 150.0, -210.0};
  else if (DSID == 394181 || DSID == 394182) parHvec = {3.00047, 200.0, -202.0};
  else if (DSID == 393421 || DSID == 393470) parHvec = {3.00105, 200.0, -203.0};
  else if (DSID == 393422 || DSID == 393471) parHvec = {3.00293, 200.0, -205.0};
  else if (DSID == 393423 || DSID == 393472) parHvec = {3.01187, 200.0, -210.0};
  else if (DSID == 393424 || DSID == 393473) parHvec = {3.04929, 200.0, -220.0};
  else if (DSID == 393425 || DSID == 393474) parHvec = {3.22707, 200.0, -240.0};
  else if (DSID == 393426 || DSID == 393475) parHvec = {3.67466, 200.0, -260.0};
  else if (DSID == 394185 || DSID == 394186) parHvec = {3.0005, 250.0, -252.0};
  else if (DSID == 393477 || DSID == 393428) parHvec = {3.00112, 250.0, -253.0};
  else if (DSID == 393429 || DSID == 393478) parHvec = {3.00312, 250.0, -255.0};
  else if (DSID == 393430 || DSID == 393479) parHvec = {3.0126, 250.0, -260.0};
  else if (DSID == 393431 || DSID == 393480) parHvec = {3.05208, 250.0, -270.0};
  else if (DSID == 393432 || DSID == 393481) parHvec = {3.23798, 250.0, -290.0};
  else if (DSID == 393433 || DSID == 393482) parHvec = {3.7019, 250.0, -310.0};
  else if (DSID == 394189 || DSID == 394190) parHvec = {3.00051, 300.0, -302.0};
  else if (DSID == 393435 || DSID == 393484) parHvec = {3.00116, 300.0, -303.0};
  else if (DSID == 393436 || DSID == 393485) parHvec = {3.00322, 300.0, -305.0};
  else if (DSID == 393437 || DSID == 393486) parHvec = {3.013, 300.0, -310.0};
  else if (DSID == 393438 || DSID == 393487) parHvec = {3.05363, 300.0, -320.0};
  else if (DSID == 393439 || DSID == 393488) parHvec = {3.24421, 300.0, -340.0};
  else if (DSID == 393440 || DSID == 393489) parHvec = {3.71784, 300.0, -360.0};
  else if (DSID == 394193 || DSID == 394194) parHvec = {3.00053, 400.0, -402.0};
  else if (DSID == 393491 || DSID == 393442) parHvec = {3.0012, 400.0, -403.0};
  else if (DSID == 393443 || DSID == 393492) parHvec = {3.00333, 400.0, -405.0};
  else if (DSID == 393444 || DSID == 393493) parHvec = {3.0134, 400.0, -410.0};
  else if (DSID == 393445 || DSID == 393494) parHvec = {3.05521, 400.0, -420.0};
  else if (DSID == 393446 || DSID == 393495) parHvec = {3.25068, 400.0, -440.0};
  else if (DSID == 393447 || DSID == 393496) parHvec = {3.73475, 400.0, -460.0};
  else return winoBinoMllWeight; // mass splitting >= 100 have mll reweighting value set to 1

  double parH[3] = {parHvec.at(0), parHvec.at(1), parHvec.at(2)};
  double parS[3] = {1.0, parH[1], -1*parH[2]};
  
  double hig_w = getWinoBinoFuncMllDistr(mass,parH);
  double stop_w = getWinoBinoFuncMllDistr(mass,parS);
  winoBinoMllWeight = stop_w/hig_w;
  
  return winoBinoMllWeight;
}
//---------------------------------------------------------  

void EwkCompressed2016::ProcessEvent(AnalysisEvent *event)
{
  //---------------------------------------------------------
  //
  //                   Prepare objects
  //
  //---------------------------------------------------------

  auto baseElectrons  = event->getElectrons(4.5, 2.47, EVeryLooseLH);
  auto baseMuons      = event->getMuons(4, 2.5, MuMedium);
  auto preJets        = event->getJets(20., 2.8); 
  auto metVec         = event->getMET();
  float met = metVec.Pt();

  // Reject events with bad jets
  if (countObjects(preJets, 20, 4.5, NOT(LooseBadJet))!=0) return;

  // hard lepton overlap removal and signal lepton/jet definitions
  auto baseJets        = overlapRemoval(preJets,       baseElectrons, 0.2, NOT(BTag85MV2c10));
       baseElectrons   = overlapRemoval(baseElectrons, baseJets,      0.4);
       baseElectrons   = overlapRemoval(baseElectrons, baseMuons,     0.01);
       baseJets        = overlapRemoval(baseJets,      baseMuons,     0.4, LessThan3Tracks);
       baseMuons       = overlapRemoval(baseMuons,     baseJets, 0.4);
  auto signalJets      = filterObjects( baseJets,      30,  2.8,  JVT50Jet);
  auto signalBJets     = filterObjects( signalJets,    30,  2.5,  BTag85MV2c10);
  auto signalElectrons = filterObjects( baseElectrons, 4.5, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signalMuons     = filterObjects( baseMuons,     4,   2.5,  MuD0Sigma3|MuZ05mm|MuIsoFixedCutTightTrackOnly);

  AnalysisObjects baseLeptons   = baseElectrons+baseMuons;
  AnalysisObjects signalLeptons = signalElectrons+signalMuons;
  
  //---------------------------------------------------------
  //
  //                Calculate variables
  //
  //---------------------------------------------------------
  
  float jet1Pt    = signalJets.size() >= 1 ? signalJets[0].Pt() : -1.;
  float dPhiJ1Met = signalJets.size() >= 1 ? fabs( signalJets[0].DeltaPhi( metVec ) ) : -1.; 

  // Minimum of the delta phi between all jets with MET
  float minDPhiAllJetsMet = minDphi(metVec,signalJets);

  // Preselect exactly 2 baseline leptons (as derivation does)
  // Final selection requires exactly 2 signal leptons; signal leptons are subset of baseline
  if ( !(baseLeptons.size() == 2) ) return;

  float lep1Pt = baseLeptons[0].Pt();
  float lep2Pt = baseLeptons[1].Pt();

  // SFOS decision
  bool isOS = ( baseLeptons[0].charge() != baseLeptons[1].charge() ); // opposite charge
  bool isSF = ( baseLeptons[0].type() == baseLeptons[1].type() ) ; // same flavour
  
  // Higher level leptonic variables
  float mll          = (baseLeptons[0]+baseLeptons[1]).M();
  float drll         = baseLeptons[0].DeltaR(baseLeptons[1]);
  float metOverHtLep = met / sumObjectsPt(baseLeptons,999,4); 
  float MTauTau      = calcMTauTau(baseLeptons[0], baseLeptons[1], metVec);
  float mt           = calcMT( baseLeptons[0],metVec );
  
  // mT2 with trial invisible particle mass at 100 GeV
  float mn = 100.; 
  //  float mt2 = calc_massive_MT2(baseLeptons[0], baseLeptons[1], metVec, mn);   
  float mt2=calcAMT2(baseLeptons[0],baseLeptons[1],metVec,mn,mn);

  // SUSYTools final state (fs) code
  int fs = event->getSUSYChannel();
  // stau veto for slepton signal - note this does not affect the denominator in the acceptance numbers
  bool isNotStau = ( fs != 206 && fs != 207 );

  int mcChannel = event->getMCNumber();
  //std::cout << "DSID: " << mcChannel <<std::endl;

  double winoBinoMllWeight = getWinoBinoMllWeight(mcChannel, mll);
  //---------------------------------------------------------
  //
  //                Perform selection
  //
  //---------------------------------------------------------
  
  // Common cuts
  bool pass_common_cuts = false;
  if (    ( isNotStau )
       && ( signalBJets.size()   == 0 ) 
       && ( met                  >  200. ) 
       && ( jet1Pt               >  100. )
       && ( dPhiJ1Met            >  2.0 )
       && ( minDPhiAllJetsMet    >  0.4 )
       && ( signalLeptons.size() == 2 )  
       && ( isOS && isSF )
       && ( lep1Pt > 5. )
       && ( MTauTau < 0. || MTauTau > 160. )
       && ( mll > 1. && mll < 60. )
       && ( mll < 3. || mll > 3.2 )
       && ( drll > 0.05 )
      ) pass_common_cuts = true;

  bool keep_SRMLL = pass_common_cuts && ( metOverHtLep > std::max( 5., 15. - 2. * mll ) ) && drll < 2.0 && mt < 70.;
  bool keep_SRMT2 = pass_common_cuts &&   metOverHtLep > std::max( 3., 15. - 2. * (mt2 - mn) );

  // Inclusive mll regions
  if (keep_SRMLL && mll < 3)  accept("SRSF_iMLLa");
  if (keep_SRMLL && mll < 5)  accept("SRSF_iMLLb");
  if (keep_SRMLL && mll < 10) accept("SRSF_iMLLc");
  if (keep_SRMLL && mll < 20) accept("SRSF_iMLLd");
  if (keep_SRMLL && mll < 30) accept("SRSF_iMLLe");
  if (keep_SRMLL && mll < 40) accept("SRSF_iMLLf");
  if (keep_SRMLL && mll < 60) accept("SRSF_iMLLg");

  // Exclusive mll region
  if (keep_SRMLL && mll < 3)  accept("SRSF_eMLLa");
  else if (keep_SRMLL && mll < 5)  accept("SRSF_eMLLb");
  else if (keep_SRMLL && mll < 10) accept("SRSF_eMLLc");
  else if (keep_SRMLL && mll < 20) accept("SRSF_eMLLd");
  else if (keep_SRMLL && mll < 30) accept("SRSF_eMLLe");
  else if (keep_SRMLL && mll < 40) accept("SRSF_eMLLf");
  else if (keep_SRMLL && mll < 60) accept("SRSF_eMLLg");
  
  // include mll reweighting for wino/bino
  if (keep_SRMLL && mll < 60) accept("winobino_iMLLg",winoBinoMllWeight);
  if (keep_SRMLL && mll < 3)  accept("winobino_eMLLa", winoBinoMllWeight);
  else if (keep_SRMLL && mll < 5)  accept("winobino_eMLLb", winoBinoMllWeight);
  else if (keep_SRMLL && mll < 10) accept("winobino_eMLLc", winoBinoMllWeight);
  else if (keep_SRMLL && mll < 20) accept("winobino_eMLLd", winoBinoMllWeight);
  else if (keep_SRMLL && mll < 30) accept("winobino_eMLLe", winoBinoMllWeight);
  else if (keep_SRMLL && mll < 40) accept("winobino_eMLLf", winoBinoMllWeight);
  else if (keep_SRMLL && mll < 60) accept("winobino_eMLLg", winoBinoMllWeight);

  // Inclusive mT2 regions
  if (keep_SRMT2 && mt2 < 102)  accept("SRSF_iMT2a");
  if (keep_SRMT2 && mt2 < 105)  accept("SRSF_iMT2b");
  if (keep_SRMT2 && mt2 < 110)  accept("SRSF_iMT2c");
  if (keep_SRMT2 && mt2 < 120)  accept("SRSF_iMT2d");
  if (keep_SRMT2 && mt2 < 130)  accept("SRSF_iMT2e");
  if (keep_SRMT2 && mt2 >= 100) accept("SRSF_iMT2f"); 
  
  // Fill in optional ntuple variables
  ntupVar("FS",                fs);               
  ntupVar("DSID",              mcChannel);
  ntupVar("met_Et",            met);               
  ntupVar("nLep_base",         static_cast<int>(baseLeptons.size()));               
  ntupVar("nLep_signal",       static_cast<int>(signalLeptons.size()));               
  ntupVar("nBJet20_MV2c10",    static_cast<int>(signalBJets.size()));               
  ntupVar("isOS",              isOS);               
  ntupVar("isSF",              isSF);               
  ntupVar("DPhiJ1Met",         dPhiJ1Met);               
  ntupVar("lep1Pt",            lep1Pt);               
  ntupVar("lep2Pt",            lep2Pt);               
  ntupVar("jet1Pt",            jet1Pt);               
  ntupVar("mll",               mll);               
  ntupVar("Rll",               drll);               
  ntupVar("mt_lep1",           mt);               
  ntupVar("mt2leplsp_100",     mt2);               
  ntupVar("MTauTau",           MTauTau);               
  ntupVar("METOverHTLep",      metOverHtLep);               
  ntupVar("minDPhiAllJetsMet", minDPhiAllJetsMet);               
  ntupVar("mc_weight",         event->getMCWeights()[0]);  
  ntupVar("winoBinoMllWeight", winoBinoMllWeight);
  
}
