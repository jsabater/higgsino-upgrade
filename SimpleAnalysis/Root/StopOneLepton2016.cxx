#include <cmath>
#include <fastjet/ClusterSequence.hh>
#include "SimpleAnalysis/AnalysisClass.h"

#define gev 1000

#ifdef ROOTCORE_PACKAGE_Ext_RestFrames

DefineAnalysis(StopOneLepton2016)

	struct ClusteringHistory : public fastjet::PseudoJet::UserInfoBase {
		enum Status {
			GOOD,
			JET_TOO_SMALL,
			JET_TOO_LARGE,
			TOO_MANY_ITERATIONS,
			NONE,
		};

		struct Step {
			double pt;
			double r;
			size_t constit;
			Status status;
		};

		size_t id;  // a per-event unique jet id that is needed for the event dump
		std::vector<Step> steps;

		static ClusteringHistory* AddStep(ClusteringHistory& history, const Step& step) {
			auto newHistory = new ClusteringHistory(history);
			newHistory->steps.push_back(step);
			return newHistory;
		}
	};

// Return the history of a PseudoJet object, handling all the ugly casting.
ClusteringHistory& GetHistory(const fastjet::PseudoJet& jet) {
	auto shared_ptr = jet.user_info_shared_ptr();
	return *dynamic_cast<ClusteringHistory*>(shared_ptr.get());
}

AnalysisObjects filterPtWindow(const AnalysisObjects& cands, float ptCut_low, float ptCut_high, float etaCut,
		int id) {
	AnalysisObjects reducedList;
	for (const auto& cand : cands) {
		if ((cand.Pt() >= ptCut_low && cand.Pt() < ptCut_high) && (fabs(cand.Eta()) < etaCut) && (cand.pass(id)))
			reducedList.push_back(cand);
	}
	return reducedList;
}

inline double optimalRadius(const double pT, const double m) { return 2 * m / pT; }
inline double minRadius(const double pT, const double m) { return optimalRadius(pT, m) - 0.3; }
inline double maxRadius(const double pT, const double m) {
	return optimalRadius(pT, m) + 0.5;
}

static std::vector<fastjet::PseudoJet> SortedByNConstit(std::vector<fastjet::PseudoJet> jets) {
	std::sort(jets.begin(), jets.end(), [](const fastjet::PseudoJet& a, const fastjet::PseudoJet& b) {
			if (a.constituents().size() != b.constituents().size())
			return a.constituents().size() > b.constituents().size();

			return a.pt() > b.pt();
			});

	return jets;
}

std::pair<bool, fastjet::PseudoJet> RecursiveRecluster(const fastjet::PseudoJet& candidate, double candRadius,
		const double mass, size_t step) {
	if (minRadius(candidate.pt(), mass) > candRadius) {
		GetHistory(candidate).steps.back().status = ClusteringHistory::JET_TOO_SMALL;
		return std::make_pair(false, candidate);
	} else if (maxRadius(candidate.pt(), mass) < candRadius) {
		const double newR = std::max(maxRadius(candidate.pt(), mass), candRadius / 2.);
		GetHistory(candidate).steps.back().status = ClusteringHistory::JET_TOO_LARGE;

		if (step > 10) {
			GetHistory(candidate).steps.back().status = ClusteringHistory::TOO_MANY_ITERATIONS;
			return std::make_pair(false, candidate);
		}

		fastjet::JetDefinition jetDef(fastjet::antikt_algorithm, newR);
		auto cs = new fastjet::ClusterSequence(candidate.constituents(), jetDef);

		std::vector<fastjet::PseudoJet> reclusteredJets;
		reclusteredJets = SortedByNConstit(cs->inclusive_jets());

		if (reclusteredJets.size() == 0) {
			delete cs;
			return std::make_pair(false, fastjet::PseudoJet());
		}

		cs->delete_self_when_unused();
		auto newCandidate = reclusteredJets[0];

		auto newHistory = ClusteringHistory::AddStep(
				GetHistory(candidate),
				{newCandidate.pt(), newR, newCandidate.constituents().size(), ClusteringHistory::NONE});
		newCandidate.set_user_info(newHistory);

		return RecursiveRecluster(newCandidate, newR, mass, step + 1);
	} else {
		GetHistory(candidate).steps.back().status = ClusteringHistory::GOOD;
		return std::make_pair(true, candidate);
	}
}

AnalysisObject reclusteredParticle(AnalysisObjects& jets, const AnalysisObjects& bjets, const double mass,
		const bool useBJets) {
  //  AnalysisObject p = AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
  AnalysisObject p = AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0,0,0);
	double r0 = 3.0;

	auto usejets = jets;
	if (useBJets && bjets.size()) usejets = jets + bjets;

	std::vector<fastjet::PseudoJet> initialJets;
	for (const auto& jet : usejets) {
		fastjet::PseudoJet Pjet(jet.Px(), jet.Py(), jet.Pz(), jet.E());
		initialJets.push_back(Pjet);
	}

	fastjet::JetDefinition jetDef(fastjet::antikt_algorithm, r0);
	fastjet::ClusterSequence cs(initialJets, jetDef);

	auto candidates = fastjet::sorted_by_pt(cs.inclusive_jets());

	std::vector<fastjet::PseudoJet> selectedJets;
	selectedJets.reserve(candidates.size());
	std::vector<fastjet::PseudoJet> badJets;
	badJets.reserve(candidates.size());

	size_t i = 0;
	for (auto& cand : candidates) {
		auto history = new ClusteringHistory();
		history->id = i;
		history->steps.push_back({cand.pt(), r0, cand.constituents().size(), ClusteringHistory::NONE});
		cand.set_user_info(history);
		++i;
	}

	for (const auto& cand : candidates) {
		bool selected = false;
		fastjet::PseudoJet jet;

		std::tie(selected, jet) = RecursiveRecluster(cand, r0, mass, 0);

		if (selected)
			selectedJets.push_back(jet);
		else
			badJets.push_back(jet);
	}

	if (selectedJets.size() < 1) {
		return p;
	}

	AnalysisObjects aoSelectedJets;
	for (const auto& jet : selectedJets)
		aoSelectedJets.push_back(
					 //AnalysisObject(jet.px(), jet.py(), jet.pz(), jet.E(), 0, 0, AnalysisObjectType::COMBINED, 0, 0));
					 AnalysisObject(jet.px(), jet.py(), jet.pz(), jet.E(), 0, 0, AnalysisObjectType::COMBINED, 0, 0,0,0));

	AnalysisClass::sortObjectsByPt(aoSelectedJets);
	p = aoSelectedJets[0];

	return p;
}

void StopOneLepton2016::Init() {
	addRegions({"tN_med", "tN_high"});
	addRegions({"tN_diag_low", "tN_diag_med", "tN_diag_high"});
	addRegions({"bWN", "bffN"});
	addRegions({"bC2x_med", "bC2x_diag", "bCbv"});
	addRegions({"bCsoft_diag", "bCsoft_med", "bCsoft_high"});
	addRegions({"DM_low_loose", "DM_low", "DM_high"});

	addHistogram("Met",20,0,2000);
	addHistogram("topReclM",20,0,400);
	addHistogram("htsig",20,-10,10);
	addHistogram("m_topchi2",20,0,400);

	LabRecoFrame* LAB = m_RF_helper.addLabFrame("LAB");
	DecayRecoFrame* CM = m_RF_helper.addDecayFrame("CM");
	DecayRecoFrame* S = m_RF_helper.addDecayFrame("S");
	VisibleRecoFrame* ISR = m_RF_helper.addVisibleFrame("ISR");
	VisibleRecoFrame* V = m_RF_helper.addVisibleFrame("V");
	InvisibleRecoFrame* I = m_RF_helper.addInvisibleFrame("I");

	LAB->SetChildFrame(*CM);
	CM->AddChildFrame(*ISR);
	CM->AddChildFrame(*S);
	S->AddChildFrame(*V);
	S->AddChildFrame(*I);

	LAB->InitializeTree();

	InvisibleGroup* INV = m_RF_helper.addInvisibleGroup("INV");
	INV->AddFrame(*I);
	CombinatoricGroup* VIS = m_RF_helper.addCombinatoricGroup("VIS");
	VIS->AddFrame(*ISR);
	VIS->SetNElementsForFrame(*ISR, 1, false);
	VIS->AddFrame(*V);
	VIS->SetNElementsForFrame(*V, 0, false);

	// set the invisible system mass to zero
	InvisibleJigsaw* InvMass = m_RF_helper.addInvisibleJigsaw("InvMass", kSetMass);
	INV->AddJigsaw(*InvMass);

	// define the rule for partitioning objects between "ISR" and "V"
	MinMassesCombJigsaw* SplitVis = m_RF_helper.addCombinatoricJigsaw("CombPPJigsaw", kMinMasses);
	VIS->AddJigsaw(*SplitVis);
	// "0" group (ISR)
	SplitVis->AddFrame(*ISR, 0);
	// "1" group (V + I)
	SplitVis->AddFrame(*V, 1);
	SplitVis->AddFrame(*I, 1);

	LAB->InitializeAnalysis();

	/* Initialise BDT readers */
	const std::string xmlFiles[3][2] = {
		{"StopOneLepton2016_BDT-tN_diag_low.weights1.xml", "StopOneLepton2016_BDT-tN_diag_low.weights2.xml"},
		{"StopOneLepton2016_BDT-tN_diag_med.weights1.xml", "StopOneLepton2016_BDT-tN_diag_med.weights2.xml"},
		{"StopOneLepton2016_BDT-tN_diag_high.weights1.xml", "StopOneLepton2016_BDT-tN_diag_high.weights2.xml"},
	};

	const std::vector<std::vector<std::string> > variableDefs{{
		/* Input variables for tN_diag_low */
		{"MET", "MT", "dMT200", "m_tophad", "m_toplep200", "dphi_lep_nu200", "dphi_rjet_lep"},
			/* Input variables for tN_diag_med */
			{"met", "mt", "m_top_chi2", "ht_sig", "dr_bjet_lep", "dphi_ttbar", "dphi_hadtop_met", "n_jet", "jet_pt[2]", "jet_pt[3]"},
			/* Input variables for tN_diag_high */
			{"mt", "dphi_ttbar", "dr_bjet_lep", "jetPt3", "jetPt4", "m_top_chi2", "RJR_RISR", "RJR_MS", "RJR_dphiISRI", "RJR_NjV"},
	}};

	const std::vector<std::string> methodname{ "BDT", "BDT", "BDTG" };
	for (unsigned int i = 0; i < sizeof(xmlFiles) / sizeof(xmlFiles[0]); ++i) {
		BDT::BDTReader* reader = new BDT::BDTReader(methodname[i], variableDefs[i], xmlFiles[i][0], xmlFiles[i][1]);
		m_BDTReaders.push_back(reader);
	}
}

void StopOneLepton2016::ProcessEvent(AnalysisEvent* event) {
	auto baseElectrons  = event->getElectrons(5, 2.47, EVeryLooseLH);
	auto baseMuons      = event->getMuons(4,2.7, MuLoose);
	auto baseJets       = event->getJets(20., 4.5);
	auto baseTaus       = event->getTaus(20., 2.5, TauLoose);
	auto metVec         = event->getMET();
	float Met = metVec.Pt();

	// Reject events with bad jets
	// CHECK: right place?
	if (countObjects(baseJets, 20, 4.5, NOT(LooseBadJet)) != 0) return;

	// overlap removal
	auto radiusCalcLepton = [](const AnalysisObject& lepton, const AnalysisObject&) {
		return std::max(0.1,std::min(0.4, 0.04 + 10 / lepton.Pt()));
	};

	auto muJetSpecial = [](const AnalysisObject& jet, const AnalysisObject& muon) {
		if (jet.pass(NOT(BTag77MV2c20)) && (jet.pass(LessThan3Tracks) || muon.Pt() / jet.Pt() > 0.7))
			return 0.2;
		else
			return 0.;
	};

	/* actually the Delta R requirement is a shared track but here this is simulated by Delta R < 0.01 */
	baseMuons     = overlapRemoval(baseMuons, baseElectrons, 0.01, NOT(MuCaloTaggedOnly));
	baseElectrons = overlapRemoval(baseElectrons, baseMuons, 0.01);

	baseJets      = overlapRemoval(baseJets, baseElectrons, 0.2, NOT(BTag77MV2c20));
	baseElectrons = overlapRemoval(baseElectrons,baseJets, radiusCalcLepton);

	baseJets      = overlapRemoval(baseJets, baseMuons, muJetSpecial, NOT(BTag77MV2c20));
	baseMuons     = overlapRemoval(baseMuons,baseJets, radiusCalcLepton);


	baseTaus      = overlapRemoval(baseTaus, baseElectrons, 0.1);

	auto baseLeptons     = baseElectrons+baseMuons;
	auto signalJets      = filterObjects(baseJets, 25, 2.5, JVT50Jet);
	if (signalJets.size() > 0) sortObjectsByPt(signalJets); // ensure these are pT sorted
	auto signalBJets     = filterObjects(signalJets, 25, 2.5, BTag77MV2c20);
	AnalysisObjects mostBjetLike;
	AnalysisObjects signalNotBjetLike;
	AnalysisObjects signalNotBjet;

	auto signalSoftElectrons = filterObjects(
			baseElectrons, 5, 2.47, (ELooseBLLH || ETightLH) && (ELooseLH || ETightLH) && ED0Sigma5 && EZ05mm);
	auto signalElectrons = filterObjects(signalSoftElectrons, 25, 2.47);
	auto signalSoftMuons =
		filterObjects(baseMuons, 4, 2.7, MuMedium && MuIsoFixedCutTightTrackOnly && MuD0Sigma3 && MuZ05mm);
	auto signalMuons = filterObjects(signalSoftMuons, 25, 2.7);
	auto signalLeptons = signalElectrons + signalMuons;
	auto signalSoftLeptons = signalSoftElectrons + signalSoftMuons;

	int nBjets = signalBJets.size();
	int nJets = signalJets.size();


	ntupVar("n_jet", nJets);
	ntupVar("n_bjet", nBjets);
	ntupVar("n_lep", (int)baseLeptons.size());
	ntupVar("n_lep_soft", (int)signalSoftLeptons.size());  
	ntupVar("n_lep_hard", (int)signalLeptons.size() );
	ntupVar("mc_weight", event->getMCWeights()[0]);
	ntupVar("met", Met);
	/* bail out for the event if minimal common event selection criteria are not fullfilled */
	if (!(Met > 100 && baseLeptons.size() == 1 &&
				(signalSoftLeptons.size() == 1 || signalLeptons.size() == 1) && nJets > 1))
		return;
	// in the following the baseLepton is always used for calculations, to avoid requiring to check for soft or
	// hard, as only exactly one baseLepton is allowed in the analysis

	// create containers with exactly 2 jets being considered to be b-jets and the inverse
	int bJet1 = -1, bJet2 = -1;
	for (unsigned int i = 0; i < signalJets.size(); ++i) {
		if (signalJets[i].pass(NOT(BTag77MV2c20))) continue;
		if (bJet1 == -1)
			bJet1 = i;
		else if (bJet2 == -1) {
			bJet2 = i;
			break;
		}
	}
	if (bJet2 == -1)
		for (unsigned int i = 0; i < signalJets.size(); ++i) {
			if (signalJets[i].pass(BTag77MV2c20)) continue;
			if (bJet1 == -1)
				bJet1 = i;
			else if (bJet2 == -1) {
				bJet2 = i;
				break;
			}
		}
	mostBjetLike.push_back(signalJets.at(bJet1));
	mostBjetLike.push_back(signalJets.at(bJet2));
	for (int i = 0; i < (int)signalJets.size(); ++i) {
		if (signalJets[i].pass(NOT(BTag77MV2c20)))
			signalNotBjet.push_back(signalJets.at(i));
		if (i == bJet1 || i == bJet2) continue;
		signalNotBjetLike.push_back(signalJets.at(i));
	}

	/* ensure object collections to be pT sorted */
	sortObjectsByPt(signalJets);
	if (baseTaus.size() > 0) sortObjectsByPt(baseTaus);

	/* jet resolution */
	auto signalJER = fakeJER(signalJets);

	float sigmaAbsHtMiss = 0;

	/* calculate vecHtMiss */
	//	AnalysisObject* vecHtMiss = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::MET, 0, 0);
	AnalysisObject* vecHtMiss = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::MET, 0, 0,0,0);
	//	AnalysisObject* leptonHtMiss = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::MET, 0, 0);
	AnalysisObject* leptonHtMiss = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::MET, 0, 0,0,0);
	for (unsigned int i = 0; i < baseLeptons.size(); ++i) {
		*vecHtMiss    -= baseLeptons[i];
		*leptonHtMiss -= baseLeptons[i];
	}

	for (unsigned int i = 0; i < signalJets.size(); ++i)
		*vecHtMiss -= signalJets[i];

	/* calculate Ht and HtSig */
	float Ht = 0;
	for (auto jet : signalJets) Ht += jet.Pt();
	//float HtSig = Met / sqrt(Ht); //not used, avoid confusions

	// variations
	TRandom3 myRandom;
	myRandom.SetSeed(signalJets[0].Pt());  // maybe use event number?
	int PEs = 100;
	double smear_factor, ETmissmean = 0, ETmissRMS = 0;
	for (int j = 0; j < PEs; ++j) {
		double jetHtx = leptonHtMiss->Px();
		double jetHty = leptonHtMiss->Py();
		for (unsigned int i = 0; i < signalJets.size(); ++i) {
			jetHtx -= myRandom.Gaus(signalJets[i].Px(), signalJets[i].Px() * signalJER[i]);
			jetHty -= myRandom.Gaus(signalJets[i].Py(), signalJets[i].Px() * signalJER[i]);
		}
		double ETtemp = sqrt(jetHtx * jetHtx + jetHty * jetHty);
		ETmissmean += ETtemp;
		ETmissRMS  += ETtemp * ETtemp;
	}
	ETmissmean = ETmissmean / PEs;
	sigmaAbsHtMiss = sqrt((ETmissRMS / PEs) - ETmissmean * ETmissmean);

	double HtSigMiss = (ETmissmean - 100.) / sigmaAbsHtMiss;

	double absDPhiJMet[4] = {fabs(signalJets[0].DeltaPhi(metVec)), fabs(signalJets[1].DeltaPhi(metVec)),
		signalJets.size() > 2 ? fabs(signalJets[2].DeltaPhi(metVec)) : NAN,
		signalJets.size() > 3 ? fabs(signalJets[3].DeltaPhi(metVec)) : NAN};
	double absDPhiJiMet = absDPhiJMet[0];

	for (int i = 1; i < 4; i++)
		if (absDPhiJMet[i] < absDPhiJiMet) absDPhiJiMet = absDPhiJMet[i];

	float mT = calcMT(baseLeptons[0], metVec);
	float dPhiMetLep = fabs(metVec.DeltaPhi(baseLeptons[0]));
	//float dphiMin = minDphi(metVec, signalJets, 2); //not used avoid confusions
	float mT2Tau = (baseTaus.size() > 0 ? calcMT2(baseTaus[0], baseLeptons[0], metVec) : 120);
	float pTLepOverMet = baseLeptons[0].Pt() / Met;

	bool preselHighMet = Met > 230 && mT > 30;
	bool preselLowMet  =  baseLeptons[0].Pt() > 27 && signalBJets.size() > 0 && signalJets[0].Pt() > 50 && Met > 100 && mT > 90;
	if (baseLeptons[0].type() == ELECTRON)
		preselLowMet = preselLowMet && baseLeptons[0].pass(ETightLH);

	float amT2 = std::min(calcAMT2(mostBjetLike[0] + baseLeptons[0], mostBjetLike[1], metVec, 0., 80),
			calcAMT2(mostBjetLike[1] + baseLeptons[0], mostBjetLike[0], metVec, 0., 80));

	float dRbl = baseLeptons[0].DeltaR(mostBjetLike[0]);

	/* Reconstruct top by a chi2 based method */
	float mW = 80.;
	float mTop = 170.;
	float chi2min = 9e99;
	//	AnalysisObject* topChi2 = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
	AnalysisObject* topChi2 = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0,0,0);
	int jetComb[3] = {0, 0, 0};
	auto signalBJER = fakeJER(mostBjetLike);
	float f;

	for (int i = 0; i < (int)signalJets.size(); ++i) {
		if (i == bJet1 || i == bJet2) continue;
		for (int j = i + 1; j < (int)signalJets.size(); ++j) {
			if (j == bJet1 || j == bJet2) continue;
			for (unsigned int k = 0; k < mostBjetLike.size() && k < 2; ++k) {
				f = pow((signalJets[i] + signalJets[j] + mostBjetLike[k]).M() - mTop, 2) /
					(pow((signalJets[i] + signalJets[j] + mostBjetLike[k]).M(), 2) *
					 (pow(signalJER[i], 2) + pow(signalJER[j], 2) + pow(signalBJER[k], 2))) +
					pow((signalJets[i] + signalJets[j]).M() - mW, 2) /
					(pow((signalJets[i] + signalJets[j]).M(), 2) * (pow(signalJER[i], 2) + pow(signalJER[j], 2)));
				if (f < chi2min) {
					chi2min = f;
					jetComb[0] = i;
					jetComb[1] = j;
					jetComb[2] = k;
				}
			}
		}
	}
	*topChi2 = signalJets[jetComb[0]] + signalJets[jetComb[1]] + mostBjetLike[jetComb[2]];
	fill("m_topchi2", topChi2->M());

	//	AnalysisObject* top1 = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
	AnalysisObject* top1 = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0,0,0);
	*top1 = baseLeptons[0] + (jetComb[2] == 0 ? mostBjetLike[1] : mostBjetLike[0]);

	/* calculate MetPerp */
	float MetPerp = NAN;
	AnalysisObject ttbar = *topChi2 + *top1;
	AnalysisObject top1Rest = *top1;
	AnalysisObject metRest = metVec;
	top1Rest.Boost(-ttbar.Px() / ttbar.E(), -ttbar.Py() / ttbar.E(), -ttbar.Pz() / ttbar.E());
	metRest.Boost(-ttbar.Px() / ttbar.E(), -ttbar.Py() / ttbar.E(), -ttbar.Pz() / ttbar.E());
	MetPerp = metRest.Vect().XYvector().Norm(top1Rest.Vect().XYvector()).Mod();

	/* rISR calculation */
	float rISR = NAN;
	float dphiISRI = NAN;
	float mTS = NAN;

	LabRecoFrame* LAB = m_RF_helper.getLabFrame("LAB");
	InvisibleGroup* INV = m_RF_helper.getInvisibleGroup("INV");
	CombinatoricGroup* VIS = m_RF_helper.getCombinatoricGroup("VIS");

	LAB->ClearEvent();
	std::vector<RFKey> jetID;

	for (const auto& jet : signalJets) jetID.push_back(VIS->AddLabFrameFourVector(jet.transFourVect()));

	INV->SetLabFrameThreeVector(metVec.Vect());
	if (!LAB->AnalyzeEvent()) std::cout << "Something went wrong..." << std::endl;

	DecayRecoFrame* CM = m_RF_helper.getDecayFrame("CM");
	DecayRecoFrame* S = m_RF_helper.getDecayFrame("S");
	VisibleRecoFrame* ISR = m_RF_helper.getVisibleFrame("ISR");
	VisibleRecoFrame* V = m_RF_helper.getVisibleFrame("V");
	InvisibleRecoFrame* I = m_RF_helper.getInvisibleFrame("I");

	int nJV = 0;
	for (unsigned int i = 0; i < signalJets.size(); ++i)
		if (VIS->GetFrame(jetID[i]) == *V) ++nJV;

	if (nJV > 0) {
		rISR = fabs(I->GetFourVector(*CM).Vect().Dot(ISR->GetFourVector(*CM).Vect().Unit())) /
			ISR->GetFourVector(*CM).Vect().Mag();
		dphiISRI = fabs(ISR->GetFourVector(*CM).DeltaPhi(I->GetFourVector(*CM)));
		mTS = S->GetMass();
	}

	AnalysisObject WRecl = reclusteredParticle(signalNotBjet, mostBjetLike, mW, false); //signalNotBjet+mostBjetLike is inconsistent but bjets are not used anyway
	AnalysisObject topRecl = reclusteredParticle(signalNotBjetLike, mostBjetLike, 175., true);

	if (nBjets > 0 && nJets > 3 && preselHighMet) {
		fill("topReclM", topRecl.M());
	}
	fill("Met", Met);
	fill("htsig", HtSigMiss);

	float BDT_tN_diag_low = NAN;
	float BDT_tN_diag_med = NAN;
	float BDT_tN_diag_high = NAN;  

	float DMTalpha = NAN;
	float MTopLep200 = NAN;
	float DPhiLepNu200 = NAN;    
	float DPhiRJetLep = NAN;

	// non-soft lepton selections
	if (signalLeptons.size() == 1) {
		//tN_med
		if (nJets > 3 && nBjets > 0 && preselLowMet && signalJets[0].Pt() > 60 && signalJets[1].Pt() > 50 &&
				signalJets[3].Pt() > 40 && Met > 250 && MetPerp > 230 && HtSigMiss > 14 && mT > 160 && amT2 > 175 &&
				topRecl.M() > 150 && dRbl < 2.0 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 && mT2Tau > 80)
			accept("tN_med");
		//tN_high
		if (nJets > 3 && nBjets > 0 && preselHighMet && signalJets[0].Pt() > 100 && signalJets[1].Pt() > 80 &&
				signalJets[2].Pt() > 50 && signalJets[3].Pt() > 30 && Met > 550 && HtSigMiss > 27 && mT > 160 &&
				amT2 > 175 && topRecl.M() > 130 && dRbl < 2.0 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 &&
				mT2Tau > 80)
			accept("tN_high");
		//tN_diag_low
		if (nJets > 3 && nBjets > 0 && preselLowMet && signalJets[0].Pt() > 120 && Met > 100 && mT > 90 &&
				absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4) {
		  //		        AnalysisObject top0(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
		  AnalysisObject top0(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0,0,0);

			float chi2min = DBL_MAX;
			float chi2_1, chi2_2;
			const float alpha = 27. / 200.;
			const float mW = 80.385;
			const float mTop = 173.21;
			//			AnalysisObject b_lep(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
			AnalysisObject b_lep(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0,0,0);

			for (unsigned int i = 1; i < signalNotBjetLike.size(); ++i) {
				//1-jet W
				chi2_1 = pow(signalNotBjetLike[i].M() - mW, 2) / mW +
					pow((signalNotBjetLike[i] + mostBjetLike[0]).M() - mTop, 2) / mTop;
				chi2_2 = pow(signalNotBjetLike[i].M() - mW, 2) / mW +
					pow((signalNotBjetLike[i] + mostBjetLike[1]).M() - mTop, 2) / mTop;

				if (chi2_1 < chi2_2 && chi2_1 < chi2min) {
					chi2min = chi2_1;
					top0 = signalNotBjetLike[i] + mostBjetLike[0];
					b_lep = mostBjetLike[1];
				} else if (chi2_2 < chi2_1 && chi2_2 < chi2min) {
					chi2min = chi2_2;
					top0 = signalNotBjetLike[i] + mostBjetLike[1];
					b_lep = mostBjetLike[0];
				}
				for (unsigned int j = i+1; j < signalNotBjetLike.size(); ++j) {

					//2-jet W
					chi2_1 = pow((signalNotBjetLike[i] + signalNotBjetLike[j]).M() - mW, 2) / mW +
						pow((signalNotBjetLike[i] + signalNotBjetLike[j] + mostBjetLike[0]).M() - mTop, 2) / mTop;
					chi2_2 = pow((signalNotBjetLike[i] + signalNotBjetLike[j]).M() - mW, 2) / mW +
						pow((signalNotBjetLike[i] + signalNotBjetLike[j] + mostBjetLike[1]).M() - mTop, 2) / mTop;

					if (chi2_1 < chi2_2 && chi2_1 < chi2min) {
						chi2min = chi2_1;
						top0 = signalNotBjetLike[i] + signalNotBjetLike[j] + mostBjetLike[0];
						b_lep = mostBjetLike[1];
					} else if (chi2_2 < chi2_1 && chi2_2 < chi2min) {
						chi2min = chi2_2;
						top0 = signalNotBjetLike[i] + signalNotBjetLike[j] + mostBjetLike[1];
						b_lep = mostBjetLike[0];
					}
				}
			}
			ntupVar("m_tophad", top0.M());

			TLorentzVector nu, ttbar;
			nu.SetPxPyPzE((1. - alpha) * metVec.Px() - alpha * (top0 + b_lep + signalLeptons[0]).Px(),
					(1. - alpha) * metVec.Py() - alpha * (top0 + b_lep + signalLeptons[0]).Py(), 0, 0);
			nu.SetE(nu.Pt());

			ttbar = top0 + b_lep + signalLeptons[0] + metVec; // under SM hypothesis

			float mTalpha = sqrt((1. - cos(signalLeptons[0].DeltaPhi(nu))) * 2. * signalLeptons[0].Pt() * nu.Pt());
			DMTalpha = mT - mTalpha;
			MTopLep200 = (b_lep + signalLeptons[0] + nu).M();
			TLorentzVector rjet; // naming is historical, -1 times ttbar system is used
			rjet.SetPxPyPzE( -1. * ttbar.Px(), -1. * ttbar.Py(), -1. * ttbar.Pz(), ttbar.E());

			DPhiLepNu200 = fabs(signalLeptons[0].DeltaPhi(nu));
			DPhiRJetLep  = fabs(signalLeptons[0].DeltaPhi(rjet));
			ntupVar("rjet_pt", rjet.Pt());

			if (rjet.Pt() > 400 && DPhiRJetLep > 1.0) {

				BDT::BDTReader* reader = m_BDTReaders[0];

				reader->initInEventLoop();
				reader->setValue("MET", Met*gev);
				reader->setValue("MT", mT*gev);
				reader->setValue("dMT200", DMTalpha*gev);
				reader->setValue("m_tophad", top0.M()*gev);
				reader->setValue("m_toplep200", MTopLep200*gev);
				reader->setValue("dphi_lep_nu200", DPhiLepNu200);
				reader->setValue("dphi_rjet_lep", DPhiRJetLep);
				BDT_tN_diag_low = reader->getBDT();
				if (BDT_tN_diag_low >= 0.55) accept("tN_diag_low");
			}
		}
		//tN_diag_med
		if (nJets > 3 && nBjets > 0 && preselLowMet && signalJets[0].Pt() > 100 && signalJets[1].Pt() > 50 &&
				Met > 120 && mT > 120 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 && mT2Tau > 80) {
			BDT::BDTReader* reader = m_BDTReaders[1];

			reader->initInEventLoop();
			reader->setValue("met", Met*gev);
			reader->setValue("mt", mT*gev);
			reader->setValue("m_top_chi2", topChi2->M()); //already in gev in the training
			reader->setValue("ht_sig", HtSigMiss);
			reader->setValue("dr_bjet_lep", dRbl);
			reader->setValue("dphi_ttbar", topChi2->DeltaPhi(*top1));
			reader->setValue("dphi_hadtop_met", metVec.DeltaPhi(*topChi2));
			reader->setValue("n_jet", signalJets.size());
			reader->setValue("jet_pt[2]", signalJets[2].Pt()*gev);
			reader->setValue("jet_pt[3]", signalJets[3].Pt()*gev);
			BDT_tN_diag_med = reader->getBDT();
			if (BDT_tN_diag_med >= 0.75) accept("tN_diag_med");      
		}
		//tN_diag_high
		if (nJets > 4 && nBjets > 0 && preselHighMet && Met > 230 && mT > 120 && rISR > 0.4) {
			BDT::BDTReader* reader = m_BDTReaders[2];

			reader->initInEventLoop();
			reader->setValue("mt", mT*gev);
			reader->setValue("dphi_ttbar", topChi2->DeltaPhi(*top1));
			reader->setValue("dr_bjet_lep", dRbl);
			reader->setValue("jetPt3", signalJets[2].Pt()*gev);
			reader->setValue("jetPt4", signalJets[3].Pt()*gev);
			reader->setValue("m_top_chi2", topChi2->M()); //already in gev in the training
			reader->setValue("RJR_RISR", rISR);
			reader->setValue("RJR_MS", mTS); //already in GeV in the training
			reader->setValue("RJR_dphiISRI", dphiISRI);
			reader->setValue("RJR_NjV", nJV);
			BDT_tN_diag_high = reader->getBDT();
			if (BDT_tN_diag_high >= 0.8) accept("tN_diag_high");      
		}
		//bWN
		if (nJets > 3 && nBjets > 0 && preselHighMet && signalJets[0].Pt() > 50 && Met > 300 && mT > 130 &&
				amT2 < 110 && dPhiMetLep < 2.5 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 && mT2Tau > 80)
			accept("bWN");
		//bC2x_diag
		if (nJets > 3 && nBjets > 1 && preselHighMet && signalJets[2].Pt() > 75 && signalJets[3].Pt() > 30 &&
				signalBJets[1].Pt() > 30 && Met > 230 && HtSigMiss > 13 && mT > 180 && amT2 > 175 &&
				absDPhiJMet[0] > 0.7 && absDPhiJMet[1] > 0.7 && WRecl.M() > 50 && mT2Tau > 80)
			accept("bC2x_diag");
		//bC2x_med
		if (nJets > 3 && nBjets > 1 && preselHighMet && signalJets[0].Pt() > 200 && signalJets[1].Pt() > 140 &&
				signalBJets[1].Pt() > 140 && Met > 230 && HtSigMiss > 10 && mT > 120 && amT2 > 300 &&
				absDPhiJMet[0] > 0.9 && absDPhiJMet[1] > 0.9 && WRecl.M() > 50 && mT2Tau > 80)
			accept("bC2x_med");
		//bCbv
		if (nJets > 1 && nBjets == 0 && preselHighMet && signalJets[0].Pt() > 120 && signalJets[1].Pt() > 80 &&
				Met > 360 && HtSigMiss > 16 && mT > 200 && absDPhiJMet[0] > 2.0 && absDPhiJMet[1] > 0.8 &&
				WRecl.M() >= 70 && WRecl.M() <= 100 && dPhiMetLep > 1.2 && baseLeptons[0].Pt() > 60)
			accept("bCbv");
		//DM_low_loose
		if (nJets > 3 && nBjets > 0 && preselHighMet && signalJets[1].Pt() > 60 && signalJets[2].Pt() > 40 &&
				Met > 300 && mT > 120 && HtSigMiss > 14 && amT2 > 140 && dPhiMetLep > 0.8 && absDPhiJiMet > 1.4)
			accept("DM_low_loose");
		//DM_low
		if (nJets > 3 && nBjets > 0 && preselHighMet && signalJets[0].Pt() > 120 && signalJets[1].Pt() > 85 &&
				signalJets[2].Pt() > 65 && signalBJets[0].Pt() > 60 && Met > 320 && mT > 170 && HtSigMiss > 14 &&
				amT2 > 160 && topRecl.M() > 130 && dPhiMetLep > 1.2 && absDPhiJiMet > 1.0 && mT2Tau > 80)
			accept("DM_low");
		//DM_high
		if (nJets > 3 && nBjets > 0 && preselHighMet && signalJets[0].Pt() > 125 && signalJets[1].Pt() > 75 &&
				signalJets[2].Pt() > 65 && Met > 380 && mT > 225 && amT2 > 190 && topRecl.M() > 130 &&
				dPhiMetLep > 1.2 && absDPhiJiMet > 1.0)
			accept("DM_high");
	}


	float Wpt = NAN;
	float minDPhiMetBJet = NAN;
	if (nBjets > 0) minDPhiMetBJet = minDphi(metVec, signalBJets, 2);
	float dRbb = mostBjetLike[0].DeltaR(mostBjetLike[1]);  

	// soft lepton selections
	if (signalSoftLeptons.size() == 1) {
		bool preselSoftLep = Met > 230;
		if (signalSoftLeptons[0].type() == ELECTRON)
			preselSoftLep = preselSoftLep && signalSoftLeptons[0].pass(ETightLH);

		Wpt = (signalSoftLeptons[0]+metVec).Pt();    

		//bffN
		if (nJets > 1 && nBjets > 0 && preselSoftLep && signalJets[0].Pt() > 400 && Met > 300 && mT < 160 &&
				pTLepOverMet < 0.02 && minDPhiMetBJet < 1.5 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 &&
				topRecl.M() < 150 && !signalJets[0].pass(BTag77MV2c20))
			accept("bffN");
		//bCsoft_diag
		if (nJets > 1 && nBjets > 0 && preselSoftLep && signalJets[0].Pt() > 400 && Met > 300 && mT < 50 &&
				pTLepOverMet < 0.02 && minDPhiMetBJet < 1.5 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 &&
				topRecl.M() < 150 && !signalJets[0].pass(BTag77MV2c20))
			accept("bCsoft_diag");
		//bCsoft_med
		if (nJets > 2 && nBjets > 1 && preselSoftLep && signalJets[0].Pt() > 120 && signalJets[1].Pt() > 60 &&
				signalJets[2].Pt() > 40 && signalBJets[0].Pt() > 120 && signalBJets[1].Pt() > 60 && Met > 230 &&
				mT < 160 && pTLepOverMet < 0.03 && amT2 > 200 && minDPhiMetBJet > 0.8 && absDPhiJMet[0] > 0.4 &&
				absDPhiJMet[1] > 0.4 && Wpt > 400)
			accept("bCsoft_med");
		//bCsoft_high
		if (nJets > 1 && nBjets > 1 && preselSoftLep && signalJets[1].Pt() > 100 && signalBJets[1].Pt() > 100 &&
				Met > 230 && mT < 160 && pTLepOverMet < 0.03 && amT2 > 300 && minDPhiMetBJet > 0.4 &&
				absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 && Wpt > 500 && dRbb > 0.8)
			accept("bCsoft_high");
	}

	ntupVar("met_perp", MetPerp);  
	ntupVar("amt2", amT2);
	ntupVar("mt", mT);
	ntupVar("mt2_tau", mT2Tau);
	ntupVar("ht_sig", HtSigMiss);

	ntupVar("lep_pt", baseLeptons[0].Pt());
	if (nJets>0) ntupVar("jet0_pt", signalJets[0].Pt());
	if (nJets>1) ntupVar("jet1_pt", signalJets[1].Pt());
	if (nJets>2) ntupVar("jet2_pt", signalJets[2].Pt());
	if (nJets>3) ntupVar("jet3_pt", signalJets[3].Pt());
	if (nBjets>0)  ntupVar("bjet_pt0", signalBJets[0].Pt());
	if (nBjets>1)  ntupVar("bjet_pt1", signalBJets[1].Pt());


	ntupVar("dr_bjet_lep", dRbl);
	ntupVar("dphi_ttbar", topChi2->DeltaPhi(*top1));
	ntupVar("dphi_hadtop_met", metVec.DeltaPhi(*topChi2));
	ntupVar("dphi_met_lep", dPhiMetLep);
	ntupVar("dphi_jet0_ptmiss", absDPhiJMet[0]);
	ntupVar("dphi_jet1_ptmiss", absDPhiJMet[1]);
	ntupVar("deltaRbb", dRbb);
	ntupVar("minDphiMetBjet", minDPhiMetBJet);

	ntupVar("m_top_chi2", topChi2->M());
	ntupVar("hadw_cand_m", WRecl.M());
	ntupVar("hadtop_cand_m0", topRecl.M());
	ntupVar("lepPt_over_met", pTLepOverMet);
	ntupVar("pt_W", Wpt);

	ntupVar("dMT200", DMTalpha);
	ntupVar("m_toplep200", MTopLep200);
	ntupVar("dphi_lep_nu200", DPhiLepNu200);
	ntupVar("dphi_rjet_lep", DPhiRJetLep);

	ntupVar("RJR_RISR", rISR);  
	ntupVar("RJR_MS", mTS);
	ntupVar("RJR_dphiISRI", dphiISRI);
	ntupVar("RJR_NjV", nJV);

	ntupVar("BDT_tN_diag_low", BDT_tN_diag_low);
	ntupVar("BDT_tN_diag_med", BDT_tN_diag_med);
	ntupVar("BDT_tN_diag_high", BDT_tN_diag_high);
}

#endif
