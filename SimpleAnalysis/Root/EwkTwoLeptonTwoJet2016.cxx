#include "SimpleAnalysis/AnalysisClass.h"

DefineAnalysis(EwkTwoLeptonTwoJet2016)

void EwkTwoLeptonTwoJet2016::Init()
{
  addRegions({
              "SR_low",
              "SR_medium",
              "SR_high",
            });


  addHistogram("SR_low_met",20,0,400);
  addHistogram("SR_medium_met",20,0,400);
  addHistogram("SR_high_met",20,0,400);

}

void EwkTwoLeptonTwoJet2016::ProcessEvent(AnalysisEvent *event)
{
  float gen_met      = event->getGenMET();
  float gen_ht       = event->getGenHT();
  int channel_number = event->getMCNumber();

  // baseline electrons are requested to pass the loose likelihood identification criteria
  //    and have pT > 10 GeV and |eta| < 2.47
  auto electrons  = event->getElectrons(10., 2.47, ELooseLH);
  // baseline muons are required to pass the Medium selections and to have pT > 10 GeV, |eta| < 2.5
  auto muons      = event->getMuons(10., 2.5, MuMedium);
  // small-R jets: pT > 20 GeV, |eta| < 2.8
  auto candJets   = event->getJets(20., 2.8);
  // TODO: determine if MET includes TST at all. Doesn't look like it.
  auto metVec     = event->getMET();
  double met      = metVec.Et();

  // Loose bad jet veto for central jets -- this is truth, so we don't really need this...
  if(countObjects(candJets, 20, 2.8, NOT(LooseBadJet))!=0) return;
  // No bad muon veto implemented

  // TODO: do we need this? Let's leave it in here as this was here before
  candJets = filterObjects(candJets, 20, 2.8, JVT50Jet);

  // Overlap removal
  auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/elec.Pt()); };

  electrons  = overlapRemoval(electrons, muons, 0.01);
  candJets   = overlapRemoval(candJets, electrons, 0.2, NOT(BTag77MV2c10));
  electrons  = overlapRemoval(electrons, candJets, radiusCalcElec);
  candJets   = overlapRemoval(candJets, muons, radiusCalcJet, LessThan3Tracks);
  muons      = overlapRemoval(muons, candJets, radiusCalcMuon);

  // No cosmic muon veto implemented
  // How do we do a prompt lepton requirement?

  // require signal jets to be 20 GeV
  auto signalJets      = filterObjects(candJets, 20);
  // signal electrons are required to pass the Medium likelihood criteria and isolated using LooseTrackOnly
  auto signalElectrons = filterObjects(electrons, 25, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoBoosted);
  // signal muons are required to be isolated using LooseTrackOnly
  auto signalMuons     = filterObjects(muons, 25, 2.5, MuD0Sigma3|MuZ05mm|MuIsoBoosted|MuNotCosmic);
  // combine into signalLeptons for easy counting
  auto signalLeptons   = signalElectrons + signalMuons;
  sortObjectsByPt( signalJets );
  sortObjectsByPt( signalLeptons );

  // get b-jets
  auto bjets = filterObjects(signalJets, 25., 2.5, BTag77MV2c10);

  int n_bjets       = bjets.size();
  int n_jets        = signalJets.size();
  int n_leptons     = signalLeptons.size();

  // require exact 2 signal leptons
  if(n_leptons!=2) return;
  if(n_jets<2) return;
  if(n_bjets!=0) return;
 
  // Leading leptons mll
  TLorentzVector dilepton( signalLeptons[0].Px()+signalLeptons[1].Px() , signalLeptons[0].Py()+signalLeptons[1].Py() , 
                           signalLeptons[0].Pz()+signalLeptons[1].Pz() , signalLeptons[0].E() +signalLeptons[1].E()  );
  TLorentzVector W01( signalJets[0].Px()+signalJets[1].Px() , signalJets[0].Py()+signalJets[1].Py() , 
                           signalJets[0].Pz()+signalJets[1].Pz() , signalJets[0].E() +signalJets[1].E()  );
  TLorentzVector ZMET = dilepton+metVec;
  double minDphiMETJet = 1e10;
  int WminJ0 = 0;
  for (int j=0;j<signalJets.size();j++) {
    double Dphi = fabs(signalJets[j].DeltaPhi(ZMET));
    if (Dphi<minDphiMETJet) {
      minDphiMETJet = Dphi;
      WminJ0 = j;
    }
  }
  minDphiMETJet = 1e10;
  int WminJ1 = 0;
  for (int j=0;j<signalJets.size();j++) {
    if (j==WminJ0) continue;
    double Dphi = fabs(signalJets[j].DeltaPhi(ZMET));
    if (Dphi<minDphiMETJet) {
      minDphiMETJet = Dphi;
      WminJ1 = j;
    }
  }
  TLorentzVector Wmin( signalJets[WminJ0].Px()+signalJets[WminJ1].Px() , signalJets[WminJ0].Py()+signalJets[WminJ1].Py() , 
                           signalJets[WminJ0].Pz()+signalJets[WminJ1].Pz() , signalJets[WminJ0].E() +signalJets[WminJ1].E()  );
  TLorentzVector ISR;
  for (int j=0;j<signalJets.size();j++) {
    if (j==WminJ0) continue;
    if (j==WminJ1) continue;
    ISR += signalJets[j];
  }
  float mll = dilepton.M();
  float pTll = dilepton.Pt();
  // inclusive - HT 
  float HT  = sumObjectsPt(signalJets);
  // dphimin between leading 4 signal jets and met
  float dphiMin2 = minDphi(metVec, signalJets, 2);
  // Leading two leptons
  float mt2     = calcMT2(signalLeptons[0],signalLeptons[1],metVec);
  // 3rd leading lepton and met
  float mT3 = n_leptons>2 ? calcMT(signalLeptons[3], metVec) : 0.;

  // mm is channel 0; ee is channel 1; em is channel 2; me is channel 3
  int channel = -1;
  if (signalElectrons.size()==0) channel = 0;
  else if (signalMuons.size()==0) channel = 1;
  else if (n_leptons==2) channel = signalElectrons[0].Pt()>signalMuons[0].Pt()?2:3;
  else {
    // 3 or more leptons, need to check momenta
    if      (signalMuons.size()>1     && signalMuons[1].Pt()>signalElectrons[0].Pt()) channel = 0;
    else if (signalElectrons.size()>1 && signalElectrons[1].Pt()>signalMuons[0].Pt()) channel = 1;
    else channel = signalElectrons[0].Pt()>signalMuons[0].Pt()?2:3;
  }
  if (channel!=0 && channel!=1) return;

  if (signalJets[0].Pt()>30. && signalJets[1].Pt()>30. && dilepton.Pt()>80. && W01.Pt()>100.) {
    if (mll>81. && mll<101. && W01.M()>70 && W01.M()<100 && mt2>100.) {
      if (fabs(metVec.DeltaPhi(W01))>0.5 && fabs(metVec.DeltaPhi(W01))<3.0) {
        if (signalJets[0].DeltaR(signalJets[1])<1.5 && signalLeptons[0].DeltaR(signalLeptons[1])<1.8) {
          if (met>150.) { accept("SR_medium"); fill("SR_medium_met",met); }
          if (met>250.) { accept("SR_high");   fill("SR_high_met",met); }
        }
      }
    }
  }

  if (n_jets==2) {
    if (signalJets[0].Pt()>30. && signalJets[1].Pt()>30. && dilepton.Pt()>60.) {
      if (mll>81. && mll<101. && Wmin.M()>70 && Wmin.M()<90 && met>100.) {
        if (fabs(metVec.DeltaPhi(dilepton))<0.8 && fabs(metVec.DeltaPhi(Wmin))>1.5) {
          if (met/Wmin.Pt()<0.8 && met/dilepton.Pt()>0.6 && met/dilepton.Pt()<1.6) { accept("SR_low"); fill("SR_low_met",met); }
        }
      }
    }
  }
  else if (n_jets>=3 && n_jets<=5) {
    if (signalJets[0].Pt()>30. && signalJets[1].Pt()>30. && signalJets[2].Pt()>30. && dilepton.Pt()>40. && fabs(dilepton.Eta())<1.6) {
      if (mll>86. && mll<96. && Wmin.M()>70 && Wmin.M()<90 && met>100.) {
        if (fabs(metVec.DeltaPhi(ISR))>2.4 && fabs(metVec.DeltaPhi(signalJets[0]))>2.6 && fabs(metVec.DeltaPhi(Wmin))<2.2) {
          if (met/ISR.Pt()>0.4 && met/ISR.Pt()<0.8 && signalJets[WminJ0].DeltaR(signalJets[WminJ1])<2.2) { accept("SR_low"); fill("SR_low_met",met); }
        }
      }
    }
  }

//  // Soft lepton regions -- need to think about lepton momenta here
//  if (met>250 && pTll<20 && n_jets>1 && mll>12 && channel<2){ accept("SRC"); }
//  if (met>500 && pTll<75 && n_jets>1 && mll>12 && channel<2){ accept("SRC_MET"); }
//  // Soft lepton control regions
//  if (met>250 && pTll<20 && n_jets>1 && mll>12 && channel>1){ accept("CRC"); }
//  if (met>500 && pTll<75 && n_jets>1 && mll>12 && channel>1){ accept("CRC_MET"); }
//  // Soft lepton validation regions
//  if (                                  mll>12 && channel<2){ accept("VRC"); }
//  if (                                  mll>12 && channel<2){ accept("VRC_MET"); }

  ntupVar("gen_met", gen_met);
  ntupVar("gen_ht", gen_ht);
  ntupVar("channel_number", channel_number);
  ntupVar("channel", channel);

  ntupVar("met", met);
  ntupVar("HT", HT);
  ntupVar("mT2", mt2);
  ntupVar("mT3", mT3);
  ntupVar("mll", mll);
  ntupVar("pTll", pTll);
  ntupVar("dphiMin2", dphiMin2);

  ntupVar("n_jets", n_jets);
  ntupVar("n_bjets", n_bjets);
  ntupVar("n_electrons", static_cast<int>(signalElectrons.size()));
  ntupVar("n_muons", static_cast<int>(signalMuons.size()));
  ntupVar("n_leptons", n_leptons);

  return;
}
