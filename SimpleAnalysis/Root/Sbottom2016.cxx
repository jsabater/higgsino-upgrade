#include "SimpleAnalysis/AnalysisClass.h"

#include "TMath.h"

DefineAnalysis(Sbottom2016)
//
// Sbottom analysis (Run2 2015+2016 data) [SUSY-2016-28]
// Support Doc: ATL-COM-PHYS-2016-1619
//              https://cds.cern.ch/record/2234855
// @author : M.Tripiana
//
void Sbottom2016::Init()
{
  //** b0L analysis (bb+MET channel)
  //signal regions
  addRegions({"SRAll","b0L_SRA350","b0L_SRA450","b0L_SRA550","b0L_SRB","b0L_SRC"});
  addRegions({"b1L_SRA450","b1L_SRA600","b1L_SRA750","b1L_SRA300_2j","b1L_SRB"});
  
  //control regions
  addRegions({"b0L_CRAz","b0L_CRAtt","b0L_CRAst","b0L_CRAw"});
  addRegions({"b0L_CRBz","b0L_CRBtt","b0L_CRBw"});
  addRegions({"b0L_CRCz","b0L_CRCtt","b0L_CRCw"});
  addRegions({"b1L_CRAtt","b1L_CRAst","b1L_CRBtt","b1L_CRBst","b1L_CRBw"});
  
  //validation regions
  //addRegions({"VRZM","VRWM","VRM"});
  
  //** tb+MET analysis 
  //signal regions
  
  //control regions
  
  //validation regions
  
  // Book 1/2D histograms
  addHistogram("MET",40,0,2000);
  
  addHistogram("b0L_SRAflow",20,0,20);
  
}

void Sbottom2016::ProcessEvent(AnalysisEvent *event)
{
  
  auto electrons  = event->getElectrons(10, 2.47, ELooseLH);
  auto muons      = event->getMuons(10, 2.7, MuMedium);
  auto candJets   = event->getJets(20., 2.8);
  auto metVec     = event->getMET();
  double met      = metVec.Et();
  
  float gen_met   = event->getGenMET();
  float gen_ht    = event->getGenHT();
  
  int    mc_channel = event->getMCNumber();
  
  //MC OR
  if(mc_channel==410000 && gen_met>200.) return; //remove ttbar inc vs MET200 overlap
  if(mc_channel==407012 && gen_met>300.) return; //remove ttbar MET200 vs MET300 overlap
  
  
  for(unsigned int iw=0; iw < event->getMCWeights().size(); iw++)
    ntupVar(("mc_weight"+std::to_string(iw)).c_str(), event->getMCWeights()[iw]);
  
  // Fill in histogram
  fill("b0L_SRAflow",0.5);


  if (countObjects(candJets, 20, 2.8, NOT(LooseBadJet))!=0) return;

  candJets = filterObjects(candJets, 20, 2.8, JVT50Jet);
  
  // Overlap removal
  // not doing overlap removal between electrons and muons with identical track

  auto radiusCalcLepton = [] (const AnalysisObject& lepton, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/lepton.Pt()); };

  candJets   = overlapRemoval(candJets,electrons,0.2,NOT(BTag85MV2c10));
  electrons  = overlapRemoval(electrons,candJets,0.4);
  candJets   = overlapRemoval(candJets, muons, 0.2, NOT(BTag85MV2c10)); 
  muons      = overlapRemoval(muons, candJets, radiusCalcLepton);

  auto baselineLeptons   = electrons + muons;

  auto signalJets20    = filterObjects(candJets, 20);
  auto signalJets35    = filterObjects(candJets, 35);
  auto signalElectrons = filterObjects(electrons, 20, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoLooseTrack);
  auto signalMuons     = filterObjects(muons, 20, 2.5, MuD0Sigma3|MuZ05mm|MuIsoLooseTrack);
  auto signalLeptons   = signalElectrons + signalMuons;
  auto signalBjets20   = filterObjects(signalJets20, 20., 2.5, BTag60MV2c10);
  auto signalBjets35   = filterObjects(signalJets35, 35., 2.5, BTag60MV2c10);


  //MET with invisible leptons
  auto metVecCorr = metVec;
  for(const auto& slep : signalLeptons)
    metVecCorr += slep;
  float metCorr = metVecCorr.Pt();


  //Fill in optional ntuple variables
  ntupVar("met",met);
  
  ntupVar("bj_N20",(int)signalBjets20.size());
  ntupVar("bj_N35",(int)signalBjets35.size());
  ntupVar("j_N20",(int)signalJets20.size());
  ntupVar("j_N35",(int)signalJets35.size());
  ntupVar("e_N",(int)electrons.size());
  ntupVar("m_N",(int)muons.size());
  ntupVar("jets",candJets);


  //Common Selection 
  int nJets20  = signalJets20.size();
  int nBjets20 = signalBjets20.size();
  int nJets35  = signalJets35.size();
  int nBjets35 = signalBjets35.size();
  
  
  accept("SRAll"); //dummy selection to save variables/histos for the full sample
  
  //fill after OR
  fill("MET",met);
  
  //super-common pre-selection
  if (nJets20<2)  return;
  if (nBjets20<1) return; 
  
  
  //** b0L-SR regions definitions
  
  bool zeroLep = (baselineLeptons.size()==0);
  bool oneLep  = (signalLeptons.size()==1);
  bool twoLep  = ((signalElectrons.size()==2 && muons.size()==0) || (signalMuons.size()==2 && electrons.size()==0)); //DF
  if(twoLep) twoLep = (signalLeptons[0].charge() != signalLeptons[1].charge()); //OS
  
  auto myjets  = signalJets35;   //35 GeV jets for A and B regions
  auto mybjets = signalBjets35;
  
  float meff2j  = met + sumObjectsPt(myjets,2);
  float meff    = met + sumObjectsPt(myjets,99);
  float ht      = sumObjectsPt(myjets,99);
  float dphib1  = (nBjets35>0 ? minDphi(metVec, mybjets[0]) : -99);
  float dphib2  = (nBjets35>1 ? minDphi(metVec, mybjets[1]) : -99);
  float dphiMin4 = minDphi(metVec, myjets, 4);
  float mjj = 0;
  float mCT = 0;
  float mblmin = 0;
  bool  bjetsLeading = false;
  if(nJets35>=2) {
    mjj = (myjets[0] + myjets[1]).M();   // = mbb for leading-bjets events
    mCT = calcMCT(myjets[0], myjets[1]); // with two leading bjets in SRAs
    if(oneLep){
      if(nBjets35>1) mblmin = std::min( (signalLeptons[0] + mybjets[0]).M(), (signalLeptons[0] + mybjets[1]).M());
      else if(nBjets35>0) (signalLeptons[0] + mybjets[0]).M();
    }
    
    bjetsLeading = (myjets[0].pass(BTag77MV2c10) && myjets[1].pass(BTag77MV2c10));
  }
  float mt = (oneLep ? calcMT(signalLeptons[0], metVec) : 0.);
  
  float amt2 = 0; //need to identify the two bjets here
  float mbb  = 0;
  int bj1=-1; int bj2=-1;
  for(unsigned int ij=0; ij < myjets.size() ; ij++){
    if( myjets[ij].pass(BTag77MV2c10) ){
      if(bj1<0){
	bj1=ij;
      }else{
	bj2=ij;
	break;
      }
    }
  }
  if(bj2>0){
    mbb = (myjets[bj1] + myjets[bj2]).M();
    
    if(oneLep){
      float mbl1 = (signalLeptons[0]+myjets[bj1]).M();
      float mbl2 = (signalLeptons[0]+myjets[bj2]).M();
      
      if(mbl1 >= 170. && mbl2 < 170.) amt2 = calcMT2(signalLeptons[0]+myjets[bj1], myjets[bj1], metVec);
      else if(mbl1 < 170. && mbl2 >= 170.) amt2 = calcMT2(myjets[bj1], signalLeptons[0]+myjets[bj1], metVec);
      else if(mbl1 < 170. && mbl2 < 170.){
	float amt2_a = calcMT2(signalLeptons[0]+myjets[bj1], myjets[bj1], metVec);
	float amt2_b = calcMT2(myjets[bj1], signalLeptons[0]+myjets[bj1], metVec);
	amt2 = std::min(amt2_a, amt2_b);
      }
    }
  }

  
  //------ b0L-SRAx + CRs
  if( zeroLep && nJets35>=2 && nJets35<=4 &&
      myjets[0].Pt() > 130. && 
      myjets[1].Pt() > 50.  && 
      (nJets35<4 || myjets[3].Pt() < 50.) && 
      nBjets35==2 && bjetsLeading && 
      met>250. && met/meff2j>0.25 && 
      dphiMin4 > 0.4 && 
      mjj > 200. ){
    if(mCT > 350.) accept("b0L_SRA350"); 
    if(mCT > 450.) accept("b0L_SRA450"); 
    if(mCT > 550.) accept("b0L_SRA550"); 
  }
  
  
  float mll = 0;
  if(twoLep) mll = (signalLeptons[0] + signalLeptons[1]).M();
  
  if( twoLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 90. &&
      (mll > 76. && mll < 106.) && 
      myjets[0].Pt() > 50. &&
      myjets[1].Pt() > 50. &&
      (nJets35<4 || myjets[3].Pt() < 50.) && 
      nBjets35==2 && bjetsLeading && 
      met < 100. && metCorr > 100. && 
      met/meff2j>0.25 &&   
      //dphiMin4 > 0.4 && //CHECK!
      mjj > 200. && 
      mCT > 250.){      accept("b0L_CRAz"); }
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 130. &&   
      myjets[1].Pt() > 50. && 
      (nJets35<4 || myjets[3].Pt() < 50.) && 
      nBjets35==2 && bjetsLeading && 
      met > 200. && 
      met/meff2j>0.25 &&   
      dphiMin4 > 0.4 && 
      mjj < 200. && 
      mCT > 250.){      accept("b0L_CRAtt"); }
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 130. &&   
      myjets[1].Pt() > 50. && 
      (nJets35<4 || myjets[3].Pt() < 50.) && 
      nBjets35==2 && bjetsLeading && 
      met > 200. && 
      met/meff2j>0.25 &&   
      dphiMin4 > 0.4 && 
      mjj > 200. && 
      mCT > 250. && 
      mblmin > 170. ){      accept("b0L_CRAst");    accept("b1L_CRAst");   }    //b0L- and b1L- share the single top CR
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 50. &&   
      myjets[1].Pt() > 50. && 
      (nJets35<4 || myjets[3].Pt() < 50.) && 
      nBjets35==2 && bjetsLeading && 
      met > 200. && 
      met/meff2j>0.25 &&   
      dphiMin4 > 0.4 && 
      mjj > 200. && 
      mCT > 250. && 
      mt  > 30. ){      accept("b0L_CRAw"); }
  
  
  //------------------

  //------ b1L-SRAx + CRs
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 35. &&   
      myjets[1].Pt() > 35. && 
      nBjets35==2 && 
      met > 200. && 
      met/sqrt(ht) > 8 &&  
      mblmin < 170. && 
      dphiMin4 > 0.4){
    
    if( mt  > 140. && 
	amt2  > 250. &&
	mbb > 200.){
      
      if(meff > 450. )  accept("b1L_SRA450"); 
      if(meff > 600. )  accept("b1L_SRA600"); 
      if(meff > 750. )  accept("b1L_SRA750"); 
      if(nJets35==2 && meff > 300. )  accept("b1L_SRA300_2j"); 
      
    }
    
    if( mt  > 120. && 
	amt2  > 200. &&
	mbb < 200. && 
	meff > 300. && 
	calcMTmin(mybjets, metVec, 2) > 200.){
      
      accept("b1L_SRB"); 
    }
  }
  
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 35. &&   
      myjets[1].Pt() > 35. && 
      nBjets35==2 && 
      met > 200. && 
      met/sqrt(ht) > 8 &&  
      (mblmin < 170.) && 
      dphiMin4 > 0.4 && 
      mt  > 140. && 
      (amt2 < 250.) &&
      (mbb < 200.) && 
      (meff > 300.) && 
      mCT < 250. ){  accept("b1L_CRAtt");  }
  
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 35. &&   
      myjets[1].Pt() > 35. && 
      met > 200. && 
      met/sqrt(ht) > 8 &&  
      dphiMin4 > 0.4 
      ){ 
    
    if( nBjets35==2 && mbb < 200. && (mblmin < 170.) && (mt > 120.) && (amt2 < 200.) && calcMTmin(mybjets, metVec, 2) < 200. && dphib1 > 2.0 ) accept("b1L_CRBtt");
    
    if( nBjets35==2 && mbb > 200. && (mblmin > 170.) && (mt > 30.) && (mt < 120.) && calcMTmin(mybjets, metVec, 2) > 200. && dphib1 > 2.0 ) accept("b1L_CRBst");
    
    float mbj = 0;
    if(mybjets.size()) mbj = (myjets[0].pass(BTag77MV2c10) ? (mybjets[0] + myjets[1]).M() : (mybjets[0] + myjets[0]).M() );
    if( nBjets35==1 && (mbj > 200.) && (mblmin < 170.) && (mt > 30.) && (mt < 120.) && (amt2 > 200.) && calcMTmin(mybjets, metVec, 1) > 200. && dphib1 > 2.0 ) accept("b1L_CRBw");
    
  }
  //------------------
    
  //b0L-SRB
  if( zeroLep && nJets35>=2 && nJets35<=4 &&
      myjets[0].Pt() > 50. && 
      myjets[1].Pt() > 50.  && 
      nBjets35==2 &&
      met>250. && 
      dphiMin4 > 0.4 && 
      mjj > 200. &&
      dphib1 < 2.0 && 
      dphib2 < 2.5 &&
      calcMTmin(myjets, metVec, 4) > 250. ){  accept("b0L_SRB");    }
  
  
  if( twoLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      (mll > 76. && mll < 106.) && 
      myjets[0].Pt() > 50. && 
      myjets[1].Pt() > 50.  && 
      nBjets35==2 &&
      met<100. && 
      metCorr>100. && 
      dphiMin4 > 0.4 && 
      mjj > 200. &&
      calcMTmin(myjets, metVec, 4) > 200. ){  accept("b0L_CRBz");    }
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 50. && 
      myjets[1].Pt() > 50.  && 
      nBjets35==2 &&
      met>100. && 
      dphiMin4 > 0.4 && 
      mt > 30. &&
      dphib1 < 2.0 && 
      dphib2 < 2.5 &&
      calcMTmin(myjets, metVec, 4) > 200. ){  accept("b0L_CRBtt");    }
  
  if( oneLep && nJets35>=2 && nJets35<=4 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 50. && 
      myjets[1].Pt() > 50.  && 
      nBjets35==1 &&
      met>100. && 
      dphiMin4 > 0.4 && 
      mt > 30. &&
      dphib1 < 2.0 && 
      calcMTmin(myjets, metVec, 4) > 250. ){  accept("b0L_CRBw");    }
  
  
  
  //b0L-SRC
  myjets  = signalJets20;  //20GeV jets 
  mybjets = signalBjets20;
  
  float ht4     = sumObjectsPt(myjets,99) - sumObjectsPt(myjets,3);
  bool bjetsSublead = (nJets35>=3 && myjets[0].pass(NOT(BTag77MV2c10)) && myjets[1].pass(BTag77MV2c10) &&
  		       (myjets[2].pass(BTag77MV2c10) || (nJets35>=4 && myjets[3].pass(BTag77MV2c10))));
  float dphiMin1  = minDphi(metVec, myjets, 1);
  float dphiMin2  = minDphi(metVec, myjets, 2);
  
  mjj  = (myjets[0] + myjets[1]).M();
  float meff4j = met + sumObjectsPt(myjets,4);
  float asym = (myjets[0].Pt()-myjets[1].Pt()) / (myjets[0].Pt()+myjets[1].Pt());
  
  if( zeroLep && nJets20>=2 && nJets20<=5 &&
      myjets[0].Pt() > 500. && 
      myjets[1].Pt() > 20.  && 
      ht4 < 70. &&	
      nBjets20==2 && bjetsSublead &&
      met > 500. && 
      dphiMin2 > 0.2 && 
      dphiMin1 > 2.5 && 
      mjj > 200. &&
      meff4j > 1300. && 
      asym > 0.8){     accept("b0L_SRC");    }
  
  if( twoLep && nJets20>=2 && nJets20<=5 &&
      signalLeptons[0].Pt() > 27. &&
      (mll > 76. && mll < 106.) && 
      myjets[0].Pt() > 250. && 
      myjets[1].Pt() > 20.  && 
      ht4 < 70. &&	
      nBjets20==2 && bjetsSublead &&
      met < 100. && 
      metCorr > 200. &&
      //dphiMin2 > 0.2 && 
      dphiMin1 > 2.5 && 
      mjj > 200. &&
      meff4j > 500. && 
      asym > 0.5){     accept("b0L_CRCz");    }
  
  if( oneLep && nJets20>=2 && nJets20<=5 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 500. && 
      myjets[1].Pt() > 20.  && 
      ht4 < 70. &&	
      nBjets20==2 && bjetsSublead &&
      met > 100. && 
      //dphiMin2 > 0.2 && 
      dphiMin1 > 2.5 && 
      mjj > 200. &&
      meff4j > 1300. &&
      mt > 30. && 
      asym > 0.5){     accept("b0L_CRCtt");    }
  
  if( oneLep && nJets20>=2 && nJets20<=5 &&
      signalLeptons[0].Pt() > 27. &&
      myjets[0].Pt() > 500. && 
      myjets[1].Pt() > 20.  && 
      ht4 < 70. &&	
      nBjets20==1 && myjets[0].pass(NOT(BTag77MV2c10)) &&
      met > 100. && 
      //dphiMin2 > 0.2 && 
      dphiMin1 > 2.5 && 
      mjj > 200. &&
      meff4j > 500. &&
      mt > 30. && mt < 120. &&
      asym > 0.8){     accept("b0L_CRCw");    }
  
  
  return;
}
