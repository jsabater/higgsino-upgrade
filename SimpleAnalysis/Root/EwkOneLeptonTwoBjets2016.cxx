#include "SimpleAnalysis/AnalysisClass.h"
#include "SimpleAnalysis/NtupleMaker.h"
#include "SimpleAnalysis/PDFReweight.h"
#include <LHAPDF/LHAPDF.h>
#include "TMath.h"

DefineAnalysis(EwkOneLeptonTwoBjets2016)
//
// Wh->1l+bb+mET analysis (Run2 2015+2016 data)
// Support Doc: https://cds.cern.ch/record/2259593
//              
// @author : S. Muanza <muanza@cern.ch>
// @author : Rima El Kosseifi <rima.el.kosseifi@cern.ch>

void EwkOneLeptonTwoBjets2016::Init()

{ 
  // Define signal/control regions
  addRegions({"SR_h_Low", "SR_h_Med", "SR_h_High"} ); 
  addRegions({"CR_tt_Low","CR_tt_Med","CR_tt_High"}); 
  addRegions({"VR_off_Low","VR_off_Med","VR_off_High"});  
  addRegions({"VR_on_Low","VR_on_Med","VR_on_High"});  
  addRegion("CR_W");                              
  addRegion("CR_singletop");                         
  addRegion("preSelection");
  addRegion("Acc");                              
}

void EwkOneLeptonTwoBjets2016::ProcessEvent(AnalysisEvent *event)
{
  //
  // Soft Objects
  //
  auto  Soft_Elec = event->getElectrons(10,2.47,ELooseBLLH);
  auto  Soft_Muon = event->getMuons(10,2.70, MuMedium| MuNotCosmic | MuGood);
  auto  Soft_Jets = event->getJets(20.,2.8);
  auto  Soft_Taus = event->getTaus(20.,2.47,TauOneProng | TauThreeProng);
  auto  mET_Vect  = event->getMET();
  float mET       = mET_Vect.Pt();
   //
  // Baseline objects
  //
  // Reject events with bad jets
  if(countObjects(Soft_Jets,20,2.8,GoodJet)==0) return;  
 //
  auto radiusCalcMuon = [] (const AnalysisObject& muon,const AnalysisObject&) {return 0.04 + 10/muon.Pt();};
  auto Base_Elec      = overlapRemoval(Soft_Elec,Soft_Muon,0.01);
  auto Base_Jets      = overlapRemoval(Soft_Jets,Base_Elec,0.2, NOT(BTag77MV2c10));
  Base_Elec           = overlapRemoval(Base_Elec,Base_Jets,0.4);
  auto Base_Muon      = overlapRemoval(Soft_Muon,Base_Jets,radiusCalcMuon);
  Base_Jets            = overlapRemoval(Base_Jets, Base_Muon, 0.4, LessThan3Tracks);
  Base_Muon           = overlapRemoval(Base_Muon, Base_Jets, 0.4);
  auto Base_Bjet      = filterObjects(Base_Jets,20.,2.5,BTag77MV2c10);
  Base_Muon           = overlapRemoval(Base_Muon,Base_Bjet,0.2);
  unsigned int N_Base_Elec=Base_Elec.size();
  unsigned int N_Base_Muon=Base_Muon.size();
  unsigned int N_Base_Lept = N_Base_Elec + N_Base_Muon;
  //
  // Signal objects
  //
  auto Signal_Elec = filterObjects(Base_Elec,27.,2.47,ETightLH | ED0Sigma5  | EZ05mm  | EIsoGradientLoose);
  auto Signal_Muon = filterObjects(Base_Muon,27.,2.7,MuMedium | MuD0Sigma3 | MuZ05mm |MuIsoGradientLoose);
  auto Signal_Jets = filterObjects(Base_Jets,25.,2.50,JVT50Jet);  
  auto Signal_BJet = filterObjects(Signal_Jets,25.,2.5,BTag77MV2c10 | GoodBJet);
  auto Signal_LJet = filterObjects(Signal_Jets,25.,2.5,NOT(BTag77MV2c10));
  auto Signal_Lep=  Signal_Elec + Signal_Muon;
  //Count the number of signal objects
  unsigned int N_Signal_Elec = Signal_Elec.size();
  unsigned int N_Signal_Muon = Signal_Muon.size();
  unsigned int N_Signal_Lept = N_Signal_Elec + N_Signal_Muon;
  unsigned int N_Signal_Jets = Signal_Jets.size();
  unsigned int N_Signal_BJet = Signal_BJet.size();
  unsigned int N_Signal_LJet = Signal_LJet.size();
  float m_T=0, m_CT=0,  m_bb=0;
  if (Signal_Lep.size()==1   && Signal_BJet.size()==2 ) {
    m_T   = calcMT(Signal_Lep[0], mET_Vect);
    m_CT = calcMCT(Signal_BJet[0],Signal_BJet[1]);
    m_bb= (Signal_BJet[0]+Signal_BJet[1]).M();
  }  

  // Preselection
  //
  if(N_Base_Lept  !=1) return;
  if(N_Signal_Lept != 1) return;
  if(N_Signal_BJet != 2) return;
  if(N_Signal_Jets>4 || N_Signal_Jets==4) return;
   accept("Acc");
   
   if(mET< 100.) return; 
  if(m_T< 40.) return; 
  if(m_bb< 50) return;
  accept("preSelection");

  // Signal regions
  if(mET>200 && m_bb>105 && m_bb<135 && m_CT>160){
    if(m_T>100 && m_T<140)  accept("SR_h_Low");
    else if(m_T>140 && m_T<200)  accept("SR_h_Med");
    else if(m_T>200) accept("SR_h_High");
  }
  
// Control  regions tt
  if(mET>200 && (m_bb<105 || m_bb>135) && m_CT<160){
    if(m_T>100 && m_T<140) accept("CR_tt_Low");
    else if(m_T>140 && m_T<200)  accept("CR_tt_Med");
    else if(m_T>200)  accept("CR_tt_High");
  }

  // validation regions off 
  if(mET>180 && (m_bb<95 ||( m_bb>145  && m_bb<195))&& m_CT>160){
    if(m_T>100 && m_T<140) accept("VR_off_Low");
    else if(m_T>140 && m_T<200) accept("VR_off_Med");
    else if(m_T>200)  accept("VR_off_High");
  } 

  // validation regions on
  if(mET>200 && m_bb>105 && m_bb<135 && m_CT<160){
    if(m_T>100 && m_T<140)   accept("VR_on_Low");
    else if(m_T>140 && m_T<200)  accept("VR_on_Med");
    else if(m_T>200)  accept("VR_on_High");
  } 

  // Control  regions W
  if(mET>200 && m_bb<80 && m_T<100 && m_CT>160) accept("CR_W");
   
  // Control  regions t
  if(mET>200 && m_bb>195 && m_T>100 && m_CT>160) accept("CR_singletop");
 
  return;    
}
