#include "SimpleAnalysis/AnalysisClass.h"
#include <unordered_set>

DefineAnalysis(DirectStau2018)

void DirectStau2018::Init()
{
  addRegions({"Total_2FS","Total_stauL","Total_stauR","SRhighMass_stauL","SRlowMass_stauL","SRhighMass_stauR","SRlowMass_stauR","SRhighMass","SRlowMass"});
}

void DirectStau2018::ProcessEvent(AnalysisEvent *event)
{
  static int entry=-1;
  entry++;
  
  int fs = event->getSUSYChannel();

  if( fs!=0 ) accept("Total_2FS");
  if( fs==206 ) accept("Total_stauL");
  if( fs==207 ) accept("Total_stauR");

  auto baselineElectrons  = event->getElectrons(15, 2.47);
  auto baselineMuons      = event->getMuons(10, 2.7);
  auto baselineTaus       = event->getTaus(20., 2.5);
  auto baselineJets       = event->getJets(20., 2.8);
  auto bjets              = filterObjects(baselineJets, 20., 2.5, BTag77MV2c20);
  auto metVec             = event->getMET();
  float met               = metVec.Pt();

  auto mediumtaus         = event->getTaus(20., 2.5, TauMedium);
  auto tightaus           = event->getTaus(20., 2.5, TauTight);

  // Extended SUSY overlap removal      
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineElectrons, 0.05);
  baselineTaus       = overlapRemoval(baselineTaus, baselineElectrons, 0.2);
  baselineTaus       = overlapRemoval(baselineTaus, baselineMuons, 0.2);
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineMuons, 0.01);
  baselineJets       = overlapRemoval(baselineJets, baselineElectrons, 0.2);
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineJets, 0.4);
  baselineJets       = overlapRemoval(baselineJets, baselineMuons, 0.2);
  baselineMuons      = overlapRemoval(baselineMuons, baselineJets, 0.4);
  baselineJets       = overlapRemoval(baselineJets, baselineTaus, 0.2);
  
  if (baselineTaus.size() != 2) return;
  if (mediumtaus.size() != 2) return;
  bool isOS = baselineTaus[0].charge()!=baselineTaus[1].charge();
  if (!isOS) return; 
  if (baselineTaus[0].Pt() < 50 || baselineTaus[1].Pt() < 40) return;
  if (baselineElectrons.size() > 0 || baselineMuons.size() > 0) return; //lightlepton veto
  int njets_B20 = bjets.size();
  if (njets_B20 != 0) return;  //bVeto
  float mll = (baselineTaus[0]+baselineTaus[1]).M();
  if (mll < 120) return; //zHVeto

  bool trig1 = (baselineTaus[0].Pt() >50 && baselineTaus[1].Pt() >40 && met>150);
  bool trig2 = (baselineTaus[0].Pt() >95 && baselineTaus[1].Pt() >60);
  float mt2  = calcMT2(baselineTaus[0],baselineTaus[1],metVec);
  float dphi = fabs(baselineTaus[0].DeltaPhi(baselineTaus[1]));
  float dR   = baselineTaus[0].DeltaR(baselineTaus[1]);

   if ( trig1 && met>150 && tightaus.size()>=1 && dphi>0.8 && dR<3.2 && mt2>70 ) accept("SRhighMass");
   if ( trig2 && met<150 && met>75 && tightaus.size()==2 && dphi>0.8 && dR<3.2 && mt2>70 ) accept("SRlowMass");
   
   if ( fs==206 && trig1 && met>150 && tightaus.size()>=1 && dphi>0.8 && dR<3.2 && mt2>70 ) accept("SRhighMass_stauL");
   if ( fs==206 && trig2 && met<150 && met>75 && tightaus.size()==2 && dphi>0.8 && dR<3.2 && mt2>70 ) accept("SRlowMass_stauL");
   
   if ( fs==207 && trig1 && met>150 && tightaus.size()>=1 && dphi>0.8 && dR<3.2 && mt2>70 ) accept("SRhighMass_stauR");
   if ( fs==207 && trig2 && met<150 && met>75 && tightaus.size()==2 && dphi>0.8 && dR<3.2 && mt2>70 ) accept("SRlowMass_stauR");

  int n_mediumTaus = mediumtaus.size();
  int n_tightTaus  = tightaus.size();
  int nElec        = baselineElectrons.size();
  int nMuon        = baselineMuons.size();
  int nTau         = baselineTaus.size();

  ntupVar("met",met);
  ntupVar("mt2", mt2);
  ntupVar("tau1Pt", baselineTaus[0].Pt());
  ntupVar("tau2Pt", baselineTaus[1].Pt());
  ntupVar("dRtautau", dR);
  ntupVar("Abs_dPhitautau", dphi);
  ntupVar("n_mediumTaus", n_mediumTaus);
  ntupVar("n_tightTaus", n_tightTaus);
  ntupVar("passed_ditauMET", trig1);
  ntupVar("passed_asymditau", trig2);
  ntupVar("minvtautau", mll);
  ntupVar("OS", isOS);
  ntupVar("nBjets",njets_B20);
  ntupVar("nElec", nElec);
  ntupVar("nMuon", nMuon);
  ntupVar("nTau", nTau);
  ntupVar("susyProcess",event->getSUSYChannel());
  ntupVar("mcDSID",event->getMCNumber());
  ntupVar("mcWeights",event->getMCWeights());
    
}

