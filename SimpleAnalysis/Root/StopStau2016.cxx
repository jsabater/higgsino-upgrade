#include "SimpleAnalysis/AnalysisClass.h"
#include <iostream>


static AnalysisObjects pileupRemoval(const AnalysisObjects& cands,
                                     float pt, float eta, int id) {
    AnalysisObjects reducedList;
    for(const auto& cand : cands) {
        if (((cand.Pt()<pt) &&
            (fabs(cand.Eta())<eta) &&
            (cand.pass(id))) || (cand.Pt()>=pt || fabs(cand.Eta())>eta)) reducedList.push_back(cand);
    }
    return reducedList;
}


// muonJetOverlapRemoval() adapted from overlapRemoval()
static AnalysisObjects muonJetOverlapRemoval(const AnalysisObjects &cands, const AnalysisObjects &others, float deltaR);

static AnalysisObjects muonJetOverlapRemoval(const AnalysisObjects &cands, const AnalysisObjects &others, 
                                             std::function<float(const AnalysisObject&,const AnalysisObject&)> radiusFunc);

AnalysisObjects muonJetOverlapRemoval(const AnalysisObjects &cands, const AnalysisObjects &others,
                                      std::function<float(const AnalysisObject&,const AnalysisObject&)> radiusFunc) {
    AnalysisObjects reducedList;
    for(const auto& cand : cands) {
        bool overlap=false;
        for(const auto& other : others) {
            if (cand.DeltaR(other)<radiusFunc(cand,other) && cand!=other && cand.Pt()<(0.5*other.Pt())) {
                overlap=true;
                break;
            }
        }
    if (!overlap) reducedList.push_back(cand);
    }
    return reducedList;
}

AnalysisObjects muonJetOverlapRemoval(const AnalysisObjects &cands, const AnalysisObjects &others, float deltaR) {
  return muonJetOverlapRemoval(cands,others,[deltaR] (const AnalysisObject& , const AnalysisObject&) {return deltaR;});
}


DefineAnalysis(StopStau2016)

void StopStau2016::Init()
{
    addRegions({"SRLH", "SRHH"});
}

void StopStau2016::ProcessEvent(AnalysisEvent *event)
{  
    auto jetCands          = event->getJets(20., 2.8, LooseBadJet);
    //Jets should be rejected if they have Pt < 60, abs(eta)<2.4 and have a JVT output value smaller than 0.59
    auto jets              = pileupRemoval(jetCands, 60., 2.4, JVT50Jet); 
    auto electrons         = event->getElectrons(10, 2.47, ELooseBLLH);
    auto muons             = event->getMuons(10, 2.7, MuMedium);
    auto mediumTaus        = event->getTaus(20, 2.5, TauMedium);
    auto oneProngTaus      = filterObjects(mediumTaus, 20, 2.5, TauMedium|TauOneProng);
    auto threeProngTaus    = filterObjects(mediumTaus, 20, 2.5, TauMedium|TauThreeProng);
    auto taus              = oneProngTaus + threeProngTaus;
    taus                   = filterCrack(taus, 1.37, 1.52);  

    //Reject events with bad jets
    if (countObjects(jets, 20, 2.8, NOT(LooseBadJet))!=0) return;
    if (countObjects(muons, 20, 2.8, NOT(MuNotCosmic))!=0) return;
    //TODO bad muon, primary vertex?
    
    taus       = overlapRemoval(taus, electrons, 0.2);
    taus       = overlapRemoval(taus, muons, 0.2); //TODO: consider combined muons only if tauPt > 50
    electrons  = overlapRemoval(electrons, muons, 0.01); //Actually a shared track requirement, simulated by deltaR = 0.01
    jets       = overlapRemoval(jets, electrons, 0.2);
    electrons  = overlapRemoval(electrons, jets, 0.4);
    muons      = muonJetOverlapRemoval(muons, jets, 0.4); // Removes muons if muon Pt < 0.5 jet Pt 
    //(condition in analysis: The muon Pt must be below either 0.5 Pt jet or 0.7 Sum(Pt(jet tracks)),
    // and the jet must have more than two associated tracks from the PV with Pt<500MeV.)
    jets       = overlapRemoval(jets, muons, 0.4);
    jets       = overlapRemoval(jets, taus, 0.4);
  
    auto signalElectrons    = filterObjects(electrons, 25., 2.47, ETightLH|EZ05mm|ED0Sigma5|EIsoGradientLoose);
    auto signalMuons        = filterObjects(muons, 25., 2.7, MuMedium|MuZ05mm|MuD0Sigma3|MuIsoGradientLoose);
    auto bjets              = filterObjects(jets, 20., 2.5, BTag77MV2c10);
    auto signalLeptons      = signalElectrons + signalMuons;
    auto baseLeptons        = electrons + muons;
  
    auto metVec  = event->getMET();
    double met   = metVec.Et();
    int nJets = jets.size();
    int nBjets = bjets.size();
    int nTaus = taus.size();
    int nBaseElectrons = electrons.size();
    int nBaseMuons = muons.size();
    int nSignalLeptons = signalLeptons.size();
    int nBaseLeptons = nBaseElectrons + nBaseMuons;
    
    float tauPt = 0;
    float tau2Pt = 0;
    float muonPt = 0;
    float electronPt = 0;
    float leadJetPt = 0;
    float subLeadJetPt = 0;
    float Ht = 0;
    if (nJets > 0) leadJetPt = jets[0].Pt();
    if (nJets > 1){
        subLeadJetPt = jets[1].Pt();
        Ht = leadJetPt + subLeadJetPt ;
    }
    if (nTaus) tauPt = taus[0].Pt();
    if (nTaus > 1) tau2Pt = taus[1].Pt();
    if (nBaseElectrons) electronPt = electrons[0].Pt();
    if (nBaseMuons) muonPt = muons[0].Pt();
    
    //Trigger plateaus:
    bool singleE=false;
    bool singleMu=false;
    bool ditau = false;
    if (electronPt > 27) singleE=true;
    if (muonPt > 27.3) singleMu=true;
    if (tauPt >= 50. && tau2Pt >= 40. && leadJetPt >= 80.) ditau=true;
    
    //Preselections:
    bool LH = false;
    bool HH = false;
    if ((nTaus == 1) && 
        (singleE || singleMu) && 
        (signalLeptons.size() == 1) &&
        (baseLeptons.size() == 1) &&
        (subLeadJetPt > 26) &&
        (nJets > 1)) LH = true;
               
    if ((nTaus == 2) && 
        (met >= 180. || ditau) &&
        (baseLeptons.size() == 0) && 
        (subLeadJetPt > 20) &&
        (nJets > 1)) HH = true;

   
    float mT2_l_tau = 0;
    float mT2_tau_tau = 0;
    bool OS_l_tau = 0;
    bool OS_tau_tau = 0;
    
    
    //LepHad variables
    if(LH){
        OS_l_tau        = signalLeptons[0].charge() != taus[0].charge(); 
        mT2_l_tau       = calcMT2(signalLeptons[0], taus[0], metVec);
    }
    //HadHad variables
    if(HH){
        OS_tau_tau      = taus[0].charge() != taus[1].charge(); 
        mT2_tau_tau     = calcMT2(taus[0], taus[1], metVec);
    }

    if ( LH && met>230. && nBjets>0 && mT2_l_tau>100. && tauPt>70. && OS_l_tau) {
        accept("SRLH");
    }
  
    if( HH && nBjets>0 && tauPt>70. && OS_tau_tau && met>200. && mT2_tau_tau>80.){
        accept("SRHH");
    }
    
   
}
