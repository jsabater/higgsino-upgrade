#include "SimpleAnalysis/AnalysisClass.h"

DefineAnalysis(Diphoton2016)

void Diphoton2016::Init()
{
  addRegions({"SRSL", "SRSH", "SRWL", "SRWH"});
}

void Diphoton2016::ProcessEvent(AnalysisEvent *event)
{
  // Baseline objects
  auto photons   = event->getPhotons(25., 2.37, PhotonGood); 
  auto electrons = event->getElectrons(10., 2.47, EMediumLH);
  auto muons     = event->getMuons(10., 2.7, MuMedium);
  auto jets      = event->getJets(20., 2.8);
  auto met       = event->getMET();

  // Remove events with one bad jet
  if (countObjects(jets, 20, 2.8, NOT(LooseBadJet)) != 0) return;

  // Overlap removal
  photons    = overlapRemoval(photons, electrons, 0.01);

  // Signal objects
  auto signal_photons   = filterObjects(photons,   27., 2.37, PhotonIsoGood);
  auto signal_muons     = filterObjects(muons,     10., 2.7,  MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
  auto signal_electrons = filterObjects(electrons, 15., 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signal_jets      = filterObjects(jets,      30., 2.8);

  signal_photons   = filterCrack(signal_photons);
  signal_electrons = filterCrack(signal_electrons);

  sortObjectsByPt(signal_photons);
  sortObjectsByPt(signal_jets);

  // At least two photons with pt>75 GeV
  if (signal_photons.size() < 2)
    return;

  if (signal_photons[0].Pt() < 75.) return;
  if (signal_photons[1].Pt() < 75.) return;

  // Other variables
  double met_et = met.Et();

  float ht = sumObjectsPt(signal_jets) + sumObjectsPt(signal_photons) + sumObjectsPt(signal_muons) + sumObjectsPt(signal_electrons);

  float dphi_gammet = minDphi(met, signal_photons, 2); //min dphi between MET and two leading photons
  float dphi_jetmet = minDphi(met, signal_jets, 2); // min dphi between MET and two leading jets
  
  // Signal regions
  if (met_et>150. && ht>2750. && dphi_jetmet>0.5)
    accept("SRSL");
  if (met_et>250. && ht>2000. && dphi_jetmet>0.5 && dphi_gammet>0.5)
    accept("SRSH");
  if (met_et>150. && ht>1500. && dphi_jetmet>0.5)
    accept("SRWL");
  if (met_et>250. && ht>1000. && dphi_jetmet>0.5 && dphi_gammet>0.5)
    accept("SRWH");

  return;
}
