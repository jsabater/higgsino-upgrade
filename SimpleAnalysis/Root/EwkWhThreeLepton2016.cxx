#include "SimpleAnalysis/AnalysisClass.h"
#include "SimpleAnalysis/NtupleMaker.h"
#include "TMath.h"

DefineAnalysis(EwkWhThreeLepton2016)

//Wh->3l+MET analysis (Run2 2015+1016 data)
//Support doc : https://cds.cern.ch/record/2289838
//
//Author : F. Trovato <fabrizio.trovato@cern.ch>

void EwkWhThreeLepton2016::Init()
{
//Define control, validation  and signal regions
addRegions({"SR3L_DFOS_0j", "SR3L_DFOS_1ja", "SR3L_DFOS_1jb"});
addRegions({"SR3L_SFOS_0ja", "SR3L_SFOS_0jb", "SR3L_SFOS_1j"});
addRegions({"VR3L_offZ_highMET"});
addRegions({"CR3L_onZ_highMET"});
addRegions({"Preselection"});
addRegions({"Acc3L"});
} 	

void EwkWhThreeLepton2016::ProcessEvent(AnalysisEvent *event) {
//
//Get soft objects in containers
//
auto  Soft_Elec = event->getElectrons(10,2.47,ELooseBLLH);
auto  Soft_Muon = event->getMuons(10,2.4, MuMedium| MuNotCosmic | MuGood);
auto  Soft_Jets = event->getJets(20.,2.8);
auto  MET_Vect  = event->getMET();
float eT_miss = MET_Vect.Pt();

//
//Remove events with badJets
//
if(countObjects(Soft_Jets,20,2.8,GoodJet)==0) return;

//
////Baseline objects through OverlapRemover
//
auto radiusCalcMuon = [] (const AnalysisObject& muon,const AnalysisObject&) {return 0.04 + 10/muon.Pt();};
auto Base_Elec      = overlapRemoval(Soft_Elec,Soft_Muon,0.01);
auto Base_Jets      = overlapRemoval(Soft_Jets,Base_Elec,0.2, NOT(BTag77MV2c10));
Base_Elec           = overlapRemoval(Base_Elec,Base_Jets,0.4);
auto Base_Muon      = overlapRemoval(Soft_Muon,Base_Jets,radiusCalcMuon);
Base_Jets            = overlapRemoval(Base_Jets, Base_Muon, 0.4, LessThan3Tracks);
Base_Muon           = overlapRemoval(Base_Muon, Base_Jets, 0.4);
auto Base_Bjet      = filterObjects(Base_Jets,20.,2.5,BTag77MV2c10);
auto Base_Bjet_OR      = filterObjects(Base_Jets,20.,2.5,BTag85MV2c10);
Base_Muon           = overlapRemoval(Base_Muon,Base_Bjet_OR,0.2);

unsigned int N_Base_Elec=Base_Elec.size();
unsigned int N_Base_Muon=Base_Muon.size();
unsigned int N_Base_Lep = N_Base_Elec + N_Base_Muon;

//
//Signal objects and multiplicities
//
auto Electrons = filterObjects(Base_Elec,20.,2.47,EMediumLH | ED0Sigma5  | EZ05mm  | EIsoFixedCutTight);
auto Muons = filterObjects(Base_Muon,27.,2.7,MuMedium | MuD0Sigma3 | MuZ05mm |MuIsoGradientLoose);
auto Jets = filterObjects(Base_Jets,20.,2.8,JVT50Jet);
auto bJets = filterObjects(Jets,20.,2.4,BTag77MV2c10 | GoodBJet);
auto Leptons = Electrons + Muons;

unsigned int N_Electrons = Electrons.size();
unsigned int N_Muons = Muons.size();
unsigned int N_Jets = Jets.size();
unsigned int N_bJets = bJets.size();
unsigned int N_Lep = N_Electrons + N_Muons;

//
//Sort leptons and jets in pT
//

//sortObjectsByPt(Leptons);
//sortObjectsByPt(Jets);

//
//3L event selection
//
int is3L=0;
if(N_Base_Lep==3 && N_Lep==3) is3L=1;
if(is3L==0) return; //3L
accept("Acc3L"); //Acceptance of events with 3L
sortObjectsByPt(Leptons); //sort leptons by pT

//
//Invariant mass of the three leptons
//
float mlll = (Leptons[0]+Leptons[1]+Leptons[2]).M();

//
//Preselection
//
if(N_bJets>0) return; //bJet veto
if(Leptons[2].Pt()<25) return; //Lepton pT > 25 GeV
if(eT_miss<20) return; //MET>20 GeV
if(mlll<20) return; //mlll>20 GeV

accept("Preselection"); //From now on all events pass preselection criteria

//
//Variables to use in SR definitions
//

int isDFOS=0.;
float mHiggs=0.;
float dPhiSS=0.;
float DeltaRmin=0.;
float dPhi_i=0.;
float dEta_i=0.;
float dPhi_j=0.;
float dEta_j=0.;

for(unsigned int i=0; i<N_Lep; i++){
   for(unsigned int j=0; j<N_Lep; j++){
	if(i!=j && Leptons[i].type()==Leptons[j].type()	&& Leptons[i].charge()==Leptons[j].charge()){ //i and j form a SFSS pair
	   for(unsigned int k=0; k<N_Lep; k++){
		if(k!=i && k!=j && (Leptons[k].charge()!=Leptons[j].charge()) && (Leptons[k].type()!=Leptons[j].type())){ //k is the DFOS lepton

		isDFOS = 1.;
			
		//Angular splittings (deltaPhi)
		
		dPhiSS = fabs(Leptons[i].Phi()-Leptons[j].Phi()); //Between SFSS pair
		if(dPhiSS > TMath::Pi()) dPhiSS = 2*TMath::Pi() - dPhiSS;

		//Identify the SFSS lepton closest to DFOS
		
		dPhi_i = fabs(Leptons[k].Phi()-Leptons[i].Phi()); //Between lepton i in SFSS pair and DFOS lepton
		if(dPhi_i > TMath::Pi()) dPhi_i = 2*TMath::Pi() - dPhi_i;
		dEta_i = fabs(Leptons[k].Eta()-Leptons[i].Eta());	

		dPhi_j = fabs(Leptons[k].Phi()-Leptons[j].Phi()); //Between lepton j in SFSS pair and DFOS lepton
                if(dPhi_j > TMath::Pi()) dPhi_j = 2*TMath::Pi() - dPhi_j;
		dEta_j = fabs(Leptons[k].Eta()-Leptons[j].Eta());

		if(dPhi_i < dPhi_j) { //if the lepton i is the closest
			mHiggs = (Leptons[i]+Leptons[k]).M();
			DeltaRmin = sqrt((dPhi_i*dPhi_i)+(dEta_i*dEta_i));

		}

		else { //if lepton i is *not* the closest
			mHiggs = (Leptons[j]+Leptons[k]).M();
                        DeltaRmin = sqrt((dPhi_j*dPhi_j)+(dEta_j*dEta_j));
		}

		} //DFOS condition
		
 	} //third lepton loop
      } //SFSS condition
   } //second lepton loop
} //first lepton loop

int isSFOS=0.;
float mTmin=999.;
float mllmin=0.;

for(unsigned int i=0; i<N_Lep; i++){
   for(unsigned int j=0; j<N_Lep; j++){
	for(unsigned int k=0; k<N_Lep; k++){
	  if(i==j || j==k || i==k){ continue;}
	  if((Leptons[i].charge() != Leptons[j].charge()) && (Leptons[i].type() == Leptons[j].type())){ //i and j form a SFOS pair
		isSFOS = 1.;

		float tmp_mT = calcMT(Leptons[k], MET_Vect);
		if(tmp_mT<mTmin) { // lepton k is the one with minimum mT, calculate the mll for SFOS pair after choosing k
		   mTmin = tmp_mT;
		   mllmin = (Leptons[i]+Leptons[j]).M();
		}

	  } //SFOS condition
	} //third lepton loop
   } //second lepton loop
} //first lepton loop
	
//
//Control, Validation and Signal Region definitions 
//

if(isDFOS){ //DFOS SRs
	if(N_Jets==0 && eT_miss>60. && mHiggs<90.){ accept("SR3L_DFOS_0j");}
	else if(N_Jets>0){
		if(eT_miss>30. && eT_miss<100. && mHiggs<60. && DeltaRmin<1.4){ accept("SR3L_DFOS_1ja");}
		else if(eT_miss>100. && mHiggs<70. && dPhiSS<2.8 && DeltaRmin<1.4){ accept("SR3L_DFOS_1jb");}
	}
}

if(isSFOS){ //SFOS SRs, VR and CR
	if(eT_miss>80. && mllmin>20. && (mllmin<81.2 || mllmin>101.2)){ //SFOS Preselection criteria (offZ)
		if(N_Jets==0 && mTmin>110.){
			if(eT_miss<120.){ accept("SR3L_SFOS_0ja");}
			else if(eT_miss>120.){ accept("SR3L_SFOS_0jb");}
		}
		else if(N_Jets>0 && mTmin>110. && eT_miss>110.){ accept("SR3L_SFOS_1j");}

		if(mTmin<110.){ accept("VR3L_offZ_highMET");}
	}

	if(eT_miss>80. && mllmin>20. && mllmin>81.2 && mllmin<101.2){ accept("CR3L_onZ_highMET");}
}

//
//Some variables to store
//
ntupVar("mcDSID",event->getMCNumber());
ntupVar("mcWeights",event->getMCWeights());

return;

}
