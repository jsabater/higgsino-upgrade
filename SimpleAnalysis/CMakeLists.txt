#
# Build configuration for the SimpleAnalysis package
#

# Set the name of the package:
atlas_subdir( SimpleAnalysis )


list(APPEND optional_libraries "")
list(APPEND optional_includes "")
list(APPEND optional_deps "")
list(APPEND optional_subdirs "")

if(DO_RESTFRAMES)
   set(optional_libraries ${optional_libraries} ${RESTFRAMES_LIBRARIES})
   set(optional_includes ${optional_includes} ${RESTFRAMES_INCLUDE_DIRS})
   list(APPEND optional_deps RestFrames)
endif(DO_RESTFRAMES)

if(DO_MT2)
   list(APPEND optional_libraries CalcGenericMT2Lib)
endif(DO_MT2)

#if(DO_TRUTHTAGGING)
#   list(APPEND optional_libraries BTaggingTruthTaggingToolLib)
#endif(DO_TRUTHTAGGING)

if(DO_SMEARING)
   list(APPEND optional_libraries UpgradePerformanceFunctionsLib)
   list(APPEND optional_deps UpgradePerformanceFunctionsLib)
   list(APPEND optional_subdirs PhysicsAnalysis/UpgradePhys/SmearingFunctions/UpgradePerformanceFunctions)
endif(DO_SMEARING)


# Set up which packages this package depends on:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthToolSupport/AsgTools
   PhysicsAnalysis/AnalysisCommon/PATCore
   PhysicsAnalysis/D3PDTools/EventLoop
   PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection
   PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools
   PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool
   PhysicsAnalysis/ElectronPhotonID/IsolationCorrections
   PhysicsAnalysis/ElectronPhotonID/IsolationSelection
   PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonSelectorTools
   PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections
   PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces
   PhysicsAnalysis/JetMissingEtID/JetSelectorTools
   PhysicsAnalysis/JetMissingEtID/JetJvtEfficiency
   PhysicsAnalysis/JetMissingEtID/JetMomentTools
   PhysicsAnalysis/JetMissingEtID/JetResolution
   PhysicsAnalysis/JetMissingEtID/JetCalibTools
   PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency
   PhysicsAnalysis/TauID/TauAnalysisTools/TauAnalysisTools
   PhysicsAnalysis/SUSYPhys/SUSYTools
   ${optional_subdirs}
   PRIVATE
   Control/AthContainers
   Control/CxxUtils
   Control/xAODRootAccess
   Tools/PathResolver
   Trigger/TrigAnalysis/TrigDecisionTool
   Trigger/TrigAnalysis/TriggerMatchingTool
   Event/xAOD/xAODBase
   Event/xAOD/xAODCore
   Event/xAOD/xAODJet
   Event/xAOD/xAODMuon
   Event/xAOD/xAODMissingET
   Event/xAOD/xAODEgamma
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODTracking
   Event/xAOD/xAODTruth
)

# External(s) used by the package:
find_package( ROOT COMPONENTS Math Core Hist Physics REQUIRED )
find_package(Eigen)
find_package(Lhapdf)

# Generate a dictionary for the library:
atlas_add_root_dictionary( SimpleAnalysisLib SimpleAnalysisLibDictSrc
   ROOT_HEADERS Root/LinkDef.h
   EXTERNAL_PACKAGES ROOT )



# Build the shared library of the package:
atlas_add_library( SimpleAnalysisLib
   SimpleAnalysis/*.h Root/*.h Root/*.cxx ${SimpleAnalysisLibDictSrc}
   PUBLIC_HEADERS SimpleAnalysis
   PRIVATE_INCLUDE_DIRS
   ${ROOT_INCLUDE_DIRS}
   ${EIGEN_INCLUDE_DIRS}
   ${optional_includes}
   LINK_LIBRARIES
   ${EIGEN_LIBRARIES}
   ${ROOT_LIBRARIES}
   ${LHAPDF_LIBRARIES}
   ${optional_libraries}
   AthLinks
   AthContainers
   CxxUtils
   MuonMomentumCorrectionsLib
   IsolationSelectionLib IsolationCorrectionsLib
   ElectronPhotonShowerShapeFudgeToolLib
   ElectronPhotonFourMomentumCorrectionLib
   PathResolver
   JetCalibToolsLib
   JetMomentToolsLib
   JetSelectorToolsLib
   TauAnalysisToolsLib
   
   
   SUSYToolsLib
   xAODMuon
   xAODBTaggingEfficiencyLib
   xAODRootAccessInterfaces xAODCore xAODEventFormat xAODEventInfo xAODEgamma  xAODTracking xAODTruth
)

if(DO_RESTFRAMES)
   target_compile_definitions( SimpleAnalysisLib PUBLIC ROOTCORE_PACKAGE_Ext_RestFrames )
endif(DO_RESTFRAMES)

if(DO_MT2)
   target_compile_definitions( SimpleAnalysisLib PUBLIC ROOTCORE_PACKAGE_CalcGenericMT2 )
endif(DO_MT2)

if(DO_TRUTHTAGGING)
   target_compile_definitions( SimpleAnalysisLib PUBLIC ROOTCORE_PACKAGE_BTaggingTruthTagging )
endif(DO_TRUTHTAGGING)

if(DO_SMEARING)
   target_compile_definitions( SimpleAnalysisLib PUBLIC ROOTCORE_PACKAGE_UpgradePerformanceFunctions )
endif(DO_SMEARING)

if(optional_deps)
   add_dependencies(
      SimpleAnalysisLib
      ${optional_deps}
   )
endif(optional_deps)

atlas_add_executable (simpleAnalysis util/simpleAnalysis.cxx 
   INCLUDE_DIRS
   ${optional_includes}
   LINK_LIBRARIES
   SimpleAnalysisLib
)

atlas_add_executable (slimMaker util/slimMaker.cxx 
   INCLUDE_DIRS
   ${optional_includes}
   LINK_LIBRARIES
   SimpleAnalysisLib
)


atlas_install_data(data/*.xml)
atlas_install_data(data/*.root)
