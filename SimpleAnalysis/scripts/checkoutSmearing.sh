#!/bin/bash
DIRECTORY=$(cd `dirname $0`/../.. && pwd)
cd $DIRECTORY
# checkout just the head of UpgradePerformanceFunctions from the athena repository
#git clone  --depth  1 -b 21.2 --no-checkout https://gitlab.cern.ch/atlas/athena.git
git clone  --depth  1 -b 21.2 --no-checkout https://:@gitlab.cern.ch:8443/jsabater/athena.git
cd athena
git config core.sparsecheckout true
echo 'PhysicsAnalysis/UpgradePhys/SmearingFunctions/UpgradePerformanceFunctions/' >  .git/info/sparse-checkout
git read-tree -mu HEAD
find .
cd ..

