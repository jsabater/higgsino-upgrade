
# :frog: RECAST :frog:

More info in the whole RECAST project can be found
https://recast-docs.web.cern.ch/recast-docs/#tldr

This example is a simple way on how to run RECAST for your SimpleAnalysis+UpgradePerformanceFunctions code.
The example shown here is the ones used for the higgsinos with soft leptons HL-LHC prospects ATL-PHYS-PUB-2018-031 or arXiv:1812.07831.

## Quick description of the files:

*  specs/steps.yml : this is where you specify the steps of the analysis, i.e., the command lines you normally run in the terminal.
*  specs/workflow.yml : this should be your workflow (e.g. event selection + statistical analysis), but here it's a dummy file since we just want to run HistFitter.
*  recast.yml : here you specify the parameters that are going to be used by steps.yml.

## Preparation
:warning: WARNING: this is intended to be run locally in your laptop (not e.g. in lxplus) :warning:

(Read [preparation.txt](./preparation.txt) for details)
*  Install docker from https://docs.docker.com/install/
*  Install pip
*  Install virtualenv and recast-atlas
```
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install recast-atlas
```
*  Create a personal access token on github : https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html


### At the beginning of each session you should
```
virtualenv venv
source venv/bin/activate
pip install recast-atlas==0.0.20
```


### To run

:warning: This example uses files under `/eos/user/s/susyupg/` , to access these contact <jorge.sabater.iglesias@desy.de> :warning:

```
git clone https://gitlab.cern.ch/jsabater/higgsino-upgrade.git
# If you are going to use files in eos space (which is the case in the example), you will need to setup the following
RECAST_USER=myUser # eos user (e.g. susyupg)
RECAST_PASS=myPass # password
RECAST_TOKEN=myToken # git token
eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"


# add the project
$(recast catalogue add higgsino-upgrade)
# check that it was added
recast catalogue ls
# check that all is ok
recast catalogue check examples/higgsinos
# check the steps were added
recast tests ls examples/higgsinos
# to run non-interactively                                                                                                                                                                                     
recast tests run examples/higgsinos test_eventselection
```
After this you should have a new directory called `recast-test-*`.
The results of the run will be in `recast-test-*/submitDir/`. There you will find the output of the statistical analysis, in this case, a SimpleAnalysis output sample called testSample.root .
If something went wrong during the run, or you want to check the output, you can take a look at the log files in `recast-test-*/_packtivity/`.

Interactive runs are also possible (for debugging, testing, etc), to do so,
```
# to run in the shell (interactively)
$(recast tests shell examples/higgsinos test_eventselection)
# Now you will have to run all the command lines that you usually run (typically the ones you have in steps.yml)
source /recast_auth/getkrb.sh
source /home/atlas/release_setup.sh 
# and so on
```

## Running in git CI
To run the analysis in git (using git ci) you will have to fill the fields in 

Settings -> CI/CD -> Variables 

KRB_PASS # pwd 

RECAST_TOKEN # token 

KRB_USER # user

I changed the variable names RECAST_PASS (KRB_PASS) and RECAST_USER (KRB_USER) from above for testing reasons. Just make sure the variable names are consistent with the ones in the [gitlab-ci](./.gitlab-ci.yml) file.



# Simpleanalysis

To compile do

```
asetup AnalysisBase,21.2.83
SimpleAnalysis/scripts/checkoutSmearing.sh
mkdir build; mkdir run
cd build
cmake -DDO_SMEARING=ON ../
make
cd ../run/
source ../build/x86_64-centos7-gcc8-opt/setup.sh
```

More info about SimpleAnalysis and UpgradePerformanceFunctions
<https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis.git>



