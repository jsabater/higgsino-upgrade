FROM atlas/analysisbase:latest
ADD . /code/source
WORKDIR /code/build
RUN sudo chown -R atlas /code && source ~/release_setup.sh && /code/source/SimpleAnalysis/scripts/checkoutSmearing.sh && cmake ../source -DDO_SMEARING=ON && make -j4
